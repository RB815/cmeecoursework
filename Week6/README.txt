CMEE (Computational Methods in Ecology and Evolution) Week 6 README file

***Directory Contents***

README.txt -----------------------This file, which provides an overview of Week 6's directory 

/Code ----------------------------Contains shell scripts created and/or used this week. 
	control_flow.py
	debugme.py
	fmr.R
	lmfitex1.py
	LV1.py
	LV2.py
	profileme.py
	run_fmr_R.py
	run_LV.py
	SQLinR.R
	test_control_flow.py
	test_oaks.py
	TestR.py
	texput.log
	ThermalResponse_skeleton.py
	TimingCode.py
	

/Data-----------------------------Contains all data used this week.
	Biotraits.db
	ClimateData.csv
	Consumer.csv
	JustOaksData.csv
	NagyetAl1999.csv
	Resource.csv
	TCP.csv
	test.db
	TestOaksData.csv
	TraitInfo.csv
	
		
/Results--------------------------Outputs from python code
	errorFile.Rout
	fmr_plot.pdf
	outputFile.Rout
	prey_and_predators_1.pdf
	TAutoCorrInterpretation.tex
	TestR.Rout
	TestR_errFile.Rout


***Contact Details***
Author: Rachel Balasuriya
e-mail: rachel.balasuriya15@imperial.ac.uk
