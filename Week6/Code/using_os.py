""" This is blah blah"""

# Use the subprocess.os module to get a list of files and  
# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess


#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.

DirecStartingWithC = []
FilesStartingWithC = []

for (dir, subdir, files) in subprocess.os.walk(home):
	FilesStartingWithC.append(files)
	DirecStartingWithC.append(subdir)

DirecStartingWithC = filter(None, DirecStartingWithC)
FilesStartingWithC = filter(None, FilesStartingWithC)

Cfiles = [item for i in FilesStartingWithC for item in i if (item[0] == 'C')]

Cdir = [item for i in DirecStartingWithC for item in i if (item[0] == 'C')]

print DirecStartingWithC
print FilesStartingWithC

#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Type your code here:


CcFiles = [item for i in FilesStartingWithC for item in i if (item[0] == 'C' or item[0] == 'c')]

CcDirs = [item for i in DirecStartingWithC for item in i if (item[0] == 'C' or item[0] == 'c')]

print CcFiles
print CcDirs
#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

# Type your code here:
CcHome = [item for i in DirecStartingWithC for item in i if (item[0] == 'C' or item[0] == 'c')]

print CcHome
