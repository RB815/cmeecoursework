#!/usr/bin/env python
from lmfit import minimize, Parameters, Parameter, report_fit
import scipy as sc
from matplotlib import pylab as pl

data = sc.genfromtxt("../Data/ClimateData.csv",dtype = None, delimiter = ',')
headers = data[0,:] #~extract headers...
data = sc.array(sc.delete(data,0,0)) #~...and delete them from the data array
Years = data[:,0].astype(int) # convert to integers - to save memory
Temperatures = data[:,3].astype(float)
x = sc.arange(len(Years)) #~pseudo timeplots to feed to SinMod

# define objecive function: returns the array to be minimized
def SinMod(params, x, data): # data = years, params = parameters,
	"""model sine wave, subtract data"""
	Shift = params['Shift'].value
	Amp = params['Amp'].value
	Len = params['Len'].value
	Phase = params['Phase'].value
	
	model = Shift + Amp * sc.sin((2 * sc.pi * x / Len) + Phase)## Measuring Shift, Amplitude, Phase, Length = parameters = 4D. Phase is where the shift is. Amp is length of something
	
	return model - data ## how different the model is from the data
	
#create a set of Parameters  ##- the more parameters, the more accurate, but we don't always know the parameters
params = Parameters()
params.add('Amp', value= 5, min=0) ## we know that we can't have negative amplitude
params.add('Shift', value= 10.0, min=5, max=15) #you can add bounds
params.add('Len', value= 12.0) # Sensitive to this parameter! ## 12 because there are cycles of 12 months in the year
params.add('Phase', value= 1.0)

# do fit (you can try different algorithms, such as lestsq, nelder, etc) ## This is a good place to use 'try:'

result = minimize(SinMod, params, args=(x, Temperatures),method="leastsq") 

# calculate final result
final = Temperatures + result.residual ## calculate result for plotting

# write error report
report_fit(result.params)

# Plot results
try: # misuse of try! ##try to do something. If it doesn't work then move on, but don't throw me out of the loop
	pl.close('all')
	pl.ion() ## interactive plot so you can close the terminal and just see the plot
	fig = pl.plot(x, Temperatures, '+k--')
	pl.plot(x, final, 'r')
	pl.title('lmfit to Climatic fluctuations')
	pl.xlabel('Time')
	pl.ylabel('Temperature ($^\circ$C)')
except:
	pass
