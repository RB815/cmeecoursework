# Using os problem 2
# Needs to run fmr. R with Nagy... data
# print "Output was successful!" if it is P.communicate
# print output

import subprocess

FMR = subprocess.Popen("/usr/lib/R/bin/Rscript --verbose fmr.R", shell=True) .wait()

#subprocess.Popen.communicate("/usr/lib/R/bin/Rscript")
#print subprocess.Popen.communicate("n\n")[0],

# Check shell=True (strongly discouraged in certain situations, but Popen doesn't work without it here)

# if Finished in R! print "Output successful!"
# elseif not Finished in R! print "Output unsuccessful"
# Print output
#self._communicate(input)

FMR = subprocess.check_output(
	'echo to stdout; echo to stderr 1>&2; exit 1',
	shell=True,
	stderr=subprocess.STDOUT,
	)
print FMR
z = FMR

if z == 0:
	print 'Output was successful!'
else:
	print 'Output was unsuccessful. Check for errors and try again!'


# egs
subprocess.check_output(["echo", "Hello World!"])
#'Hello World!\n'

subprocess.check_output("exit 1", shell=True)
#Traceback (most recent call last):
#   ...

# subprocess.CalledProcessError: Command 'exit 1' returned non-zero exit status 


subprocess.check_output(
	"ls non_existent_file; exit 0",
	stderr=subprocess.STDOUT,
	shell=True)
