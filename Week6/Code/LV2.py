""" The typical Lotka-Volterra Model simulated using scipy """
from sys import argv

import scipy as sc 
import scipy.integrate as integrate

import pylab as p #Contains matplotlib for plotting

# import matplotlip.pylab as p #Some people might need to do this

#~ def dR_dt(pops, t=0):
    #~ """ Returns the growth rate of predator and prey populations at any 
    #~ given time step """
    #~ 
    #~ R = pops[0]
    #~ C = pops[1]
    #~ dRdt = r*R - a*R*C 
    #~ dydt = -z*C + e*a*R*C
    #~ 
    #~ return sc.array([dRdt, dydt])

# Add prey density dependence 'rR(1-R/K)

def dR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
 
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1-(R/K)) - a*R*C 
    dydt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dydt])



# Define parameters:
r = float(argv[1]) # Resource growth rate
a = float(argv[2]) # Consumer search rate (determines consumption rate)
z = float(argv[3]) # Consumer mortality rate
e = float(argv[4]) # Consumer production efficiency
K = float(argv[5]) # Carrying capacity

# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 15,  1000)
x0 = 10 # number of starting prey
y0 = 5  # number of starting predators
z0 = sc.array([x0, y0]) # initial conditions: 10 prey and 5 predators per unit area

pops, infodict = integrate.odeint(dR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

#~ print "r =", float(argv[1]), ",", "a =", float(argv[2]), ",", "z =", float(argv[3]), ",", "e =", float(argv[4]), ",", "K =", float(argv[5])

ARGSTEXT = 'r =', float(argv[1]), "a =", float(argv[2]), "z =", float(argv[3]), "e =", float(argv[4]), "K =", float(argv[5])

prey, predators = pops.T # Transpose array 
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-prey population dynamics')
p.suptitle(ARGSTEXT)
p.show()
f1.savefig('../Results/prey_and_predators_1.pdf') #Save figure
