import csv
import sys
import pdb
import doctest

#Define function
def is_an_oak(name):
	""" 
	
	Returns True if name starts with 'quercus '
	>>> is_an_oak('quercus')
	True
	
	>>> is_an_oak('Fagus sylvatica')
	False
	
	>>> is_an_oak('Fraxinus')
	False
	
	>>> is_an_oak('quercuss')
	True
	
	"""
	return name.lower().startswith('quercus')	
	
print(is_an_oak.__doc__)

def main(argv): 
	""" Write oak species to a csv file"""
	f = open('../Data/TestOaksData.csv','rb') # 'r'ead 'b'inary
	g = open('../Data/JustOaksData.csv','wb') # 'w'rite 'b'inary
	taxa = csv.reader(f)
	csvwrite = csv.writer(g)
	oaks = set()
	for row in taxa:
		print row
		print "The genus is", row[0]
		if is_an_oak(row[0]):
			print row[0]
			print 'FOUND AN OAK!'
			print " "
			csvwrite.writerow([row[0], row[1]])    
	
	return 0
	
	ipdb.set_trace()
	
if (__name__ == "__main__"):
	status = main(sys.argv)

doctest.testmod()
