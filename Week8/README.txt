CMEE (Computational Methods in Ecology and Evolution) Week 8 README file

***Directory Contents***

README.txt -----------------------This file, which provides an overview of Week 8's directory 

/Code ----------------------------Contains shell scripts and outputs created and/or used this week
	Day1Prac.R----------------
	Day2Prac.R----------------
		/DataSet1_Analysis
			/01_LD_Trim_Dataset1_2015
				Rakefile
				/scripts
					LDprune.pl	
			/02_MDS_Dataset1_2015
				Rakefile
				/scripts
					MDS.pl
					MDSPlot.R
					plinkMDSR.pl
			/03_ADMIXTURE_Dataset1_2015
				Rakefile
				/scripts
					admixCV.pl
					admixPlot.pl
					admixPlot_old.pl
					admixture2R.pl
					AdPlot.R
					errorPlot.R
					R_Code_backup.txt
					snpexclude.pl
			/04_ADMIX_Datset1_Pretty_Plot_2015
				Rakefile
				ordered_popdata.csv
				/scripts
					plotcsv.pl
					structure_plots.R
		/DataSet2_Analysis
			/01_LD_Trim_Dataset2_2015
				Rakefile
				/scripts
					LDprune.pl	
			/02_MDS_Dataset2_2015
				Rakefile
				/scripts
					MDS.pl
					MDSPlot.R
					plinkMDSR.pl
			/03_ADMIXTURE_Dataset2_2015
				Rakefile
				/scripts
					admixCV.pl
					admixPlot.pl
					admixPlot_old.pl
					admixture2R.pl
					AdPlot.R
					errorPlot.R
					R_Code_backup.txt
					snpexclude.pl
			/04_ADMIX_Datset2_Pretty_Plot_2015
				Rakefile
				ordered_popdata.csv
				/scripts
					plotcsv.pl
					structure_plots.R
		/DataSet3_Analysis
			/01_LD_Trim_Dataset3_2015
				Rakefile
				/scripts
					LDprune.pl	
			/02_MDS_Dataset3_2015
				Rakefile
				/scripts
					MDS.pl
					MDSPlot.R
					plinkMDSR.pl
			/03_ADMIXTURE_Dataset3_2015
				Rakefile
				/scripts
					admixCV.pl
					admixPlot.pl
					admixPlot_old.pl
					admixture2R.pl
					AdPlot.R
					errorPlot.R
					R_Code_backup.txt
					snpexclude.pl
			/04_ADMIX_Datset3_Pretty_Plot_2015
				Rakefile
				ordered_popdata.csv
				/scripts
					plotcsv.pl
					structure_plots.R
			
/Data-----------------------------Contains all data used this week
	/comparative_methods
		anolis_data.csv
		anolis_mtDNA.mrb.con
		anolis_mtDNA_chronogram.tre
		consensusTree_10kTrees_Version2.nex
		Day1_R_basics.docx
		Day2.docx
		Day3.docx
		Primatedata.txt
	/Dataset1
		ME_Dataset1.bed
		ME_Dataset1.bim
		ME_Dataset1.fam
	/Dataset2
		ME_Dataset2.bed
		ME_Dataset2.bim
		ME_Dataset2.fam
	/Dataset3
		ME_Dataset3.bed
		ME_Dataset3.bim
		ME_Dataset3.fam
	/ME_Full_Dataset
		ME_Full_data.bed
		ME_Full_data.bim
		ME_Full_data.fam
	ME_popinfo.txt
	ME_Sample_Localities.gif
	ME_Sample_Localities.gif.pdf

/Results--------------------------Outputs from this week's practicals
	anolisTree.newick---------From Day1Prac.R
	anolisTree.nex------------From Day1Prac.R

/Sandbox--------------------------Across the Shire there is a land fiercely guarded by dragons!
	primate_aligned.fasta
	primate_raw.fasta

***Contact Details***
Author: Rachel Balasuriya
e-mail: rachel.balasuriya15@imperial.ac.uk
