# setwd("~/Documents/CMEECourseWork2015/Week8/Code")
library("ape", "geiger", "caper", "picante")
# "ape" includes function pic() - for independent contrasts (Day2Prac)
# "geiger" includes functions fitContinuous() and fitDiscrete() - to compute lambda

# load tab delimited file with 1st line as headers; assign to 'primatedata'
primatedata <- read.table("../Data/comparative_methods/Primatedata.txt", sep = "\t", header = TRUE) # (sep = "\t") = tab delimited data. (header = TRUE) = first row is header names

str(primatedata) # structure of dataframe
head(primatedata) # first few rows of dataframe
names(primatedata) # headers of dataframe

primatetree <- read.nexus("../Data/comparative_methods/consensusTree_10kTrees_Version2.nex")
primatetree
str(primatetree)

plot(primatetree) # plot data into phylogenetic tree
plot(primatetree, cex = 0.5) # make node labels smaller

zoom(primatetree, list(grep("Cercopithecus", primatetree$tip.label)), subtree = FALSE) # just get tree for Cercopithecus spp.
zoom(primatetree, list(grep("Cercopithecus", primatetree$tip.label)), subtree = TRUE) # just get tree for Cercopithecus spp., but also shows how it fits into rest of the tree

# convert table to dataframe
primatedata2 <- data.frame(primatedata[,4:5])

#log transform data
primatedata2$lnAdultBodyMass_g <- log(primatedata2$AdultBodyMass_g)
primatedata2$lnGestationLen_d <- log(primatedata2$GestationLen_d)

primatedata2$Family <- primatedata[,2] # keep the family name (might be useful later)

rownames(primatedata2) <- primatedata[,3]

head(primatedata2) # first few line
str(primatedata2) # structure of dataframe
primatedata2

# primatedata2 as default dataframe
attach(primatedata2) 

# apply taxon names to variables in the dataframe
names(AdultBodyMass_g) <- rownames(primatedata2)
names(GestationLen_d) <- rownames(primatedata2)
names(lnAdultBodyMass_g) <- rownames(primatedata2)
names(lnGestationLen_d) <- rownames(primatedata2)
names(Family) <- rownames(primatedata2)

# store names of species present in the phylogeny tree, but not the dataset and vice versa
primateOverlap <- name.check(primatetree,primatedata2)
primateOverlap

# remove all spp. from tree which aren't present in the dataset
primateComparativeTree <- drop.tip(primatetree, primateOverlap$tree_not_data)

str(primateComparativeTree)
plot(primateComparativeTree, cex = 0.5)


    # Comparative Analyses Controlling for Phylogeny
# Ordinary Least Squares (OLS) Regression

par(mfrow = c(2,2))

# adult body mass and gestation length
hist(primatedata2$AdultBodyMass_g)
hist(primatedata2$GestationLen_d)

# log transsform data to help counteract skewedness 
hist(log(primatedata2$AdultBodyMass_g))
hist(log(primatedata2$GestationLen_d))

# non-phylogenetic regression
par(mfrow = c(1,1))

model.ols <- lm(GestationLen_d~ lnAdultBodyMass_g, data = primatedata2)
summary(model.ols)

plot(lnGestationLen_d ~ lnAdultBodyMass_g, data =  primatedata2)
abline(model.ols)

# look at the pseudoreplication problem by looking ar one family
points(lnGestationLen_d[Family == "Cercopithecidae"] ~ lnAdultBodyMass_g[Family == "Cercopithecidae"], data = primatedata2, col = "blue", pch = 16)
points(lnGestationLen_d[Family == "Galagidae"] ~ lnAdultBodyMass_g[Family == "Galagidae"], data = primatedata2, col = "red", pch = 16)
points(lnGestationLen_d[Family == "Lemuridae"] ~ lnAdultBodyMass_g[Family == "Lemuridae"], data = primatedata2, col = "purple", pch = 16)


# Independent Contrasts
# fit the model
contr.logGest <- pic(lnGestationLen_d, phy = primateComparativeTree)
contr.logBM <- pic(lnAdultBodyMass_g, phy = primateComparativeTree)
model.icl <- lm(contr.logGest ~ contr.logBM -1) # -1 forces the regression through the origin.
summary(model.icl)

# plot the log gestation against log body mass
plot(contr.logGest ~ contr.logBM)
abline(model.icl)

# Phylogenetic Signal - Pagel's lambda
# generate maximum likelihood estimate of lambda for log GestationLen_d
lngestl <- primatedata2$lnGestationLen_d
names(lngestl) <- row.names(primatedata2)
lambda_GL_ML <- fitContinuous(primatetree, lngestl, model = "lambda")

primatetree_L0 <- rescale(primatetree, "lambda", 0)
plot(primatetree_L0) #note this is now a star phylogeny

