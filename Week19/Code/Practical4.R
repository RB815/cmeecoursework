setwd("~/Documents/CMEECourseWork2015/Week19/Data")
gala <- read.delim("gala.txt")
str(gala)

gala$propEnd <- gala$Endemics / gala$Species
gala$lgArea <- log(gala$Area)
plot(propEnd ~ lgArea, data = gala, cex = log(Species/2))

resp <- with(gala, cbind(Endemics, Species-Endemics))
galaMod <- glm(resp ~ lgArea, data = gala, family = binomial(link=logit))

galaMod <- glm(propEnd ~ lgArea, weights = Species, data = gala, family = binomial(link=logit)) # same as galaMod above

anova(galaMod, test = 'Chisq')
1-pchisq(44.053, 1)

summary(galaMod)
(galaMod$null.deviance - galaMod$deviance)/galaMod$null.deviance

par(mfrow = c(1,2))
plot(galaMod, which=c(1,2))

pred <- expand.grid(lgArea = seq(-5,9,by=0.1))
pred$fit <- predict(galaMod, newdata = pred, type = 'response')

plot(propEnd ~ lgArea, data = gala)
lines(fit ~ lgArea, data = pred, col = 'red')

pred <- expand.grid(lgArea = seq(-5,9,by=0.1))
predMod <- predict(galaMod, newdata = pred, se.fit = TRUE)
pred$fit <- predMod$fit
pred$se.fit <- predMod$se.fit
predMod$confint <- predMod$se.fit * qt(0.975, df = galaMod$df.residual)

graphics.off()
plot(qlogis(propEnd) ~ log(Area), data = gala)
lines(fit ~ lgArea, data = pred, col = 'red')
lines(fit + confint ~ lgArea, data = pred, col = 'red', lty = 2) ### Error
lines(fit - confint ~ lgArea, data = pred, col = 'red', lty = 2) ### Error

plot(propEnd ~ lgArea, data = gala, cex = log(Species/2))
lines(plogis(fit) ~ lgArea, data = pred, col = 'red')
lines(plogis(fit + confint) ~ lgArea, data = pred, col = 'red', lty = 2) ### Error
lines(plogis(fit + confint) ~ lgArea, data = pred, col = 'red', lty = 2) ### Error

galliformes <- read.csv('galliformesData.csv')
str(galliformes)

galliformes <- na.omit(galliformes)

galliformes$ThreatBinary <- ifelse(galliformes$Status04 %in% c("1(LC)", "2(NT)"), 0, 1)
pairs(ThreatBinary ~ Range + Mass + Clutch + ElevRange, data = galliformes)

galliformes$lgMass <- log(galliformes$Mass)
galliformes$lgRange <- log(galliformes$Range)
galliformes$lgClutch <- log(galliformes$Clutch)
galliformes$lgElevrange <- log(galliformes$ElevRange)

par(mfrow = c(2,2))
plot(ThreatBinary ~ lgRange, data = galliformes)
plot(ThreatBinary ~ lgMass, data = galliformes)
plot(ThreatBinary ~ lgClutch, data = galliformes)
plot(ThreatBinary ~ lgElevrange, data = galliformes)

