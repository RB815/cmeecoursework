CMEE (Computational Methods in Ecology and Evolution) Week 2 README file

***Directory Contents***

README.txt -----------------------This file, which provides an overview of Week 2's directory 

/Code ----------------------------Contains shell scripts and outputs created and/or used this week
	align_seqs.py-------------Aligns two fasta files, showing the best alignment and best score
	basic_csv.py--------------Saves selected data into csv files
	basic_io.py---------------Examples of inputting and outputting files and storing data
	blackbirds.py-------------A regex which prints the Kingdom, Phylum and Species name of each species in blackbirds.txt
	boilerplate.py------------Prints 'this is a boilerplate' in Python
	cfexercises.py------------fooXX interactive functions
	control_flow.py-----------Functions which demonstrate the use of control statements	
	dictionary.py-------------Exercise which groups species by order in a dictionary
	lc1.py--------------------Exercise organising information using list comprehensions and loops
	lc2.py--------------------Exercise to extract information using list comprehensions and loops
	loops.py------------------Prints "GERONIMO! Infinite loop! ctrl c to stop!" continuously until you press ctrl c
	LV1.py--------------------Runs the Lotka-Volterra model and plots the equalibrium
	oaks.py-------------------Using list comprehensions to edit data
	pickle--------------------'pickled' output of dictionary in basic_io.py which can be saved on a disk
	re4.py--------------------Extracting email addresses and their subsets using regex
	regexs.py-----------------Using regex (regular expressions) to search strings for particular patterns
	scope.py------------------Effects of using local vs. global variables in functions
	sysargv.py----------------Variable which states the name of the script, number of arguments, and what those arguments are when run in a function/module.
	tuple.py------------------Exercise to organise a tuple of tuples into separate lines
	using_name.py-------------Demonstrates use of __name__ == '__main__'

/Data-----------------------------Contains all data used this week
	blackbirds.txt------------Blackbird taxonomy used in blackbirds.py practical

/Sandbox--------------------------Where we go digging for hidden treasures!
	bodymass.csv--------------Output of basic_csv.py - body mass of species
	Regex_eg------------------Some examples from the date-finding exercise
	testcsv.csv---------------Output of basic_csv.py - taxonomy and body mass of species
	testout.txt---------------Output of basic_io.py
	testp.p-------------------Output of basic_io.py
	test.txt------------------Text file used in basic_io.py function


***Contact Details***
Author: Rachel Balasuriya
e-mail: rachel.balasuriya15@imperial.ac.uk
