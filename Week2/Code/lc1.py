#!/usr/bin/python

__author__ = 'Rachel Balasuriya (rachel.balasuriya15@imperial.ac.uk)'
__version__ = '0.0.1'

"""Latin name, common name and mean body mass (g) of birds separated 
by list comprehensions and conventional loops"""

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )


#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses (g) for
# each species in birds, respectively. 

# (2) Now do the same using conventional loops (you can choose to do this 
# before 1 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

latin_name = [x for x,y,z in birds]
print latin_name
# Gives the latin name (x) of the birds, using list comprehensions

common_name = [y for x,y,z in birds]
print common_name
# Gives the common name (y) of the birds, using list comprehensions

weight_g = [z for x,y,z in birds]
print weight_g
# Gives the weight (z) of the birds, using list comprehensions

for x,y,z in birds:
	print [x]
# Gives the latin name (x) of the birds, using loops

for x,y,z in birds:
	print [y]
# Gives the common name (y) of the birds, using loops

for x,y,z in birds:
	print [z]
# Gives the weight (z) of the birds, using loops
