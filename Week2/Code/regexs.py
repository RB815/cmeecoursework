import re

my_string = "a given string"
# find a space in the string
match = re.search(r'\s', my_string) 
#always include r as it tells the computer that it's looking for a regular expression

print match
# this should print something like
# <_sre.SRE_Match object at 0x93ecdd30>

# now we can see what has matched
match.group()

match = re.search(r's\w*', my_string)

# this sould return 'string'
match.group()

# NOW AN EXAMPLE OF NO MATCH:
# find a digit in the string
match = re.search(r'\d', my_string)

# this should print 'None'
print match

## Further Example
#
my_other_string = 'an example'
match = re.search(r'\w*\s', my_other_string)
if match:
	print 'found a match:', match.group()
else:
	print 'did not find a match'


# to know whether a pattern was matched, we can use if:

some_string = 'an example'

match = re.search(r'\w*\s', some_string)

if match:
	print 'found a match:', match.group()
else:
	print 'did not find a match'


# Some Basic Examples
match = re.search(r'\d' , "it takes 2 to tango")
print match.group() #print 2

match = re.search(r'\s\w*\s', 'once upon a time')
match.group() # ' upon '

match = re.search(r'\s\w{1,3}\s', 'once upon a time')
match.group () # ' a '

match = re.search(r'\s\w*$', 'once upon a time')
match.group() # ' time'

match = re.search(r'\w*\s\d.*\d', 'take 2 grams of H2O')
match.group() # 'take 2 grams of H2'

match = re.search(r'^\w*.*\s', 'once upon a time')
match.group() # 'once upon a '
## NOTE THAT *, +, and { } are all "greedy":
## They repeat the previous regex token as many times as possible
## As a result, they may match more text than you want

# To make it non-greedy, use ?:
match = re.search(r'^\w*.*?\s', 'once upon a time')
match.group() # 'once '

## To further illustrate greediness, let's try matching an HTML tag:
match = re.search(r'<.+>', 'This is a <EM>first</EM> test')
match.group() # '<EM>first</EM>'
## But we didn't want this: we wanted just <EM>
## It's because + is greedy!

## Instead, we can make + "lazy"!
match =re.search (r'<.+?>', 'This is a <EM>first</EM> test')
match.group() # '<EM>'

## OK, moving on from greed and laziness
match = re.search(r'\d*\.?\d*','1432.75+60.22i') # note "\" before "."
match.group() # '1432.75'

match = re.search(r'\d*\.?\d*','1432+60+221')
match.group() # '1432'

match = re.search(r'[AGTC]+', 'the sequence ATTCGT')
match.group() # 'ATTCGT'

re.search(r'\s+[A-Z]{1}\w+\s\w+', 'The bird-shit frog''s name is Theloderma asper').group() # 'Theloderma asper'
## NOTE THAT I DIRECTLY RETURNED THE RESULT BY  APPENDING .group()

string_3 = 'Rachel Balasuriya, rachel.balasuriya15@imperial.ac.uk, Computational Methods in Ecology and Evolution'
# without groups
match = re.search(r"[\w\s]*,\s[\w\.@]*,\s[\w\s&]*",string_3)

match.group()
# 'Rachel Balasuriya, rachel.balasuriya15@imperial.ac.uk, Computational Methods in Ecology and Evolution'

match.group(0)
# 'Rachel Balasuriya, rachel.balasuriya15@imperial.ac.uk, Computational Methods in Ecology and Evolution'

# now add groups using ( )
match = re.search(r"([\w\s]*),\s([\w\.@]*),\s([\w\s&]*)",string_3)

match.group(0)
# 'Rachel Balasuriya, rachel.balasuriya15@imperial.ac.uk, Computational Methods in Ecology and Evolution'

match.group(1)
# 'Rachel Balasuriya'

match.group(2)
# 'rachel.balasuriya15@imperial.ac.uk'

match.group(3)
# 'Computational Methods in Ecology and Evolution'
