#!/usr/bin/python

__author__ = 'Rachel Balasuriya (rachel.balasuriya15@imperial.ac.uk)'
__version__ = '0.0.1'

import sys # module to interface our program with the operating system

# How many times will hello be printed?
# 1)
for i in range(3, 17):
	print 'hello' # 14 times


# 2)
for j in range(12):
	if j % 3 == 0:
		print 'hello' # 4 times

# 3)
for j in range(15):
	if j % 5 == 3:
		print 'hello' # 3 times

# 4)
z = 0
while z != 15:
	print 'hello'
	z = z +3 # 5 times

# 5)
z = 12
while z < 100:
	if z == 31:
		for k in range(7):
			print 'hello'
	elif z == 18:
		print 'hello'
	z = z + 1  #8 times

# What does fooXX do?
"""Finds the squareroot of number x"""
def foo1(x=25):
	print "square root of %d is %d" % (x, x ** 0.5)
	return x ** 0.5 
	

"""Displays the larger number"""
def foo2(x=9, y=8):
	if x > y:
		print "the greatest number of %d and %d is %d" % (x, y, x)
		return x
	print "the greatest number of %d and %d is %d" % (x, y, y)
	return y

"""When x is greater than y, numbers x and y switch position, otherwise,
all numbers remain in the same sequence"""
def foo3(x=100, y=10, z=5):
	if x > y:
		tmp = y
		y = x
		x = tmp
	return [x, y, z]

"""Calculate factorial of x"""
def foo4(x=5):
	result = 1
	for i in range(1, x + 1):
		result = result * i
	print "The factorial of %d is %d)" % (x, result)
	return result # A Russian Doll of multiplication

# This is a recursive function, meaning that the function calls itself
# read about it at 
# en.wikipedia.org/wiki/Recursion_(computer_science)
"""Calcultes the recursive factorial of x"""
def foo5(x=5):
	if x == 1:
		return 1
	print "The recursive factorial of %d is %d" % (x, x * foo5(x -1))
	return x * foo5(x -1) 

	
def main(argv):
	print foo1(25)
	print foo2(9,8)
	print foo3(100,10,5)
	print foo4(5)
	print foo5(5)
	

if (__name__ == "__main__"): # make sure the "main" function is called from commandline
		status = main(sys.argv)
		sys.exit(status)
