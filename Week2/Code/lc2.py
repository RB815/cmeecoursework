#!/usr/bin/python

__author__ = 'Rachel Balasuriya (rachel.balasuriya15@imperial.ac.uk)'
__version__ = '0.0.1'

"""Months during which average UK rainfall was above 100mm and below
50mm in 1910"""

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )



# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS


highrain = [[i,j] for i,j in rainfall if j>100]
print highrain 
# List comprehension of months with rainfall over 100mm, along with the average rainfall

lowrain = [i for i,j in rainfall if j<50]
print lowrain
# List comprehension of months with average rainfall below 50mm in 1910

for i,j in rainfall:
	if j > 100:
		print (i,j)
	
# Conventional loop of month with rainfall over 100m, along with the average rainfall

for i,j in rainfall:
	if j < 50:
		print (i)
# Conventional loop of months with average rainfall below 50mm in 1910
