#!/usr/bin/python

"""Aligns two sequences, showing the best alignment and best score"""

__author__ = 'Rachel Balasuriya (rachel.balasuriya15@imperial.ac.uk)'
__version__ = '0.0.1'

import sys
import re #regex

f = open('../../Week1/Data/fasta/407228326.fasta') 
seq1_ = f.readlines()[1:] # removes header
seq1 = ''.join(seq1_) # turns the list into a string
seq1 = seq1.replace('\n', '') # removes newlines
f.close()
print(seq1)

g = open('../../Week1/Data/fasta/407228412.fasta') 
seq2_ = g.readlines()[1:]
seq2 = ''.join(seq2_)
seq2 = seq2.replace('\n', '') 
g.close()
print(seq2)


# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
    s1 = seq1
    s2 = seq2
else:
    s1 = seq2
    s2 = seq1
    l1, l2 = l2, l1 # swap the two lengths

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"

    # build some formatted output
    print "." * startpoint + matched           
    print "." * startpoint + s2
    print s1
    print score 
    print ""

    return score

calculate_score(s1, s2, l1, l2, 0)
calculate_score(s1, s2, l1, l2, 1)
calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1):
    z = calculate_score(s1, s2, l1, l2, i)
    if z > my_best_score:
        my_best_align = "." * i + s2
        my_best_score = z

#sys.stdout = open('align_seqs_fasta', 'w')

print my_best_align, s1, "Best score:", my_best_score

#with open('../Sandbox/new_file.txt', 'w') as :
#	print >f, my_best_score		
#%save a.txt _oh[68]
#f = open('../Sandbox/new_file.txt', 'w') # creates a new text file in Sandbox
#f.write('..\n')
#~ list_to_save = range(100)
#~ 
#~ f = open('../Sandbox/align_seqs_output.txt','w')
#~ for i in list_to_save:
	#~ f.write(str(i) + '\n') ## Add a new line at the end
#~ 
#~ f.close()
