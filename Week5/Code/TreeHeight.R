# This function calculates heights of trees
# from the angle of elevation and the distance
# from the base using a trigonometric formula
#
# Arguments:
# degrees   the angle of elevation
# distance  the distance from base
#
# Output:
# the height of the tree, same units as 'distance'

TreeHeight <- function(degrees, distance)
{
  radians <- degrees * pi / 180
  height <- distance * tan(radians)
  print
  return(height)
}

# 1a
Trees <- read.csv("../Data/trees.csv")

angle <- Trees [,3]
distance <- Trees [,2]

result <- TreeHeight(angle,distance)

result
# 1b
write.csv(Trees, "../Results/TreeHts.csv", row.names = FALSE)
write.table(result, file = "../Results/TreeHts.csv", append = TRUE)
