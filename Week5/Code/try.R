# try.R
# functions: try, sample, stop
           # stats::rnorm(), function(), replace, if, else, length, unique, mean, <-


## Run a simulation that involves sampling from a population

x <- stats::rnorm(50) # generates 50 numbers
doit <- function(x) {
  x <- sample(x, replace = TRUE) # replaces a sample of unique numbers with repeats
  if(length(unique(x)) > 30) {  # The lower the number the less chance of errors, and fewer error messages (25 seems to be the tipping point)
    mean(x)}
  else {
    stop("too few unique plots") # When an error occurs because we used 'stop', a message is added (manually) to tell us why
  }
}

## Try using 'try' with vectorization:
result <- lapply(1:100, function (i) try(doit(x), FALSE))

## Or using a for loop
result <- vector("list", 100) # Preallocate/Initialize
for(i in 1:100) {
  result[[i]] <- try(doit(x), FALSE) # try allows the script to keep running despite errors
}
