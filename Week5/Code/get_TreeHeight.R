# This function calculates heights of trees
# from the angle of elevation and the distance
# from the base using a trigonometric formula
#
# Arguments:
# degrees   the angle of elevation
# distance  the distance from base
#
# Output:
# the height of the tree, same units as 'distance'

argv <- commandArgs(TRUE)
Trees <- read.csv(argv[1])


TreeHeight <- function(degrees, distance)
{
  radians <- degrees * pi / 180
  height <- distance * tan(radians)
  return(height)
}


angle <- Trees [,3]
distance <- Trees [,2]
Trees$TreeHeight.m <- TreeHeight(angle,distance)

Name <- basename(argv)
Name <- (strsplit(Name, ".csv"))


write.csv(Trees, file = paste0("../Results/", Name, "_treeheights",".csv"), 
          row.names=TRUE)
# 'write.csv' to make a file
# 'file = ' to create path and name
# 'paste0' to concatenate vectors after converting to characters
# '../Results/' is the path where I want the new file to be stored
# 'Name' is the new name of the file (followed by the rest of the file name)