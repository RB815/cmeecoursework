# Title: PP_Regress.R
# Desc: Linear regression for subsets of data, with a plot displaying results.

# setwd("~/Documents/CMEECourseWork2015/Week5/Code/")

# Load ggplot & plyr
require(ggplot2)
require(plyr)

# Read in data
MyData <- read.csv("../Data/EcolArchives-E089-51-D1.csv")

# Linear regression

LM <- function(df) {

 output <- data.frame(slope = NA, intercept = NA, rsquared = NA, fstat = NA, pvalue = NA)
  
  model <-  lm(log(df$Predator.mass)~log(df$Prey.mass))
  output$slope <- model$coefficients[2]
  output$intercept <- model$coefficients[1]
  output$rsquared <- summary(model)$r.squared
  output$fstat <- summary(model)$fstatistic[1]
  output$pvalue <- anova(model)$'Pr(>F)'[1]


return(output)
}

x <- ddply(MyData, .(Type.of.feeding.interaction, Predator.lifestage), LM)

# Save dataframe to csv
write.csv(x, "../Results/PP_Regress_Results.csv")


# qplot

p <- qplot(Prey.mass, Predator.mass,
           facets = Type.of.feeding.interaction~., 
           data = MyDF, log = "xy", 
           geom = c("point", "smooth"), method = "lm",
           colour = Predator.lifestage, shape = I(3),
           fullrange = TRUE)

# ggplot
q <- ggplot(MyData, aes(x = Prey.mass, y = Predator.mass,
                        colour = Predator.lifestage, )) + 
  facet_grid(Type.of.feeding.interaction ~.) + 
  geom_point(size = I(2), shape = I(3)) + theme(legend.position = "bottom") + 
  scale_x_log10() + scale_y_log10() +
  geom_smooth(method = lm, fullrange=TRUE) +
  labs(x = "Prey Mass in grams", y = "Predator Mass in grams")   + theme(panel.background = element_rect(fill = "white", colour = "black"))

#Save graph
pdf("../Results/PP_Regress.pdf", 11.7, 8.3)
print (q)
dev.off()
