#!/bin/Rscript
# Author: Rachel Balasuriya
# Script: run_get_TreeHeight.sh
# Desc: runs get_TreeHeight.R with a given csv file
# Date: Nov 2015

#Rscript -e 'source("get_TreeHeight.R"), args <- $1'  #"../Data/TreeHeight.R"' #$1

Rscript get_TreeHeight.R ../Data/trees.csv