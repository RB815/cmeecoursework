# apply.R

##apply:
# applying the same function to rows/columns of a matrix

## Build a random matrix

M <- matrix(rnorm(100),10,10)

## Take the mean of each row
RowMeans <- apply(M, 1, mean)

## Now the variance
RowVars <- apply(M, 1, var)

## By column
ColMeans <- apply(M, 2, mean)
print(ColMeans)

## You can use it to apply your own functions
## (What does this function do?)
SomeOperation <- function(v){
  if(sum(v) > 0){
    return(v *100)
  }
  return (v)
}
print (apply(M, 1, SomeOperation))