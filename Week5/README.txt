CMEE (Computational Methods in Ecology and Evolution) Week 5 README file

***Directory Contents***
----------------------------------
README.txt -----------------------This file, which provides an overview of Week 5's directory 

/Code ----------------------------Contains shell scripts and outputs created and/or used this week. 
	apply.R-------------------Vectorize in R using *apply functions instead of for loops 
	basic_io.R----------------Read and create .csv files and tables
	break.R-------------------Break out of a loop
	browse.R------------------Debugging - check for errors within scripts
	control.R-----------------Loops in R
	get_TreeHeight.R----------Runs get_TreeHeight.R with any (appropriate) dataset and outputs results into a csv file in the Results folder
	GPPDMap.R-----------------Creates a world map with datapoints showing Lat-Long coordinates from GPDDFiltered.RData	
	next.R--------------------Skip to next iteration of a loop
	PP_Lattice.R--------------Creates lattice plots and mean, median and log mass of predator-prey mass ratio and log predator and prey mass 
	PP_Regress.R--------------Creates a linear regression for subsets of predator-prey data, with a plot displaying the results - predator lifestage
	PP_Regress_loc.R----------Creates a linear regression for subsets of predator-prey data, with a plot displaying the results - location 
	run_get_TreeHeight.sh-----Unix shell script to test get_TreeHeight.R
	sample.R------------------Random sampling from a population
	stochrickvect.R-----------Stochastic Ricker model written in a vectorized manner
	TAutoCorr.R---------------Testing whether mean annual temperatures in Florida are significantly correlated with the previous years temperatuers
	TreeHeight.R--------------A function to get the height of trees using trigonometry
	try.R---------------------Try to do something and continue (rather than stop or break out of loop) if it doesn't work
	Vectorize1.R--------------Writing a sum calculation using for loops vs. vectorised to demostrate the time difference
	Vectorize2.R--------------Stochastic Ricker model written in with loops


/Data----------------------------------------Contains all data used this week.
	EcolArchives-E089-51-D1.csv----------Predator prey dataset: used in PP_Latice.R, PP_Regress.R and PP_Regress_loc.R practicals
	GPPDFiltered.RData-------------------Common name of observed species with their latitude and longitude coordinates: used in GPPDMap.R extra credit practical	
	KeyWestAnnualMeanTemperature.RData---Annual mean temperatures from Key West, Florida during the 20th centuary: used in TAutoCorr.R practical
	Results.txt--------------------------Data used for case study 3
	trees.csv----------------------------Tree species name with distance from tree and angle between ground and top of tree: used in TreeHeight.R and run_get_TreeHeight.R practicals


/Results----------------------------Outputs from R code (.csv and .pdf)
	MyBars.pdf------------------Barplot created in case study 3	
	Girko.pdf-------------------Eclipse created in case study 2	
	MyFirst-ggplot2-Figure.pdf--GGPlot showing correlation between predator and prey mass
	MyLinReg.pdf----------------Linear regression plot created in case study 4	
	PP_Regress.pdf--------------Regression of predator-prey log mass, subset by predator lifestage and feeding type from PP_Regress.R practical
	PP_Regress_loc.pdf----------Regression of predator-prey log mass, subset by location and feeding type from PP_Regress_loc.R practical
	PP_Regress_Results.csv------Regression results from PP_Regress.R practical
	PP_Regress_Results_loc.csv--Regression results from PP_Regress_loc.R extra credit practical
	PP_Results.csv--------------Log data from PP_Lattice.R practical
	Pred_Lattice.pdf------------Lattice plot of log predator mass from PP_Lattice.R practical
	Prey_Lattice.pdf------------Lattice plot of log predator mass from PP_Lattice.R practical	
	SizeRatio_Lattice.pdf-------Lattice plot of log predator-prey mass ratio from PP_Lattice.R practical
	TAutoCorrInterp.aux---------Output from creating LaTeX document
	TAutoCorrInterp.log---------Output from creating LaTeX document
	TAutoCorrInterp.pdf---------LaTeX document summarizing temperature autocorrelation practical
	TAutoCorrInterp.tex---------My original LaTeX document before it was turned into a pdf
	TreeHts.csv-----------------Output calculated from TreeHeight.R practical
	trees_treeheights.csv-------Output calculated from get_TreeHeight.R practical


/Sandbox--------------------------Where Mr. Tinkles leaves his gifts!
	9_1_Plotting.R------------Plotting data (the basics): Scatterplots, Histograms, Subplots, Overlaying plots, Boxplots, Combining plot types, Lattice plots
	9_3_GGPlot.R--------------Plotting data with ggplot (qplot): Scatterplots, Boxplots, Histograms & density plots, Multi-faceted plots, Logarithmic axes, Plot annotations
				  Change colour, shape & size of datapoints, change column distribution & transparency of bars, add smoothers (linear regression), fullrange, 'jitter' data.
	10_Vectorization.R--------Vectorization (*apply, plyr) and random number generator
	AnotatePlot.R-------------Case study 3 - create a barplot with multiple layers and annotations
	GirkosCircle--------------Case study 2 - Girko's circular law illustrated
	MyLinReg------------------Case study 4 - Plots a linear regression with mathematical annotations and colours expressing residuals	
	PlotMatrix----------------Case study 1 - plotting a matrix	
	Ricker.R------------------The ricker model
	TAutoCorrDensity.pdf------Denisty plot created in TAutoCorr.R practical to input into LaTeX file
	TAutoCorrOutput.csv-------Testing output for autocorrelation practical


***TIPS***
#setwd("~/Documents/CMEECourseWork2015/Week5/Code")


***Contact Details***
Author: Rachel Balasuriya
e-mail: rachel.balasuriya15@imperial.ac.uk
