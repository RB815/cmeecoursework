# Basic plotting and graphical data exploration
# Predator-prey relationships in aquatic animals

# setwd("~/Documents/CMEECourseWork2015/Week5/Sandbox")

MyDF <- read.csv("../Data/EcolArchives-E089-51-D1.csv")
dim(MyDF) # check the size of the data frame you loaded

# MyDF$ 'tab' to get list of possible commands


# Scatterplots

plot(MyDF$Predator.mass,MyDF$Prey.mass)

plot(log(MyDF$Predator.mass), log(MyDF$Prey.mass))

plot(log(MyDF$Predator.mass), log(MyDF$Prey.mass), pch=20) # Change marker

plot(log(MyDF$Predator.mass), log(MyDF$Prey.mass), pch=20, xlab = "Predator Mass (kg)", ylab = "Prey Mass (kg)") # add labels


# Histograms
hist(MyDF$Predator.mass)
hist(log(MyDF$Predator.mass), xlab = " Predator Mass (kg)", ylab = "Count") # include labels
hist(log(MyDF$Predator.mass), xlab = "Predator Mass (kg)", ylab = "Count", col = "lightblue", border = "pink") # Change bar and borders colour
hist(log(MyDF$Predator.mass), xlab = expression(bold("Predator Mass (kg)")), ylab = expression(bold("Count")), col = "lightblue", border = "brown", cex.lab = 1.25)




# Subplots

pdf("../Results/Subplots", 11.7, 8.3)
par(mfcol = c(2,1)) # initialize multi-paneleed plot
par(mfg = c(1,1)) # specify which sub-plot to use first
hist(log(MyDF$Predator.mass),
     xlab = "Predator Mass (kg)", ylab = "Count",
     col = "lightblue", border = "brown",
     main = 'Predator') # add title
par(mfg = c(2,1)) # second sub-plot
hist(log(MyDF$Prey.mass),
     xlab = "Prey Mass (kg)", ylab = "Count",
     col = "lightgreen", border = "brown",
     main = 'Prey')


# Overlaying plots
graphics.off()

hist(log(MyDF$Predator.mass), # Predator histogram
     xlab = "Body Mass (kg)", ylab = "Count",
     col = rgb(1, 0, 0, 0.5), # note rgb
     main = 'Predator-prey size overlap',
     breaks = seq(-20, 20, by=2))

hist(log(MyDF$Prey.mass), col = rgb(0, 0, 1, 0.5), add = T) # plot prey

legend('topleft', c('Predators','Prey'), # add legend
       fill = c(rgb(1,0,0,0.5), rgb(0,0,1,0.5))) # define legend colours


# Boxplots

boxplot(log(MyDF$Predator.mass),
        xlab = "Location", ylab = "Predator Mass",
        main = "Predator Mass")

boxplot(log(MyDF$Predator.mass) ~ MyDF$Location, # Why the tilda? To plot by the "factor" location
        xlab = "Location", ylab = "Predator Mass",
        main = "Predator Mass by Location")

boxplot(log(MyDF$Predator.mass) ~ MyDF$Type.of.feeding.interaction,
        xlab = "Location", ylab = "Predator Mass",
        main = "Predator mass by feeding interaction type")


# Combining plot types
graphics.off()

par(fig=c(0,0.8,0,0.8), new = TRUE) # specify figure size and proportion

plot(log(MyDF$Predator.mass), log(MyDF$Prey.mass),
     xlab = "Predator Mass (kg)", ylab = "Prey Mass (kg)") # add labels and scatterplot

par(fig=c(0,0.8,0.55,1), new = TRUE)

boxplot(log(MyDF$Predator.mass), horizontal = TRUE, axes = FALSE) # adds horizontal (predator) box plot

par(fig=c(0.65,1,0,0.8), new = TRUE)

boxplot(log(MyDF$Prey.mass), axes = FALSE) # adds vertical (prey) boxplot

mtext("Fancy Predator-Prey Scatterplot", side = 3, outer = TRUE, line = -3) # adds title


# Lattice Plots

graphics.off()

library(lattice)
densityplot(~log(Predator.mass) | Type.of.feeding.interaction, data=MyDF)

    11.7, 8.3) # These numbers are page dimensions in inches
hist(log(MyDF$Predator.mass), # Plot predator histogram (note 'rgb')
     xlab="Body Mass (kg)", ylab="Count",
     col = rgb(1, 0, 0, 0.5),
     main = "Predator-Prey Size Overlap")
hist(log(MyDF$Prey.mass), # Plot prey weights
     col = rgb(0, 0, 1, 0.5),
     add = T) # Add to same plot = TRUE
legend('topleft',c('Predators','Prey'), # Add legend
       fill=c(rgb(1, 0, 0, 0.5), rgb(0, 0, 1, 0.5)))