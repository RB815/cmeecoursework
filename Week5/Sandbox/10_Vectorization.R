# Chapter 10: Advanced Topics in R
# Chapter 10.1: Vectorization
# Chapter 10.3: Generating random numbers
# functions: tapply, by, colMeans, replicate, set.seed, rnorm
           # <-, :, print, factor, rep, letters, each, sum, attach, $, runif

# tapply

x <- 1:20 # a vector

# A factor of the same length
y <- factor(rep(letters[1:5], each = 4))

# Add up the values in x within each subgroup defined by y:
tapply(x, y, sum)



# by

# Import some data
attach(iris)
print(iris)

# use colMeans (as it is better for dataframes)
by(iris[,1:2], iris$Species, colMeans)
by(iris[,1:2], iris$Petal.Width, colMeans)

by(iris[,3:4], iris$Species, colMeans)



# Replicate
print(replicate(10, runif(5)))



# Generating random numbers

set.seed(1234567) # try changing this number (can be any random sequence or single digit number)
rnorm(1) # try changing number. 
# Running this line multiple times will give a different number each time. 
# Running it with the same set.seed number will give the same 'random' number each time.
