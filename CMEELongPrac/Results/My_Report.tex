\documentclass[11pt]{article}
\usepackage{authblk} % for adding affiliation
\usepackage{setspace} % for double spaced lines
\usepackage{lineno} % for line numbers
\usepackage{gensymb} % for degrees symbol
\usepackage{graphicx} % for plots

\graphicspath{{../Results/Figs/}} % where to get plots from
\doublespacing % double spaced lines
\title{\textbf{Model fitting of thermoresponse curves}} % add title
\author[1]{Rachel Balasuriya\\} % add name
\affil[1]{Imperial College London} % add affiliation
\date{} % can leave date blank, but don't remove this line

% command to count words in document
\newcommand\wordcount{\immediate\write18{texcount -inc -incbib -sum -0 -template={SUM} My_Report.tex | grep "[0-9]" > count.txt} (\input{count.txt}words)}

\begin{document}
  \maketitle
\wordcount

\section{Introduction}
\begin{linenumbers}
Understanding the effects of temperature on growth, respiration and photosynthesis rates are vital to our comprehension of the effects of climate change. Current IPCC (International Panel on Climate Change) projections\cite{pachauri2014climate} show an increase of up 7.8\degree C from pre-industrial levels by 2100 if no mitigation occurs. Whilst studies have shown that adaptation to a rise in temperature is possible for some organisms, the projected rate of global warming may exceed that of evolutionary capability for many others\cite{hughes2003climate}, pushing them into increasingly smaller segments of suitable habitat. Further studies are required in this area \cite{gienapp2008climate} Understanding the thermodynamic responses on metabolism of individual populations and species, however, fail to demonstrate the wider impact of this global issue.

Meta-analyses are used to increase statistical power by combining results from comparable studies\cite{fagard1996advantages}. They are able to show trends in data which may not be identified by smaller scale studies\cite{fagard1996advantages}. However, caution must be taken to ensure that these analyses are robust. Models with many parameters may provide the best fit for a given dataset, but can become uninformative \cite{morris2007methods}. 

Here, a meta-analysis is conducted to compare Schoolfields' full model to two reduced Schoolfield models and a linear cubic model.

\end{linenumbers}

\section{Data}
\begin{linenumbers}
Rates of growth, respiration and photosynthesis were measured from 348 species spanning Chromista, Bacteria, Plantae, Metazoa, Viridiplantae and Fungi, along with their environmental temperatures. Both in-situ and ex-situ studies were used in the analysis, ranging in habitat from terrestrial to aquatic. Each dataset comprises a set of pseudoreplications of various lengths. Traits, trait values and temperatures were standardised for comparison.  
\subsection{Preparing Data}
Missing data was removed, along with datasets containing 5 or fewer data points to prevent overfitting\cite{cawley2007preventing}. Temperatures were converted to Kelvins for the non-linear analyses, to prevent problems of logging negative or 0\degree C values. For this same reason, trait values were increased by the lowest negative value, plus 0.01. 
\end{linenumbers}

\section{Methods}
\begin{linenumbers}
Non linear least squares (NLLS) models, such as Schoolfield models \ref{eq:1};\ref{eq:2};\ref{eq:3}, require starting values to fit the data. These were calculated in R\cite{R3} for each subset of data, using the Boltzmann constant \(k = 8.617 * 10^{-5}\). 

Full Schoolfield model:
\begin{equation}\label{eq:1}
B = \frac{B_0 e^{\frac{-E}{k} (\frac{1}{T} - \frac{1}{283.15})}}{1 + e^{\frac{E_l}{k} (\frac{1}{T_l} - \frac{1}{T})} + e^{\frac{E_h}{k} (\frac{1}{T_h} - \frac{1}{T})}}
\end{equation}

Reduced Schoolfield model (high):
\begin{equation}\label{eq:2}
B = \frac {B_{0}e^ \frac{-E}{k} ( \frac{1}{T} - \frac{1}{283.15})}{1+e^ \frac{E_{h}}{k}(\frac{1}{T_{h}}- \frac{1}{T})}
\end{equation}

Reduced Schoolfield model (low):
\begin{equation}\label{eq:3}
B = \frac {B_{0}e^ \frac{-E}{k} ( \frac{1}{T} - \frac{1}{283.15})}{1+e^ \frac{E_{l}}{k}(\frac{1}{T_{l}}- \frac{1}{T})}
\end{equation}

B0 was determined as the trait value with a temperature closest to 10\degree C. \(T_{h}\) is the peak temperature, set as the highest temperature within the subset. \(T_{l}\) is the lowest temperature within the subset. \(E\) (activation energy) was calculated as the slope of the logged arrhenius equation, or 0.65 as default. \(E_{l}\) is the low temperature inactivation energy, calculated as \({E}/{2}\). \(E_{h}\) is the high temperature deactivation energy , calculated as \(E*5\). \(T\) is the temperature in kelvins. 

The cubic model (\(B = B_{0} + B_{1}T + B_{2}T^2 + B_{3}T^3\)) was also calculated and fit R.

Python\cite{PER-GRA:2007} was used to reduce the NLLS models, fit parameters and calculate the AIC and BIC values for model selection. Models were then fit to the orginal data in R and the AIC/ BIC values compared to determine the best fitting model. 

\end{linenumbers}

\section{Results}
\begin{linenumbers}

\begin{figure}[h]
\caption{Example of NLLS models fit to a subset of data (ID = 57)}
\includegraphics{57.png}
\end{figure}

\begin{table}[h!]
\centering
\begin{tabular}{| c | c c c |}
\hline
 & AIC counts & BIC counts & AIC+BIC \\[0.5ex]
 \hline \hline
Schoolfield full & 397 & 389 & 786 \\
\hline
Schoolfield high & 348 & 350 & 698 \\
\hline
Schoolfield low & 298 & 304 & 602 \\
\hline
Cubic & 70 & 70 & 140 \\ [1ex]
\hline
\end{tabular}
\caption{Total best AIC and BIC values for each model}
\end{table}

\end{linenumbers}

\section{Discussion}
\begin{linenumbers}
The results show that the full Schoolfield model has the best fit for most of the datasets. It could be thought that the full Schoolfield model is the most appropriate model to use for metaanalyses of thermal responses. However, whilst BIC does penalise larger numbers of parameters, there is still the risk of overfitting the data with this model due to a small number of datapoints in some datasets relative to the number of parameters. 
\end{linenumbers}

\section{References}
\begin{linenumbers}

\bibliographystyle{plain}
\bibliography{Biblio}

\end{linenumbers}
\end{document}

