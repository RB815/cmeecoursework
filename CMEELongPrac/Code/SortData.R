rm(list = ls())
graphics.off()
# setwd("~/Documents/CMEECourseWork2015/CMEELongPrac/Code/")
library(plyr)
library(ggplot2)

# Import metadata
ThermRespData <- read.csv("../Data/GrowthRespPhotoData.csv") 

# Remove datasets with 5 or fewer data points
ThermoData <- ThermRespData[ThermRespData$FinalID %in% names(which(table(ThermRespData$FinalID) > 5)), ] 

# Create a new dataframe with columns I want
ThermoData <- data.frame(ThermoData$FinalID, ThermoData$OriginalTraitName, ThermoData$OriginalTraitValue, ThermoData$ConTemp,
                         ThermoData$Latitude, ThermoData$Habitat, ThermoData$ConSpecies, ThermoData$ConKingdom)

ThermoData <- na.omit(ThermoData) # remove na's

# Insert a unique number for each entry
ThermoData <- transform(ThermoData, ID=match(ThermoData$ThermoData.FinalID, unique(ThermoData$ThermoData.FinalID)))

# deal with minus and 0 values by making them positive
rmMinus <- function(x){
    if (min(x$ThermoData.OriginalTraitValue) <= 0){
      x$ThermoData.OriginalTraitValue <- 
      x$ThermoData.OriginalTraitValue + (0-min(x$ThermoData.OriginalTraitValue)) + 1
    }
  return(data.frame(x$ThermoData.OriginalTraitValue))
}

# apply changes to make all trait values in dataset positive
Minus <- ddply(ThermoData, "ThermoData.FinalID", rmMinus)
ThermoData$ThermoData.OriginalTraitValue <- Minus$x.ThermoData.OriginalTraitValue

# Convert temps to Kelvins
k <- ThermoData$ThermoData.ConTemp[1] + 273.15
for (i in 2:length(ThermoData[,1])){
k[i] <- ThermoData$ThermoData.ConTemp[i] + 273.15
}
ThermoData$Kelvins <- k

# create starting values
startval <- function(TR){
  k <- 8.617e-05 # 8.617 * 10 ^ (-5)
  B0 <- TR$ThermoData.OriginalTraitValue[which.min(abs(TR$Kelvins - 283.15))] # Trait value at temp closest to 283.15K
  T_h <- TR$Kelvins[which.max(TR$ThermoData.OriginalTraitValue)] # Temperature from the largest trait value
  
  S <- subset(TR, TR$Kelvins <= T_h) # subset data so to look at start of curve only
  
  if(length(unique(S$Kelvins))>1) { # needs more than one unique temperature value within the subset to calculate E
    x <- 1/(k*S$Kelvins) # set x axis as 1 over k * temperature
    y <- log(S$ThermoData.OriginalTraitValue) # set y axis as the trait values logged
    arrheniusplot <- summary(lm(y~x)) # a linear model of x and y gives the arrhenius plot. The summary contains the coefficients
    E <- abs(arrheniusplot$coefficients[2]) # abs gives positive number. Activation energy! 
  } else {
    E <- 0.65
  }
    
  E_h <- E*5 # high temp deactivation energy
  MinTraitVal <- which.min(TR$ThermoData.OriginalTraitValue)
  T_l <- TR$Kelvins[MinTraitVal] # lowest point
  E_l <- E/2 #low temp deactivation energy
  return(data.frame(TR$ThermoData.OriginalTraitValue,B0, T_h, T_l, E, E_h, E_l))
}

newdf <- ddply(ThermoData, "ID", startval)
ThermoData$B0 <- newdf$B0
ThermoData$E <- newdf$E
ThermoData$E_l <- newdf$E_l
ThermoData$E_h <- newdf$E_h
ThermoData$T_l <- newdf$T_l
ThermoData$T_h <- newdf$T_h
names(ThermoData)[1] <- "Finalid"
names(ThermoData)[2] <- "TraitName"
names(ThermoData)[3] <- "TraitValue"
names(ThermoData)[4] <- "ConTemp"
names(ThermoData)[5] <- "Latitude"
names(ThermoData)[6] <- "Habitat"
names(ThermoData)[7] <- "Species"
names(ThermoData)[8] <- "Kingdom"

write.csv(ThermoData, file = "../Data/ExtraData.csv")

