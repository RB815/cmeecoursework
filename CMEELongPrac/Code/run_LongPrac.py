#!/usr/bin/env python
""" Python script to run whole project:
	Data Preparation in R
	NLLS fitting in Python
	Graph plotting in R 
	Compiling Latex script in bash"""
__author__ = 'Rachel Balasuriya (rachel.balasuriya15@imperial.ac.uk)'
__version__ = '0.0.1'

import subprocess

subprocess.os.system("rstudio SortData.R")
subprocess.os.system("ipython ThermalResponse_SchoolFull.py ExtraData.csv")
subprocess.os.system("ipython ThermalResponse_SchoolHigh.py ExtraData.csv")
subprocess.os.system("ipython ThermalResponse_SchoolLow.py ExtraData.csv")
subprocess.os.system("rstudio GetPlots.R")
subprocess.os.system("bash CompileLaTeX.sh My_Report")
