#!/usr/bin/env python
""" This code performs non-linear least squares fitting of different
unimodal functions to experimental thermal response curves."""
__author__ = 'Rachel Balasuriya (rbalasuriya15@imperial.ac.uk)'
__version__ = '0.0.1'

# Scroll down to the section called "MAIN CODE" first.

import sys
import numpy as np
import scipy as sc 
from lmfit import Minimizer, minimize, Parameters, Parameter, report_fit, fit_report
import pandas as pd
import csv

#############################
# F  U  N  C  T  I  O  N  S #
#############################

def schoolf_eq(Temps, B0, E, E_l, T_l,E_h, T_h):
	"""Full schoolfield model for calculating trait values at a given temperature"""
	function = (B0 * np.exp((-E/k) * ((1/Temps)-(1/283.15))))/ (1 + np.exp((E_l/k) * (1/T_l - 1/Temps)) + np.exp((E_h/k) * (1/T_h - 1/Temps)))
	
	return function

def schoolf(params, Temps, data):
	"""Schoolfield model, to be called by schoolfield_model()"""

	B0 = params['B0'].value
	E = params['E'].value
	E_l = params['E_l'].value
	E_h = params['E_h'].value
	T_l = params['T_l'].value
	T_h = params['T_h'].value
	
	ModelPred = schoolf_eq(Temps, B0, E, E_l, T_l, E_h, T_h)
	return(ModelPred - data)

def schoolfield_model(dataset):
	"""NLLS fitting to the Schoolfield model; this function will 
	contain the lmfit.minimize calls to the schoolf() function. This is 
	where you can constrain the parameters."""

#	 Prepare the parameters and their bounds:
	B0_start = dataset['B0'].iloc[0]
	E_start = dataset['E'].iloc[0]
	E_l_start = dataset['E_l'].iloc[0]
	E_h_start = dataset['E_h'].iloc[0]
 	T_l_start = dataset['T_l'].iloc[0]
	T_h_start = dataset['T_h'].iloc[0]
 
#	 The datase containing temperatures and trait values.
	TraitVals = dataset['TraitValue'].values
	Temps = dataset['Kelvins'].values

#	 Define parameters
	params = Parameters()
	params.add('B0', value=B0_start, vary = True, min = -10, max = 1000)
	params.add('E', value= E_start, vary= True, min=0.0000001, max=20)
	params.add('E_l', value=E_l_start, vary = True, min=0.00000001, max=20)
	params.add('E_h', value=E_h_start, vary = True, min=0.00000001, max=59.502971163)
	params.add('T_l', value=T_l_start, vary = True, min=0.0000001, max=350.9354)
	params.add('T_h', value=T_h_start, vary = True, min=0.00000001, max=400.1500)
	
#	 Minimising the Model
	out = minimize(schoolf, params, args=(Temps, TraitVals),method="leastsq")
	par_out_school = out.params
#	 Calculates the r squared value to determine how well the model fits the data.
	r_squared_school = 1-out.residual.var()/sc.var(TraitVals)
	RSS = out.residual.var()
	nvarys_school= out.nvarys
	ndata_school = out.ndata
	return(par_out_school, r_squared_school, nvarys_school, ndata_school, out.chisqr, RSS)
	
	
def AICrss(n, k, rss):
	"""Calculate the Akaike Information Criterion value, using:

	- n:   number of observations
	- k:   number of parameters
	- rss: residual sum of squares
	"""
	return n * np.log((2 * sc.pi) / n) + n + 2 + n * np.log(rss) + 2 * k

def BICrss(n, k, rss):
	"""Calculate the Bayesian Information Criterion value, using:
	
	- n:   number of observations
	- k:   number of parameters
	- rss: residual sum of squares
	"""
	return n + n * np.log(2 * sc.pi) + n * np.log(rss / n) + (np.log(n)) * (k + 1)

#~ ############################
#~ # M  A  I  N    C  O  D  E #
#~ ############################

def main(argv):
	"""Performs fitting to the Gaussian-Gompertz, Schoolfield and Cubic model,
	and returns the best fits as a csv file to ./Results/results.csv"""

	# Define the Boltzmann constant (units of eV * K^-1).
	global k
	k = 8.617 * 10 ** (-5)

	# import data
	data = pd.read_csv("../Data/" + argv)
	
	iD = []
	B0 = []
	E = []
	E_l = []
	E_h = []
	T_l = []
	T_h = []
	AIC = []
	BIC = []
	R2 = []
	
	for i in range(max(data.ID)+1):
		newdata = data[data.ID == i]
		try:
			results = schoolfield_model(newdata)
				
			AICr = AICrss(results[3],results[2],results[5])
			BICr = BICrss(results[3],results[2],results[5])
			
			iD.append(i)
			B0.append(results[0]["B0"].value)
			E.append(results[0]["E"].value)
			E_l.append(results[0]["E_l"].value)
			E_h.append(results[0]["E_h"].value)
			T_l.append(results[0]["T_l"].value)
			T_h.append(results[0]["T_h"].value)
			R2.append(results[1])
			AIC.append(AICr)
			BIC.append(BICr)

		except:
			pass	
			
	df = pd.DataFrame({"iD": iD, "B0": B0, "E": E, "E_h": E_h, "E_l": E_l, "T_h": T_h, "T_l": T_l, "AIC": AIC, "BIC": BIC, "R2": R2})					
	

	#~ print df

	#df = pd.DataFrame(columns = ["i", "B0", "E", "E_l", "E_h", "T_l", "T_h"])
	df.to_csv("../Data/ParamsSchoolFull2.csv", sep = ",")
	
if __name__ == "__main__":
	#The input file name will be the minimum input, but you can add more inputs if you want 
	main(sys.argv[1])		
