CMEE (Computational Methods in Ecology and Evolution) Week 1 README file

***Directory Contents***

README.txt -----------------------This file, which provides an overview of Week 1's directory 

/Code ----------------------------Contains shell scripts and outputs created and/or used this week. 
	boilerplate.sh------------This is a boilerscript. It just prints out a specified sentence. (-e enables special characters to be used within the sentence). $ bash <file.sh>
	CompileLaTeX.sh-----------Creates a pdf by compiling the .tex and .bib files. This shell script also removes unnecessary byproducts which are listed in the script. $ bash <file.sh> <file>
	ConcatenateTwoFiles.sh----Combines two files. $ bash <file.sh>
	CountLines.sh-------------Counts number of lines in file. $ bach <file.sh> <File>
	csvtospace.sh-------------Converts files with data separated by commas to a new file with data separated by spaces. $ bash <file.sh> <relative/path/File.old> <relative/path/File.new>
	FirstBiblio.bib-----------A reference imported from GoogleScholar which is used in FirstExample.pdf
	FirstExample.bb-----------Byproduct of pdf
	FirstExample.blg----------Byproduct of pdf
	FirstExample.pdf----------The end product of FirstExample. and FirstBiblio.bib
	FirstExample.tex----------An example of a file which was later converted to a pdf.
	MyExampleScript.sh--------A friendly script which says hello to the user. $ bash <file.sh> 
	tabtocsv.sh---------------Converts all the tabs in a file into commas and saves this as a csv file.
	UnixPrac1.txt-------------Contains answers to Week1 Practical questions
	variables.sh--------------Calculates the sum of two variables

/Data-----------------------------Contains all data used this week.
	/fasta--------------------Contains fasta files taken from cmee2015masterepo. These were used for Week 1's Practical
		407228326.fasta
		407228412.fasta
		AT
		E.coli.fasta
		GC
	spawannxs.txt-------------File taken from http://www.cep.unep.org/pubs/legislation/spawannxs.txt. Used for practicising grep commands
	/Temperatures-------------Contains fasta files taken from cmee2015masterepo. These were used for Week 1's Practical
		1800.csv  
		1801.csv  
		1802.csv  
		1803.csv
		
/sandbox--------------------------A fun place to play!
	ListRootDir.txt
	/TestFind-----------------Contains a bunch of empty files, just for practicing creating and finding files.
		/Dir1
			/Dir11
				/Dir111
					File111.txt
			File1.csv
			File1.tex
			File1.txt
		/Dir2
			File2.csv
			File2.txt
			File.tex
		/Dir3
			File3.txt
	test.txt--------------------The result of redirecting and appending text in terminal
	/TestWild-------------------Another bunch of empty files, used to practice using wildcards
		Anotherfile.csv
		File1.csv
		File2.csv
		File3.csv
		File4.csv
		Anotherfile.txt
		File1txt
		File2.txt
		File3.txt
		File4.txt


***TIPS***

Bash scripts: 
Ensure that you are in the Code directory before running bash scripts.
Type $ bash <file.sh> <optional files or commands> in terminal to run shell script

***Contact Details***
Author: Rachel Balasuriya
e-mail: rachel.balasuriya15@imperial.ac.uk
