#!bin/bash
# Author: RB815 rachel.balasuriya15@imperial.ac.uk
# Script: tabtoccsv.sh
# Desc: sustitute the tabs in the files with commas
#		saves the output into a .csv file
# Date: Oct 2015

echo "Creating a comma delimited version of $1 ..."

cat $1 | tr -s "\t" "," >> $1.csv

echo "Done!"

exit
