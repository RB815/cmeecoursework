#!bin/bash
# Author: RB815 rachel.balasuriya15@imperial.ac.uk
# Script: csvtospace.sh
# Desc: sustitute the commas in the files with spaces
#		saves the output as a new file
# Date: Oct 2015

echo "Creating a space delimited version of $1 ..."

cat $1 | tr -s "," " " > $2


# bash csvtospace.sh <file.csv> <file.txt>
# include relative path before file names if in a different directory
