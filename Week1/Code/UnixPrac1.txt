find . -type f | xargs wc -l 
#a. find . -type f finds all the files in the fasta directory. xargs wc -l counts the number of lines WITHIN the files

cat E.coli.fasta | sed 1d
#b. cat E.coli.fasta prints everything in the file. sed 1d ignores the first line

tail -n +2 E.coli.fasta | tr -d '\n' | wc -m 
#c. tail -n +2 E.coli.fasta ignores the first line of the E.coli file. tr -d '\n' prevents newlines from being included as characters. wc -m counts the number of characters

tr -d '\n' | grep -o "ATGC" E.coli.fasta | tail -n +2 | wc -l 
#d. tr -d '\n' removes newlines which distrupt the pattern. grep -o "ATGC" E.coli.fasta puts the sequence we're looking for in the file order. tail -n +2 removes the first line of the file from being included. wc -l counts the number of lines (occurances)
 
Var1=$(grep 'A\|T' E.coli.fasta | tail -n +2 | wc -l) | Var2=$(grep 'G\|C' E.coli.fasta | tail -n +2 | wc -l) echo $Var1 $Var2 | awk '{print $1/$2}'
#e.  Var1=$ adds the first variable (which will be the total number of A's and T's in the E.coli file.
