# Statistics Practical Three
# setwd("~/Documents/CMEECourseWork2015/Week4/Code")

# Graphics in R

plantGrowth <- read.delim("../Data/ipomopsis.csv")
str(plantGrowth)
## 'data.frame':	40 obs. of  3 variables:
## $ Root   : num  6.22 6.49 4.92 5.13 5.42 ...
## $ Fruit  : num  59.8 61 14.7 19.3 34.2 ...
## $ Grazing: Factor w/ 2 levels "Grazed","Ungrazed": 2 2 2 2 2 2 2 2 2 2 ...

# Histograms
pdf("../Results/3a_Histograms", 11.7, 8.3)
hist(plantGrowth$Fruit)

hist(plantGrowth$Fruit, xlab = "Fruit production (dry weight, mg)", main = "", breaks = 20) 
# data can be equally divided by any given number

hist(plantGrowth$Fruit, xlab = "Fruit production (dry weight, mg)", main = "", breaks = 10)

hist(plantGrowth$Fruit, xlab = "Fruit production (dry weight, mg)", main = "", breaks = seq(0, 125, by = 25)) 
# intervals can be specified

# Density measure (rather than frequency)
hist(plantGrowth$Fruit, xlab = "Fruit production (dry weight, mg)", main = "", breaks = seq(0, 125, by = 25), freq = FALSE)
dev.off()

# Box plots
pdf("../Results/3b_BoxPlots", 11.7, 8.3)

plot(x = plantGrowth$Grazing, y = plantGrowth$Fruit)

plot(Fruit ~ Grazing, data = plantGrowth) # Labels can be added from the dataset

plot(Fruit ~ Grazing, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Rabbit grazing treatments") 
# More informative labels can be added using xlab and ylab

dev.off()

# Scatterplots
pdf("../Results/3c_Scatterplots", 11.7, 8.3)

plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter (mm)")

plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter (mm)", pch = 16) 
# pch (plot character) can be changed with symbols 1-25 

plot(Fruit ~ Root, data = plantGrowth, subset = Grazing == "Grazed", ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter (mm)", pch = 1)
points(Fruit ~ Root, data = plantGrowth, subset = Grazing == "Ungrazed", pch =19)

# Correcting the axis
xRange <- range(plantGrowth$Root) # gets the min and max values of Root
plot(Fruit ~ Root, data = plantGrowth, subset = Grazing == "Grazed", ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter (mm)", xlim = xRange, pch = 1) # xlim sets the axis correctly
points(Fruit ~ Root, data = plantGrowth, subset = Grazing == "Ungrazed", pch = 19)

# Assigning values to pch
  # Change symbol
plantGrowth$plotChar <- 1
plantGrowth$plotChar[plantGrowth$Grazing == "Ungrazed"] <- 19
plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter (mm)", pch = plotChar)

  # Change to assigned letter
plantGrowth$plotText <- "G"
plantGrowth$plotText[plantGrowth$Grazing == "Ungrazed"] <- "U"
plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter (mm)", pch = plotText)

  # Add a touch of colour
plantGrowth$plotCol <- "red"
plantGrowth$plotCol[plantGrowth$Grazing == "Ungrazed"] <- "darkblue"
plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter (mm)", pch = plotChar, col = plotCol)

dev.off()

# Barplots
pdf("../Results/3_BarPlots", 11.7, 8.3)

fruitMean <- with(plantGrowth, tapply(Fruit, Grazing, mean))
fruitSD <- with(plantGrowth, tapply(Fruit, Grazing, sd))

barplot(fruitMean, ylab = "Mean fruit production (dry weight, mg)", xlab = "Rabbit grazing treatment")

 # adding 'error bars'
xMids <- barplot(fruitMean, ylab = "Mean fruit production (dry weight, mg)", xlab = "Rabbit grazing treatment")
arrows(x0 = xMids, y0 = fruitMean - fruitSD, x1 = xMids, y1 = fruitMean + fruitSD, ang = 90, code = 3)

yLim <- c(0, max(fruitMean + fruitSD))
xMids <- barplot(fruitMean, ylab = "Mean fruit production (dry weight, mg)", xlab = "Rabbit grazing treatment", ylim = yLim)
arrows(x0 = xMids, y0 = fruitMean - fruitSD, x1 = xMids, y1 = fruitMean + fruitSD, ang = 90, code = 3)


# Fine tuning graphics
par("mar") # check margin sizes
## [1] 5.1 4.1 4.1 2.1  # read bottom, left, top, right
par(mar = c(4.5,4.5,1.1,1.0)) # make them smaller

dev.off()

# Multiple

# example layout
par(mar=c(3,3,1,1), mfrow=c(3,2))


# Save my plot

pdf(file = "../Results/myPlot.pdf", width = 6, height = 4, pointsize = 16)
plot(1:10)
title(main = "My pdf of some dull numbers")
dev.off()
