# Statistics Practical Eight
# setwd("~/Documents/CMEECourseWork2015/Week4/Code/")

# Model Criticism and Simplification

decay <- read.delim("../Data/decay.txt")

pdf("../Results/8a_LinearReg", 11.7, 8.3)

plot(mass ~ duration, data = decay, xlab = "Burial duration (days)", ylab = "Remaining mass (g)")
decayMod <- lm(mass ~ duration, data = decay)
summary(decayMod)
# # Call:
# #   lm(formula = mass ~ duration, data = decay)
# # 
# # Residuals:
# #   Min      1Q  Median      3Q     Max 
# # -19.065 -10.029  -2.058   5.107  40.447 
# # 
# # Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)    
# # (Intercept)  84.5534     5.0277   16.82  < 2e-16 ***
# #   duration     -2.8272     0.2879   -9.82 9.94e-11 ***
# #   ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 14.34 on 29 degrees of freedom
# # Multiple R-squared:  0.7688,	Adjusted R-squared:  0.7608 
# # F-statistic: 96.44 on 1 and 29 DF,  p-value: 9.939e-11

abline(decayMod)
dev.off()


pdf("../Results/8b_DiagnosticPlots", 11.7, 8.3)
par(mfrow = c(2,2))

plot(decayMod)
dev.off()

par(mfrow = c(1,1))
decay$logMass <- log10(decay$mass)

pdf("../Results/8c_LogLinearReg", 11.7, 8.3)
plot(logMass ~ duration, data = decay, xlab = "Burial duration(days)", ylab = "Log remaining mass (g)")
logDecayMod <- lm(logMass ~ duration, data = decay)
summary(logDecayMod)
# # Call:
# #   lm(formula = logMass ~ duration, data = decay)
# # 
# # Residuals:
# #   Min       1Q   Median       3Q      Max 
# # -0.25776 -0.08874  0.00291  0.09547  0.27349 
# # 
# # Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)    
# # (Intercept)  1.974905   0.043558   45.34  < 2e-16 ***
# #   duration    -0.029762   0.002494  -11.93 1.04e-12 ***
# #   ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 0.1242 on 29 degrees of freedom
# # Multiple R-squared:  0.8308,	Adjusted R-squared:  0.825 
# # F-statistic: 142.4 on 1 and 29 DF,  p-value: 1.038e-12

abline(logDecayMod)
dev.off()


pdf("../Results/8d_LogDiagnosticPlots", 11.7, 8.3)

par(mfrow = c(2,2))
plot(logDecayMod)
dev.off()
  
  # (Changing dataset...)
graphics.off()
birdMass <- read.delim("../Data/birdMass.txt")
str(birdMass)
# # 'data.frame':	30 obs. of  2 variables:
# # $ mass   : num  11.2 15.9 17.1 19.7 18.9 ...
# # $ species: Factor w/ 3 levels "Species A","Species B",..: 1 1 1 1 1 1 1 1 1 1 ...

pdf("../Results/8e_BoxPlot", 11.7, 8.3)

plot(mass ~ species, data = birdMass)
dev.off()

birdMod <- lm(mass ~ species, data = birdMass)
summary(birdMod)
# # Call:
# #   lm(formula = mass ~ species, data = birdMass)
# # 
# # Residuals:
# #   Min     1Q Median     3Q    Max 
# # -5.636 -2.145  0.019  2.216  5.079 
# # 
# # Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)    
# # (Intercept)       15.8760     0.9508  16.697 9.36e-16 ***
# #   speciesSpecies B   3.4100     1.3447   2.536   0.0173 *  
# #   speciesSpecies C   6.3150     1.3447   4.696 6.89e-05 ***
# #   ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 3.007 on 27 degrees of freedom
# # Multiple R-squared:  0.4501,	Adjusted R-squared:  0.4094 
# # F-statistic: 11.05 on 2 and 27 DF,  p-value: 0.0003116

  # (Changing dataset...)
species <- read.delim("../Data/species.txt")

pdf("../Results/8f_SpeciesCounts")
plot(Species ~ Area, data = species, xlab = "Sampling area", ylab = "Number of species")
speciesAreaMod <- lm(Species ~ Area, data = species)
summary(speciesAreaMod)
# # Call:
# #   lm(formula = Species ~ Area, data = species)
# # 
# # Residuals:
# #   Min       1Q   Median       3Q      Max 
# # -2.85010 -0.55049  0.00738  0.51393  2.25026 
# # 
# # Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)    
# # (Intercept) 4.798788   0.175588   27.33   <2e-16 ***
# #   Area        0.051311   0.003019   17.00   <2e-16 ***
# #   ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 0.8714 on 98 degrees of freedom
# # Multiple R-squared:  0.7467,	Adjusted R-squared:  0.7441 
# # F-statistic: 288.9 on 1 and 98 DF,  p-value: < 2.2e-16

abline(speciesAreaMod)
dev.off()


pdf("../Results/8g_SpeciesDiagnosticPlots", 11.7, 8.3)
par(mfrow = c(2,2))
plot(speciesAreaMod)
dev.off()

  # (Changing dataset...)
desertRats <- read.delim("../Data/desertRats.txt")

pdf("../Results/8h_Pairs", 11.7, 8.3)

pairs(desertRats)
dev.off()

maxMod <- lm(rodent ~ rain + predators + cover + seed, data = desertRats)
summary(maxMod)
# # Call:
# #   lm(formula = rodent ~ rain + predators + cover + seed, data = desertRats)
# # 
# # Residuals:
# #   Min      1Q  Median      3Q     Max 
# # -2.6856 -1.2743  0.2832  0.6472  3.4896 
# # 
# # Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)   
# # (Intercept) -4.297654   2.520517  -1.705   0.1162   
# # rain         1.365145   0.375235   3.638   0.0039 **
# #   predators   -0.003237   0.310467  -0.010   0.9919   
# # cover        0.233255   0.295734   0.789   0.4469   
# # seed         0.779619   0.503049   1.550   0.1495   
# # ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 1.977 on 11 degrees of freedom
# # Multiple R-squared:  0.8897,	Adjusted R-squared:  0.8496 
# # F-statistic: 22.18 on 4 and 11 DF,  p-value: 3.195e-05

simpMod1 <- update(maxMod, . ~ . -predators) # alter maxMod so that all (.) are a factor of (~) all (.) variables, except (-) predators
summary(simpMod1)
# # Call:
# #   lm(formula = rodent ~ rain + cover + seed, data = desertRats)
# # 
# # Residuals:
# #   Min      1Q  Median      3Q     Max 
# # -2.6806 -1.2743  0.2804  0.6489  3.4886 
# # 
# # Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)    
# # (Intercept)  -4.3145     1.8533  -2.328 0.038213 *  
# #   rain          1.3629     0.2953   4.616 0.000594 ***
# #   cover         0.2340     0.2736   0.855 0.409070    
# # seed          0.7826     0.3970   1.971 0.072191 .  
# # ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 1.893 on 12 degrees of freedom
# # Multiple R-squared:  0.8897,	Adjusted R-squared:  0.8621 
# # F-statistic: 32.27 on 3 and 12 DF,  p-value: 5.024e-06

anova(maxMod, simpMod1)
# # Analysis of Variance Table
# # 
# # Model 1: rodent ~ rain + predators + cover + seed
# # Model 2: rodent ~ rain + cover + seed
# # Res.Df    RSS Df   Sum of Sq     F Pr(>F)
# # 1     11 42.988                            
# # 2     12 42.989 -1 -0.00042474 1e-04 0.9919

simpMod2 <- update(maxMod, . ~ . - predators - cover)
summary(simpMod2)
# # Call:
# #   lm(formula = rodent ~ rain + seed, data = desertRats)
# # 
# # Residuals:
# #   Min      1Q  Median      3Q     Max 
# # -2.8101 -0.9396  0.1155  0.7081  3.8931 
# # 
# # Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)    
# # (Intercept)  -3.3756     1.4780  -2.284 0.039823 *  
# #   rain          1.2960     0.2818   4.600 0.000498 ***
# #   seed          0.8152     0.3910   2.085 0.057378 .  
# # ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 1.873 on 13 degrees of freedom
# # Multiple R-squared:  0.883,	Adjusted R-squared:  0.865 
# # F-statistic: 49.04 on 2 and 13 DF,  p-value: 8.786e-07

anova(maxMod, simpMod1, simpMod2)
# # Analysis of Variance Table
# # 
# # Model 1: rodent ~ rain + predators + cover + seed
# # Model 2: rodent ~ rain + cover + seed
# # Model 3: rodent ~ rain + seed
# # Res.Df    RSS Df Sum of Sq      F Pr(>F)
# # 1     11 42.988                           
# # 2     12 42.989 -1  -0.00042 0.0001 0.9919
# # 3     13 45.610 -1  -2.62144 0.6708 0.4302

simpMod3 <- update(maxMod, . ~ . - predators - cover - seed)
summary(simpMod3)
# # Call:
# #   lm(formula = rodent ~ rain, data = desertRats)
# # 
# # Residuals:
# #   Min      1Q  Median      3Q     Max 
# # -2.9640 -1.2198  0.0244  0.5905  4.7831 
# # 
# # Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)    
# # (Intercept)  -1.2657     1.1989  -1.056    0.309    
# # rain          1.7471     0.2009   8.698  5.1e-07 ***
# #   ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 2.085 on 14 degrees of freedom
# # Multiple R-squared:  0.8439,	Adjusted R-squared:  0.8327 
# # F-statistic: 75.66 on 1 and 14 DF,  p-value: 5.103e-07

anova(maxMod, simpMod1, simpMod2, simpMod3)
# # Analysis of Variance Table
# # 
# # Model 1: rodent ~ rain + predators + cover + seed
# # Model 2: rodent ~ rain + cover + seed
# # Model 3: rodent ~ rain + seed
# # Model 4: rodent ~ rain
# # Res.Df    RSS Df Sum of Sq      F  Pr(>F)  
# # 1     11 42.988                              
# # 2     12 42.989 -1   -0.0004 0.0001 0.99187  
# # 3     13 45.610 -1   -2.6214 0.6708 0.43017  
# # 4     14 60.858 -1  -15.2484 3.9018 0.07386 .
# # ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

nullMod <- update(maxMod, . ~ 1)
anova(maxMod, simpMod3, nullMod)
# # Analysis of Variance Table
# # 
# # Model 1: rodent ~ rain + predators + cover + seed
# # Model 2: rodent ~ rain
# # Model 3: rodent ~ 1
# # Res.Df    RSS Df Sum of Sq       F    Pr(>F)    
# # 1     11  42.99                                   
# # 2     14  60.86 -3    -17.87  1.5242    0.2629    
# # 3     15 389.75 -1   -328.89 84.1581 1.738e-06 ***
# #   ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

  # (Changing dataset..)
graphics.off()
competition <- read.delim("../Data/competition.txt")

pdf("../Results/8i_BoxPlot", 11.7, 8.3)

plot(biomass ~ clipping, data = competition, ylab = "Plant biomass (g)", xlab = "Root clipping treatment")
dev.off()

fullMod <- lm(biomass ~ clipping, data = competition)

competition$clipSimp <- competition$clipping
levels(competition$clipSimp)
# # [1] "control" "n25"     "n50"     "r10"     "r5"     

simpMod <- lm(biomass ~ clipSimp, data = competition)
anova(fullMod, simpMod)
# # Analysis of Variance Table
# # 
# # Model 1: biomass ~ clipping
# # Model 2: biomass ~ clipSimp
# # Res.Df    RSS Df Sum of Sq F Pr(>F)
# # 1     25 124020                      
# # 2     25 124020  0         0   

competition$clipSimp2 <- competition$clipping
levels(competition$clipSimp2) <- c("control", "n", "n", "r", "r") #$
simpMod2 <- lm(biomass ~ clipSimp2, data = competition)
anova(fullMod, simpMod)
# # Analysis of Variance Table
# # 
# # Model 1: biomass ~ clipping
# # Model 2: biomass ~ clipSimp
# # Res.Df    RSS Df Sum of Sq F Pr(>F)
# # 1     25 124020                      
# # 2     25 124020  0         0  

competition$clipSimp3 <- competition$clipping
levels(competition$clipSimp3) <- c("control", "manip", "manip", "manip", "manip") #$
simpMod3 <- lm(biomass ~clipSimp3, data = competition)
anova(fullMod, simpMod3)
# # Analysis of Variance Table
# # 
# # Model 1: biomass ~ clipping
# # Model 2: biomass ~ clipSimp3
# # Res.Df    RSS Df Sum of Sq      F Pr(>F)
# # 1     25 124020                           
# # 2     28 139342 -3    -15322 1.0295 0.3965

anova(simpMod3)
# # Analysis of Variance Table
# # 
# # Model 1: biomass ~ clipping
# # Model 2: biomass ~ clipSimp3
# # Res.Df    RSS Df Sum of Sq      F Pr(>F)
# # 1     25 124020                           
# # 2     28 139342 -3    -15322 1.0295 0.3965
# # > anova(simpMod3)
# # Analysis of Variance Table
# # 
# # Response: biomass
# # Df Sum Sq Mean Sq F value    Pr(>F)    
# # clipSimp3  1  70035   70035  14.073 0.0008149 ***
# #   Residuals 28 139342    4976                      
# # ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1