# Statistics Practical Two
# setwd("~/Documents/CMEECourseWork2015/Week4/Code")

# Data Entry and Manipulation

# Using the from:to function (simply prints numbers x to y, with each number increasing or decreasing by 1)
1:10
## [1] 1 2 3 4 5 6 7 8 9 10

-4:4
## [1] -4 -3 -2 -1 0 1 2 3 4

12:3
## [1] 12 11 10 9 8 7 6 5 4 3


# Using the seq() function - More flexible than simply using the colon
seq(from = 1, to = 10)
## [1] 1 2 3 4 5 6 7 8 9 10

seq(from = 1, to = 20, by = 2) # prints numbers 1 to 10 in intervals of 2
## [1] 1 3 5 7 9 11 13 15 17 19

seq(from = 1, to = 3, length = 10) # prints 10 numbers at regular intervals between 1 and 3
## [1] 1.00 1.22 1.44 1.67 1.89 2.11 2.33 2.56 2.78 3.00


# Using the rep() function
rep(x = 1:4, times = 2) # prints the whole vector (1:4) twice
## [1] 1 2 3 4 1 2 3 4

rep(x = 1:4, each = 2) # prints each value twice
## [1] 1 1 2 2 3 3 4 4

rep(x = 1:4, times = 1:4) # prints 1 once, 2 twice etc.
## [1] 1 2 2 3 3 3 4 4 4 4


# creating a factor
myCharacterVector <- c ("low", "low", "low", "low", "high", "high", "high", "high")
myFactor <- as.factor(myCharacterVector)    # Shows the values as vectors
str(myFactor)
##Factor w/ 2 levels "high","low": 2 2 2 2 1 1 1 1


# using gl() to create factors
myFactor <- gl(n = 2, k = 4)    
# gl() generates levels, so the example will give 2 levels, each containing 4 values (like above)
print(myFactor)
## [1] 1 1 1 1 2 2 2 2
## Levels: 1 2

myFactor <- gl(n= 2, k = 4, labels = c("low", "high"))    
# labels can also be added to the values to give them meaning
print(myFactor)
## [1] low  low  low  low  high high high high
## Levels: low high

myFactor <- gl(n = 2, k = 2, length = 8, labels = c("low","high"))    
# The length of the factor can also be altered
print(myFactor)
## [1] low low high high low low high high
## Levels: low high


# create some data vectors
myNumericVector <- c(1.3, 2.5, 1.9, 3.4, 5.6, 1.4, 3.1, 2.9)
myCharacterVector <- c("low", "low", "low", "low","high", "high", "high", "high")
myLocalVector <- c(TRUE, TRUE, FALSE, FALSE, TRUE,TRUE, FALSE, FALSE)

# join them into columns of a data frame
myData <- data.frame(num = myNumericVector, char = myCharacterVector, logical = myLogicalVector)
str(myData)

## 'data.frame': 8 obs. of 3 variables:
## $ num: num 1.3 2.5 1.9 3.4 5.6 1.4 3.1 2.9
## $ char: Factor w/ 2 levels "high","low": 2 2 2 2 1 1 1 1
## $ logical: logi TRUE TRUE FALSE FALSE TRUE TRUE ...

print (myData)

##    num char  logical
## 1  1.3 low   TRUE
## 2  2.5 low   TRUE
## 3  1.9 low   FALSE
## 4  3.4 low   FALSE
## 5  5.6 high  TRUE
## 6  1.4 high  TRUE
## 7  3.1 high  FALSE
## 8  2.9 high  FALSE


# Don't try to turn our character vector into a factor
myData <- data.frame(num = myNumericVector, char = myCharacterVector, logical = myLogicalVector, stringsAsFactors = FALSE)
str(myData)

## 'data.frame': 8 obs. of 3 variables:
## $ num: num 1.3 2.5 1.9 3.4 5.6 1.4 3.1 2.9
## $ char: chr "low" "low" "low" "low" ...
## $ logical: logi TRUE TRUE FALSE FALSE TRUE TRUE ...

print(myData)

##  num   char  logical
## 1 1.3  low    TRUE
## 2 2.5  low    TRUE
## 3 1.9  low   FALSE
## 4 3.4  low   FALSE
## 5 5.6 high    TRUE
## 6 1.4 high    TRUE
## 7 3.1 high   FALSE
## 8 2.9 high   FALSE


  # (Change dataset...)
factories <- read.csv("../Data/factories.csv")
str(factories)

## 'data.frame':	35 obs. of  3 variables:
## $ factory  : Factor w/ 7 levels "Doncaster","Gwent",..: 5 5 5 5 5 3 3 3 3 3 ...
## $ treatment: Factor w/ 5 levels "A","B","C","D",..: 1 2 3 4 5 1 2 3 4 5 ...
## $ rate     : num  10.08 11.94 9.35 14.56 12.79 ...

summary(factories)

##       factory  treatment      rate       
## Doncaster :5   A:7       Min.   : 6.370  
## Gwent     :5   B:7       1st Qu.: 9.905  
## Newport   :5   C:7       Median :12.430  
## Rotherham :5   D:7       Mean   :12.573  
## Sheffield :5   E:7       3rd Qu.:14.835  
## Sunderland:5             Max.   :19.240  
## Worksop   :5

rate    # Will give an error because columns in a data frame are not automatically accessible

## Error object 'rate' not found

# Rate is contained within factories
factories$rate
## [1] 10.08 11.94  9.35 14.56 12.79 15.21 16.95 14.96 19.24 17.66  7.91  8.10  6.37 11.95  9.40 10.82 12.20  9.73 14.71 12.82 14.01 15.89
## [23] 13.15 18.87 16.76  8.70  9.61  7.69 12.43 10.09 11.67 13.12 10.56 16.32 14.45


# Use indices to get to the rate column (if the data is sorted and we know what rows we're looking for)
factories[1:5, 3]    # one way of extracting the data
## [1] 10.08 11.94  9.35 14.56 12.79

factories$rate[1:5]    # another way of extracting the data
## [1] 10.08 11.94  9.35 14.56 12.79


# using logical tests to select rows (if we don't know the row numbers)
factories$factory == "Sheffield"
## [1]  TRUE  TRUE  TRUE  TRUE  TRUE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
## [23] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE

factories$rate[factories$factory == "Sheffield"]
## [1] 10.08 11.94  9.35 14.56 12.79

factories$treatment == "C"
## [1] FALSE FALSE  TRUE FALSE FALSE FALSE FALSE  TRUE FALSE FALSE FALSE FALSE  TRUE FALSE FALSE FALSE FALSE  TRUE FALSE FALSE FALSE FALSE
## [23]  TRUE FALSE FALSE FALSE FALSE  TRUE FALSE FALSE FALSE FALSE  TRUE FALSE FALSE

factories$rate[factories$treatment == "C"]
## [1]  9.35 14.96  6.37  9.73 13.15  7.69 10.56


# label the country for each row
factories$country <-"England"

# change the label to Wales for the factories in Gwent and Newport
factories$country[factories$factory == "Gwent"] <- "Wales"
factories$country[factories$factory == "Newport"] <- "Wales" #£
str(factories)

## 'data.frame':	35 obs. of  4 variables:
## $ factory  : Factor w/ 7 levels "Doncaster","Gwent",..: 5 5 5 5 5 3 3 3 3 3 ...
## $ treatment: Factor w/ 5 levels "A","B","C","D",..: 1 2 3 4 5 1 2 3 4 5 ...
## $ rate     : num  10.08 11.94 9.35 14.56 12.79 ...
## $ country  : chr  "England" "England" "England" "England" ...


# Attach() command (makes local copies of each column outside of the data frame, allowing them to be use directly)
attach(factories)
rate[treatment == "C"]     # a simpler way of getting the rates data for treatment C
##[1]  9.35 14.96  6.37  9.73 13.15  7.69 10.56

rate[factory == "Sheffield"]
##[1] 10.08 11.94  9.35 14.56 12.79

detach(factories)    
# good practice to 'detach' data otherwise we could end up with multiple versions of the data

with(factories, rate[factory == "Sheffield"])    
# a temporary attachment which only applies to this line (so, no need to detach)
## [1] 10.08 11.94  9.35 14.56 12.79  


# Using subset (a way of extacting subsets of data without too many $'s)
welshData <- subset(factories, subset = country == "Wales") 
str(welshData)
## 'data.frame':	10 obs. of  4 variables:
## $ factory  : Factor w/ 7 levels "Doncaster","Gwent",..: 3 3 3 3 3 2 2 2 2 2
## $ treatment: Factor w/ 5 levels "A","B","C","D",..: 1 2 3 4 5 1 2 3 4 5
## $ rate     : num  15.2 16.9 15 19.2 17.7 ...
## $ country  : chr  "Wales" "Wales" "Wales" "Wales" ...

welshRates <- subset(factories, subset = country == "Wales", select = rate)
str(welshRates)
## 'data.frame':	10 obs. of  1 variable:
## $ rate: num  15.2 16.9 15 19.2 17.7 ...
