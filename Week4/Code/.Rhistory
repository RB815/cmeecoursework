myMat <- matrix(1:12, ncol = 3)
myMat[2, ] # gives data in the 2nd row of the matrix
## [1]  2  6 10
myMat[ ,3] # gives data in the 3rd column of the matrix
## [1]  9 10 11 12
myMat[3,2] # gives data in 3rd row down, 2nd column across
[1] 7
myMat[3,2] <- -99 # replaces the data in row 3, column 2 with the number -99
myMat[,3] <- 33 # replaces the whole of column 3 with the number 33
myMat
##      [,1] [,2] [,3]
## [1,]    1    5   33
## [2,]    2    6   33
## [3,]   33  -99   33
## [4,]    4    8   33
# Chi squared
hairEyes <- matrix(c(34, 59, 3, 10, 42, 47), ncol = 2, dimnames =list(Hair = c("Black", "Brown", "Blond"), Eyes = c("Brown", "Blue")))
hairEyes
##        Eyes
## Hair    Brown Blue
## Black    34   10
## Brown    59   42
## Blond     3   47
rowTot <- rowSums(hairEyes)
colTot <- colSums(hairEyes)
tabTot <- sum(hairEyes)
Expected <- outer(rowTot, colTot)/tabTot
Expected
##          Brown     Blue
## Black 21.66154 22.33846
## Brown 49.72308 51.27692
## Blond 24.61538 25.38462
cellChi <- (hairEyes - Expected)^2/Expected
tabChi <- sum(cellChi)
tabChi
## [1] 54.63907 # Chi-squared value
1 - pchisq(tabChi, df = 2) # gives the p value from our chi squared test
## [1] 1.365463e-12
hairChi <- chisq.test(hairEyes)
print(hairChi)
## 	Pearson's Chi-squared test
## data:  hairEyes
## X-squared = 54.6391, df = 2, p-value = 1.365e-12
str(hairChi)
## List of 9
## $ statistic: Named num 54.6
## ..- attr(*, "names")= chr "X-squared"
## $ parameter: Named int 2
## ..- attr(*, "names")= chr "df"
## $ p.value : num 1.37e-12
## $ method
## : chr "Pearson's Chi-squared test"
## $ data.name: chr "hairEyes"
## $ observed : num [1:3, 1:2] 34 59 3 10 42 47
## ..- attr(*, "dimnames")=List of 2
## .. ..$ Hair: chr [1:3] "Black" "Brown" "Blond"
## .. ..$ Eyes: chr [1:2] "Brown" "Blue"
## $ expected : num [1:3, 1:2] 21.7 49.7 24.6 22.3 51.3 ...
## ..- attr(*, "dimnames")=List of 2
## .. ..$ Hair: chr [1:3] "Black" "Brown" "Blond"
## .. ..$ Eyes: chr [1:2] "Brown" "Blue"
## $ residuals: num [1:3, 1:2] 2.65 1.32 -4.36 -2.61 -1.3 ...
## ..- attr(*, "dimnames")=List of 2
## .. ..$ Hair: chr [1:3] "Black" "Brown" "Blond"
## .. ..$ Eyes: chr [1:2] "Brown" "Blue"
## $ stdres
## : num [1:3, 1:2] 4.23 2.66 -7.09 -4.23 -2.66 ...
## ..- attr(*, "dimnames")=List of 2
## .. ..$ Hair: chr [1:3] "Black" "Brown" "Blond"
## .. ..$ Eyes: chr [1:2] "Brown" "Blue"
## - attr(*, "class")= chr "htest"
hairChi$residuals #$
##        Eyes
## Hair        Brown      Blue
## Black  2.651041 -2.610564
## Brown  1.315603 -1.295517
## Blond -4.356720  4.290201
?fisher.test # for data in a 2x2 table or with counts smaller than 5
par(mfrow = c(2,2))
# Probability distributions
dt(0.5, df = 19) # density (height of distribution for a particular t value)
## [1] 0.3454832
pt(0.5, df = 19) # probability of getting t value or smaller
## [1] 0.6885918
qt(0.8, df = 19) # quantiles of probability
## [1] 0.8609506
par(mfrow = c(1,1))
randT <- rt(n = 1000, df = 19) # number of random values drawn from the t distribution
hist(randT) # histogram of randT
# t test
x <- c(6, 4, 2, 5, 4, 4, 5, 6, 3, 3, 4, 6, 8, 5, 6, 6, 5, 4, 8, 6)
xN <- length(x)
xMean <- mean(x)
xSS <- sum((x - xMean)^2)
xVar <- xSS/(xN - 1)
xSE <- sqrt(xVar/xN)
xT <- (5 - 4)/xSE
print(xT)
par(mfrow = 2,2)
# Probability distributions
dt(0.5, df = 19) # density (height of distribution for a particular t value)
## [1] 0.3454832
pt(0.5, df = 19) # probability of getting t value or smaller
## [1] 0.6885918
qt(0.8, df = 19) # quantiles of probability
## [1] 0.8609506
par(mfrow = c(1,1))
randT <- rt(n = 1000, df = 19) # number of random values drawn from the t distribution
hist(randT) # histogram of randT
# t test
x <- c(6, 4, 2, 5, 4, 4, 5, 6, 3, 3, 4, 6, 8, 5, 6, 6, 5, 4, 8, 6)
setwd("~/Documents/CMEECourseWork2015/Week4/Code")
graphics.off()
# Linear models
# Box plot to compare data distribution for each treatment
competition <- read.delim("../Data/competition.txt")
str(competition)
## 'data.frame':	30 obs. of  2 variables:
## $ biomass : int  551 457 450 731 499 632 595 580 508 583 ...
## $ clipping: Factor w/ 5 levels "control","n25",..: 2 2 2 2 2 2 3 3 3 3 ...
summary(competition)
##     biomass         clipping
## Min.   :415.0   control:6
## 1st Qu.:508.8   n25    :6
## Median :568.0   n50    :6
## Mean   :561.8   r10    :6
## 3rd Qu.:631.8   r5     :6
## Max.   :731.0
# Fit linear model
par(mfrow = c(1,1))
plot(biomass ~ clipping, data = competition, ylab = "Plot biomass (g)", xlab = "Root clipping treatment")
competModel <- aov(biomass ~ clipping, data = competition)
pdf("../Results/6_BoxPlot", 11.7, 8.3)
plot(biomass ~ clipping, data = competition, ylab = "Plot biomass (g)", xlab = "Root clipping treatment")
dev.off()
competModel <- aov(biomass ~ clipping, data = competition)
print(competModel) # Summary of how the sums of squares of the biomass data are split between residual variation and that explained by clipping
summary(competModel)
summary.lm(competModel)
competMean <- with(competition, tapply(biomass, INDEX = clipping, FUN = mean))
print(competMean)
seFun <- function(x) {sqrt(var(x)/length(x))}
competSEMean <- with(competition, tapply(biomass, INDEX = clipping, FUN = seFun))
print(competSEMean)
competTukeyHSD <- TukeyHSD(competModel)
competTukeyHSD
plot(competTukeyHSD)
midpoints <- barplot(competMean, xlab = "Root competition treatment", ylab = "Plant biomass (g)", ylim = c(0,700))
arrows(midpoints, competMean - competSEMean, midpoints, competMean + competSEMean, code = 3, angle = 90)
# Kruskal-Wallis test
kruskal.test(biomass ~ clipping, data = competition)
tannin <- read.delim("../Data/tannin.txt")
str(tannin)
## 'data.frame':	9 obs. of  2 variables:
## $ growth: int  12 10 8 11 6 7 2 3 3
## $ tannin: int  0 1 2 3 4 5 6 7 8
summary(tannin)
##      growth           tannin
## Min.   : 2.000   Min.   :0
## 1st Qu.: 3.000   1st Qu.:2
## Median : 7.000   Median :4
## Mean   : 6.889   Mean   :4
## 3rd Qu.:10.000   3rd Qu.:6
## Max.   :12.000   Max.   :8
plot(growth ~ tannin, data = tannin, xlab = "Tannin concentration", ylab = "Growth (mg)")
tanninMod <- lm(growth ~ tannin, data = tannin)
summary.aov(tanninMod)
tanninMod <- lm(growth ~ tannin, data = tannin)
summary.aov(tanninMod)
summary(tanninMod)
abline(tanninMod)
source('~/Documents/CMEECourseWork2015/Week4/Code/PracticalSix.R', echo=TRUE)
pdf("../Results/6_KruskalWallis")
plot(growth ~ tannin, data = tannin, xlab = "Tannin concentration", ylab = "Growth (mg)")
tanninMod <- lm(growth ~ tannin, data = tannin)
summary.aov(tanninMod)
##             Df Sum Sq Mean Sq F value   Pr(>F)
##  tannin       1  88.82   88.82   30.97 0.000846 ***
##  Residuals    7  20.07    2.87
##  ---
##  Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
summary(tanninMod)
## Call:
## lm(formula = growth ~ tannin, data = tannin)
##
## Residuals:
##   Min      1Q  Median      3Q     Max
## -2.4556 -0.8889 -0.2389  0.9778  2.8944
##
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)
## (Intercept)  11.7556     1.0408  11.295 9.54e-06 ***
##  tannin       -1.2167     0.2186  -5.565 0.000846 ***
##  ---
##  Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
##
## Residual standard error: 1.693 on 7 degrees of freedom
## Multiple R-squared:  0.8157,	Adjusted R-squared:  0.7893
## F-statistic: 30.97 on 1 and 7 DF,  p-value: 0.0008461
# Add line to graph
abline(tanninMod)
dev.off()
pdf("../Results/4_Histogram", 11.7, 8.3)
par(mfrow = c(1,1))
randT <- rt(n = 1000, df = 19) # number of random values drawn from the t distribution
hist(randT) # histogram of randT
dev.off()
par(mfrow = c(1, 4))
plot(ozone ~ garden, data = ozoneDat, ylab = "[Ozone] (ppm)", xlab = "Garden")
with(ozoneDat, hist(ozone[garden == "A"], main = "Garden A", xlab = "[Ozone] (ppm)"))
with(ozoneDat, hist(ozone[garden == "B"], main = "Garden B", xlab = "[Ozone] (ppm)"))
with(ozoneDat, hist(ozone[garden == "C"], main = "Garden C", xlab = "[Ozone] (ppm)"))
A <- ozoneDat$ozone[ozoneDat$garden == "A"]
B <- ozoneDat$ozone[ozoneDat$garden == "B"]
t.test(A, B)
pdf("../Results/5_oZone", 11.7, 8.3)
par(mfrow = c(1, 4))
plot(ozone ~ garden, data = ozoneDat, ylab = "[Ozone] (ppm)", xlab = "Garden")
with(ozoneDat, hist(ozone[garden == "A"], main = "Garden A", xlab = "[Ozone] (ppm)"))
with(ozoneDat, hist(ozone[garden == "B"], main = "Garden B", xlab = "[Ozone] (ppm)"))
with(ozoneDat, hist(ozone[garden == "C"], main = "Garden C", xlab = "[Ozone] (ppm)"))
dev.off()
pdf("../Results/5_Ozone", 11.7, 8.3)
par(mfrow = c(1, 4))
plot(ozone ~ garden, data = ozoneDat, ylab = "[Ozone] (ppm)", xlab = "Garden")
with(ozoneDat, hist(ozone[garden == "A"], main = "Garden A", xlab = "[Ozone] (ppm)"))
with(ozoneDat, hist(ozone[garden == "B"], main = "Garden B", xlab = "[Ozone] (ppm)"))
with(ozoneDat, hist(ozone[garden == "C"], main = "Garden C", xlab = "[Ozone] (ppm)"))
dev.off()
pdf("../Results/5_Kicksample", 11.7, 8.3)
par(mfrow = c(1, 1))
plot(Upstream ~ Downstream, data = kicksample)
with(kicksample, abline(v = mean(Downstream), h = mean(Upstream), lty = 2))
with(kicksample, cor.test(Upstream, Downstream))
with(kicksample, cor.test(Upstream, Downstream, method = "spearman"))
with(kicksample, cor.test(Upstream, Downstream, method = "kendall"))
dev.off()
daphnia <- read.delim("../Data/daphnia.txt")
summary(daphnia)
par(mfrow = c(1, 2))
plot(Growth.rate ~ Detergent, data = daphnia)
plot(Growth.rate ~ Daphnia, data = daphnia)
seFun <- function(x) {sqrt(var(x)/length(x))}
detergentMean <- with(daphnia, tapply(Growth.rate, INDEX = Detergent,FUN = mean))
detergentSEM <- with(daphnia, tapply(Growth.rate, INDEX = Detergent,FUN = seFun))
cloneMean <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = mean))
cloneSEM <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = seFun))
pdf("../Results/7_BoxPlot")
plot(Growth.rate ~ Detergent, data = daphnia)
plot(Growth.rate ~ Daphnia, data = daphnia)
dev.off()
par(mfrow = c(2,1), mar = c(4,4,1,1))  # adjust margins and layout of graphs
barMids <- barplot(detergentMean, xlab = "Detergent type", ylab = "Population growth rate", ylim = c(0,5)) # create bar chart
arrows(barMids, detergentMean - detergentSEM, barMids, detergentMean + detergentSEM, code = 3, angle = 90) # adds error bars
# add second graph
barMids <- barplot(cloneMean, xlab = "Daphnia clone", ylab = "Population growth rate", ylim = c(0,5))
arrows(barMids, cloneMean - cloneSEM, barMids, cloneMean + cloneSEM, code = 3, angle = 90)
pdf("../Results/7_BoxPlot", 11.7, 8.3)
plot(Growth.rate ~ Detergent, data = daphnia)
plot(Growth.rate ~ Daphnia, data = daphnia)
dev.off()
pdf("../Results/7_BarPlot", 11.7, 8.3)
par(mfrow = c(2,1), mar = c(4,4,1,1))  # adjust margins and layout of graphs
barMids <- barplot(detergentMean, xlab = "Detergent type", ylab = "Population growth rate", ylim = c(0,5)) # create bar chart
arrows(barMids, detergentMean - detergentSEM, barMids, detergentMean + detergentSEM, code = 3, angle = 90) # adds error bars
# add second graph
barMids <- barplot(cloneMean, xlab = "Daphnia clone", ylab = "Population growth rate", ylim = c(0,5))
arrows(barMids, cloneMean - cloneSEM, barMids, cloneMean + cloneSEM, code = 3, angle = 90)
dev.off()
par(mfrow = c(1, 2))
par(mfrow = c(2,1), mar = c(4,4,1,1))  # adjust margins and layout of graphs
daphniaMod <- aov(Growth.rate ~ Detergent + Daphnia, data = daphnia)
summary(daphniaMod)
summary.lm(daphniaMod) # more details...
detergentMean - detergentMean[1] # comparisons of the mean to BrandA
cloneMean - cloneMean[1]
daphniaModHSD <- TukeyHSD(daphniaMod)
daphniaModHSD
par(mfrow = c(1,2), mar = c(4,8,4,4), las = 1) # mfrow puts 2 graphs side by side; mar adjusts margins; las adjusts orientation of axis labels to horizontal
plot(daphniaModHSD)
timber <- read.delim("../Data/timber.txt")
summary(timber)
pairs(timber)
pdf("../Results/7_Pairs", 11.7, 8.3)
pairs(timber)
dev.off()
timberMod <- lm(volume ~ girth + height, data = timber)
summary.aov(timberMod)
summary(timberMod)
plantGrowth <- read.delim("../Data/ipomopsis.csv")
summary(plantGrowth)
plantMod <- lm(Fruit ~ Root + Grazing, data = plantGrowth)
anova(plantMod)
summary(plantMod)
plantGrowth$plotChar <- 1 #$
plantGrowth$plotChar[plantGrowth$Grazing == "Ungrazed"] <- 19
plantGrowth$plotCol <- "red" #$
plantGrowth$plotCol[plantGrowth$Grazing == "Ungrazed"] <- "darkblue"
plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter(mm)", pch = plotChar, col = plotCol)
abline(a = -127.829, b = 23.56, col = "red")
abline(a = -127.829 + 36.103, b = 23.56, col = "darkblue")
pdf("../Results/ScatterPlot", 11.7, 8.3)
plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter(mm)", pch = plotChar, col = plotCol)
abline(a = -127.829, b = 23.56, col = "red")
abline(a = -127.829 + 36.103, b = 23.56, col = "darkblue")
dev.off()
pdf("../Results/7_ScatterPlot", 11.7, 8.3)
plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter(mm)", pch = plotChar, col = plotCol)
abline(a = -127.829, b = 23.56, col = "red")
abline(a = -127.829 + 36.103, b = 23.56, col = "darkblue")
dev.off()
pdf("../Results/7_TukeyHSD", 11.7, 8.3)
plot(daphniaModHSD)
dev.off()
decay <- read.delim("../Data/decay.txt")
plot(mass ~ duration, data = decay, xlab = "Burial duration (days)", ylab = "Remaining mass (g)")
decayMod <- lm(mass ~ duration, data = decay)
summary(decayMod)
abline(decayMod)
par(mfrow = c(2,2))
plot(decayMod)
pdf("../Results/8_LinearReg", 11.7, 88.3)
plot(mass ~ duration, data = decay, xlab = "Burial duration (days)", ylab = "Remaining mass (g)")
decayMod <- lm(mass ~ duration, data = decay)
summary(decayMod)
abline(decayMod)
dev.off()
plot(decayMod)
par(mfrow = c(1,1))
decay$logMass <- log10(decay$mass)
plot(logMass ~ duration, data = decay, xlab = "Burial duration(days)", ylab = "Remaining mass (g)")
logDecayMod <- lm(logMass ~ duration, data = decay)
summary(logDecayMod)
pdf("../Results/8_DiagnosticPlots")
plot(decayMod)
dev.off()
pdf("../Results/8_DiagnosticPlots", 11.7, 8.3)
plot(decayMod)
dev.off()
decay$logMass <- log10(decay$mass)
plot(logMass ~ duration, data = decay, xlab = "Burial duration(days)", ylab = "Remaining mass (g)")
logDecayMod <- lm(logMass ~ duration, data = decay)
summary(logDecayMod)
abline(logDecayMod)
par(mfrow = c(2,2))
birdMass <- read.delim("../Data/birdMass.txt")
str(birdMass)
plot(mass ~ species, data = birdMass)
birdMod <- lm(mass ~ species, data = birdMass)
summary(birdMod)
species <- read.delim("../Data/species.txt")
plot(Species ~ Area, data = species, xlab = "Sampling area", ylab = "Number of species")
speciesAreaMod <- lm(Species ~ Area, data = species)
summary(speciesAreaMod)
decay <- read.delim("../Data/decay.txt")
pdf("../Results/8_LinearReg", 11.7, 88.3)
plot(mass ~ duration, data = decay, xlab = "Burial duration (days)", ylab = "Remaining mass (g)")
decayMod <- lm(mass ~ duration, data = decay)
summary(decayMod)
abline(decayMod)
dev.off()
par(mfrow = c(2,2))
pdf("../Results/8_DiagnosticPlots", 11.7, 8.3)
plot(decayMod)
dev.off()
par(mfrow = c(1,1))
decay$logMass <- log10(decay$mass)
plot(logMass ~ duration, data = decay, xlab = "Burial duration(days)", ylab = "Remaining mass (g)")
logDecayMod <- lm(logMass ~ duration, data = decay)
summary(logDecayMod)
abline(logDecayMod)
dev.off()
decay <- read.delim("../Data/decay.txt")
pdf("../Results/8_LinearReg", 11.7, 88.3)
plot(mass ~ duration, data = decay, xlab = "Burial duration (days)", ylab = "Remaining mass (g)")
decayMod <- lm(mass ~ duration, data = decay)
summary(decayMod)
abline(decayMod)
dev.off()
decay <- read.delim("../Data/decay.txt")
pdf("../Results/8_LinearReg", 11.7, 8.3)
plot(mass ~ duration, data = decay, xlab = "Burial duration (days)", ylab = "Remaining mass (g)")
decayMod <- lm(mass ~ duration, data = decay)
summary(decayMod)
abline(decayMod)
dev.off()
pdf("../Results/8a_LinearReg", 11.7, 8.3)
plot(mass ~ duration, data = decay, xlab = "Burial duration (days)", ylab = "Remaining mass (g)")
decayMod <- lm(mass ~ duration, data = decay)
summary(decayMod)
abline(decayMod)
dev.off()
par(mfrow = c(2,2))
pdf("../Results/8a_DiagnosticPlots", 11.7, 8.3)
plot(decayMod)
dev.off()
pdf("../Results/8b_DiagnosticPlots", 11.7, 8.3)
plot(decayMod)
dev.off()
par(mfrow = c(1,1))
decay$logMass <- log10(decay$mass)
pdf("../Results/LogLinearReg", 11.7, 8.3)
plot(logMass ~ duration, data = decay, xlab = "Burial duration(days)", ylab = "Remaining mass (g)")
logDecayMod <- lm(logMass ~ duration, data = decay)
summary(logDecayMod)
abline(logDecayMod)
dev.off()
pdf("../Results/8c_LogLinearReg", 11.7, 8.3)
plot(logMass ~ duration, data = decay, xlab = "Burial duration(days)", ylab = "Remaining mass (g)")
logDecayMod <- lm(logMass ~ duration, data = decay)
summary(logDecayMod)
abline(logDecayMod)
dev.off()
pdf("../Results/8c_LogLinearReg", 11.7, 8.3)
plot(logMass ~ duration, data = decay, xlab = "Burial duration(days)", ylab = "Log remaining mass (g)")
logDecayMod <- lm(logMass ~ duration, data = decay)
summary(logDecayMod)
abline(logDecayMod)
dev.off()
par(mfrow = c(2,2))
plot(logDecayMod)
pdf("../Results/8d_LogDiagnosticPlots", 11.7, 8.3)
plot(logDecayMod)
dev.off()
birdMass <- read.delim("../Data/birdMass.txt")
str(birdMass)
plot(mass ~ species, data = birdMass)
par(mfrow = c(1,1))
plot(mass ~ species, data = birdMass)
par(mfrow = c(2,2))
plot(logDecayMod)
graphics.off()
birdMass <- read.delim("../Data/birdMass.txt")
str(birdMass)
plot(mass ~ species, data = birdMass)
pdf("../Results/8e_BoxPlot", 11.7, 8.3)
plot(mass ~ species, data = birdMass)
dev.off()
species <- read.delim("../Data/species.txt")
plot(Species ~ Area, data = species, xlab = "Sampling area", ylab = "Number of species")
speciesAreaMod <- lm(Species ~ Area, data = species)
summary(speciesAreaMod)
abline(speciesAreaMod)
par(mfrow = c(2,2))
plot(speciesAreaMod)
pdf("../Results/SpeciesDiagnosticPlots", 11.7, 8.3)
par(mfrow = c(2,2))
plot(speciesAreaMod)
dev.off()
pdf("../Results/8g_SpeciesDiagnosticPlots", 11.7, 8.3)
par(mfrow = c(2,2))
plot(speciesAreaMod)
dev.off()
pdf("../Results/8f_SpeciesCounts")
plot(Species ~ Area, data = species, xlab = "Sampling area", ylab = "Number of species")
speciesAreaMod <- lm(Species ~ Area, data = species)
summary(speciesAreaMod)
abline(speciesAreaMod)
dev.off()
pdf("../Results/8d_LogDiagnosticPlots", 11.7, 8.3)
par(mfrow = c(2,2))
plot(logDecayMod)
dev.off()
pdf("../Results/8b_DiagnosticPlots", 11.7, 8.3)
par(mfrow = c(2,2))
plot(decayMod)
dev.off()
desertRats <- read.delim("../Data/desertRats.txt")
pairs(desertRats)
maxMod <- lm(rodent ~ rain + predators + cover + seed, data = desertRats)
summary(maxMod)
simpMod1 <- update(maxMod, . ~ . -predators) # alter maxMod so that all (.) are a factor of (~) all (.) variables, except (-) predators
summary(simpMod1)
anova(maxMod, simpMod1)
simpMod2 <- update(maxMod, . ~ . - predators - cover)
summary(simpMod2)
anova(maxMod, simpMod1, simpMod2)
simpMod3 <- update(maxMod, . ~ . - predators - cover - seed)
summary(simpMod3)
anova(maxMod, simpMod1, simpMod2, simpMod3)
nullMod <- update(maxMod, . ~ 1)
anova(maxMod, simpMod3, nullMod)
pdf("../Results/8h_Pairs", 11.7, 8.3)
pairs(desertRats)
dev.off()
competition <- read.delim("../Data/competition.txt")
plot(biomass ~ clipping, data = competition, ylab = "Plant biomass (g)", xlab = "Root clipping treatment")
fullMod <- lm(biomass ~ clipping, data = competition)
competition$clipSimp <- competition$clipping
levels(competition$clipSimp)
graphics.off()
competition <- read.delim("../Data/competition.txt")
plot(biomass ~ clipping, data = competition, ylab = "Plant biomass (g)", xlab = "Root clipping treatment")
fullMod <- lm(biomass ~ clipping, data = competition)
competition$clipSimp <- competition$clipping
levels(competition$clipSimp)
# # [1] "control" "n25"     "n50"     "r10"     "r5"
simpMod <- lm(biomass ~ clipSimp, data = competition)
anova(fullMod, simpMod)
competition$clipSimp2 <- competition$clipping
levels(competition$clipSimp2) <- c("control", "n", "n", "r", "r") #$
simpMod2 <- lm(biomass ~ clipSimp2, data = competition)
anova(fullMod, simpMod)
competition$clipSimp3 <- competition$clipping
levels(competition$clipSimp3) <- c("control", "manip", "manip", "manip", "manip") #$
simpMod3 <- lm(biomass ~clipSimp3, data = competition)
anova(fullMod, simpMod3)
anova(simpMod3)
pdf("../Results/8i_BoxPlot", 11.7, 8.3)
plot(biomass ~ clipping, data = competition, ylab = "Plant biomass (g)", xlab = "Root clipping treatment")
dev.off()
load("/home/gjp15/Documents/CMEECourseWork2015/Week4/Data/PracticalOne.RData")
