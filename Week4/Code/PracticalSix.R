# Statistics Practical Six

 setwd("~/Documents/CMEECourseWork2015/Week4/Code")

# Linear models

   # Box plot to compare data distribution for each treatment
competition <- read.delim("../Data/competition.txt")
str(competition)
## 'data.frame':	30 obs. of  2 variables:
## $ biomass : int  551 457 450 731 499 632 595 580 508 583 ...
## $ clipping: Factor w/ 5 levels "control","n25",..: 2 2 2 2 2 2 3 3 3 3 ...

summary(competition)
##     biomass         clipping
## Min.   :415.0   control:6  
## 1st Qu.:508.8   n25    :6  
## Median :568.0   n50    :6  
## Mean   :561.8   r10    :6  
## 3rd Qu.:631.8   r5     :6  
## Max.   :731.0      


  # Fit linear model

par(mfrow = c(1,1))

pdf("../Results/6a_BoxPlot", 11.7, 8.3)
plot(biomass ~ clipping, data = competition, ylab = "Plot biomass (g)", xlab = "Root clipping treatment")
dev.off()

competModel <- aov(biomass ~ clipping, data = competition)
print(competModel) # Summary of how the sums of squares of the biomass data are split between residual variation and that explained by clipping
## Call:
## aov(formula = biomass ~ clipping, data = competition)
##
## Terms:
##   clipping Residuals
## Sum of Squares   85356.47 124020.33
## Deg. of Freedom         4        25
##
## Residual standard error: 70.43304
## Estimated effects may be unbalanced

  # Calcuate ANOVA table
summary(competModel)
##               Df Sum Sq Mean Sq F value  Pr(>F)   
##   clipping     4  85356   21339   4.302 0.00875 **
##   Residuals   25 124020    4961                   
##   ---
##   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1


  # ...more detail
summary.lm(competModel)
## Call:
## aov(formula = biomass ~ clipping, data = competition)
##
## Residuals:
##   Min       1Q   Median       3Q      Max 
## -103.333  -49.667    3.417   43.375  177.667 
##
##  Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
##  (Intercept)   465.17      28.75  16.177  9.4e-15 ***
##  clippingn25    88.17      40.66   2.168  0.03987 *  
##  clippingn50   104.17      40.66   2.562  0.01683 *  
##  clippingr10   145.50      40.66   3.578  0.00145 ** 
##  clippingr5    145.33      40.66   3.574  0.00147 ** 
##  ---
##  Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
##
## Residual standard error: 70.43 on 25 degrees of freedom
## Multiple R-squared:  0.4077,	Adjusted R-squared:  0.3129 
## F-statistic: 4.302 on 4 and 25 DF,  p-value: 0.008752


  # Means and SE of biomass for each level
competMean <- with(competition, tapply(biomass, INDEX = clipping, FUN = mean))
print(competMean)
##  control      n25      n50      r10       r5 
## 465.1667 553.3333 569.3333 610.6667 610.5000 

seFun <- function(x) {sqrt(var(x)/length(x))}
competSEMean <- with(competition, tapply(biomass, INDEX = clipping, FUN = seFun))
print(competSEMean)
##  control      n25      n50      r10       r5 
## 23.51796 44.97011 19.59025 23.99815 24.47277 


# Tukey's honest significant difference
competTukeyHSD <- TukeyHSD(competModel)
competTukeyHSD
##   Tukey multiple comparisons of means
## 95% family-wise confidence level
##
## Fit: aov(formula = biomass ~ clipping, data = competition)
##
## $clipping
## diff        lwr      upr     p adj
## n25-control  88.1666667  -31.25984 207.5932 0.2243248
## n50-control 104.1666667  -15.25984 223.5932 0.1088202
## r10-control 145.5000000   26.07350 264.9265 0.0115253
## r5-control  145.3333333   25.90683 264.7598 0.0116387
## n50-n25      16.0000000 -103.42650 135.4265 0.9946026
## r10-n25      57.3333333  -62.09317 176.7598 0.6272414
## r5-n25       57.1666667  -62.25984 176.5932 0.6297485
## r10-n50      41.3333333  -78.09317 160.7598 0.8453941
## r5-n50       41.1666667  -78.25984 160.5932 0.8472695
## r5-r10       -0.1666667 -119.59317 119.2598 1.0000000

pdf("../Results/6b_TukeysHSD", 11.7, 8.3)
plot(competTukeyHSD)
dev.off()

# Make a bar chart
pdf("../Results/6c_BarChart", 11.7, 8.3)  
midpoints <- barplot(competMean, xlab = "Root competition treatment", ylab = "Plant biomass (g)", ylim = c(0,700))

  # Add error bars
arrows(midpoints, competMean - competSEMean, midpoints, competMean + competSEMean, code = 3, angle = 90)
dev.off()

# Kruskal-Wallis test
kruskal.test(biomass ~ clipping, data = competition)
##	Kruskal-Wallis rank sum test
##
## data:  biomass by clipping
## Kruskal-Wallis chi-squared = 11.9672, df = 4, p-value = 0.0176

tannin <- read.delim("../Data/tannin.txt")
str(tannin)
## 'data.frame':	9 obs. of  2 variables:
## $ growth: int  12 10 8 11 6 7 2 3 3
## $ tannin: int  0 1 2 3 4 5 6 7 8

summary(tannin)
##      growth           tannin 
## Min.   : 2.000   Min.   :0  
## 1st Qu.: 3.000   1st Qu.:2  
## Median : 7.000   Median :4  
## Mean   : 6.889   Mean   :4  
## 3rd Qu.:10.000   3rd Qu.:6  
## Max.   :12.000   Max.   :8

pdf("../Results/6_KruskalWallis")
plot(growth ~ tannin, data = tannin, xlab = "Tannin concentration", ylab = "Growth (mg)")

tanninMod <- lm(growth ~ tannin, data = tannin)
summary.aov(tanninMod)
##             Df Sum Sq Mean Sq F value   Pr(>F)    
##  tannin       1  88.82   88.82   30.97 0.000846 ***
##  Residuals    7  20.07    2.87                     
##  ---
##  Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

summary(tanninMod)
## Call:
## lm(formula = growth ~ tannin, data = tannin)
##
## Residuals:
##   Min      1Q  Median      3Q     Max 
## -2.4556 -0.8889 -0.2389  0.9778  2.8944 
##
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  11.7556     1.0408  11.295 9.54e-06 ***
##  tannin       -1.2167     0.2186  -5.565 0.000846 ***
##  ---
##  Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
##
## Residual standard error: 1.693 on 7 degrees of freedom
## Multiple R-squared:  0.8157,	Adjusted R-squared:  0.7893 
## F-statistic: 30.97 on 1 and 7 DF,  p-value: 0.0008461

  # Add line to graph
abline(tanninMod)
dev.off()
