# Statistics Practical Seven
# setwd("~/Documents/CMEECourseWork2015/Week4/Code/")

# Multivariate models
daphnia <- read.delim("../Data/daphnia.txt")
summary(daphnia)
##   Growth.rate     Water     Detergent    Daphnia  
## Min.   :1.762   Tyne:36   BrandA:18   Clone1:24  
## 1st Qu.:2.797   Wear:36   BrandB:18   Clone2:24  
## Median :3.788             BrandC:18   Clone3:24  
## Mean   :3.852             BrandD:18              
## 3rd Qu.:4.807                                    
## Max.   :6.918 

par(mfrow = c(1, 2))

pdf("../Results/7a_BoxPlot", 11.7, 8.3)
plot(Growth.rate ~ Detergent, data = daphnia)
plot(Growth.rate ~ Daphnia, data = daphnia)
dev.off()

seFun <- function(x) {sqrt(var(x)/length(x))}
detergentMean <- with(daphnia, tapply(Growth.rate, INDEX = Detergent,FUN = mean))
detergentSEM <- with(daphnia, tapply(Growth.rate, INDEX = Detergent,FUN = seFun))
cloneMean <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = mean))
cloneSEM <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = seFun))

pdf("../Results/7b_BarPlot", 11.7, 8.3)
par(mfrow = c(2,1), mar = c(4,4,1,1))  # adjust margins and layout of graphs
barMids <- barplot(detergentMean, xlab = "Detergent type", ylab = "Population growth rate", ylim = c(0,5)) # create bar chart
arrows(barMids, detergentMean - detergentSEM, barMids, detergentMean + detergentSEM, code = 3, angle = 90) # adds error bars

  # add second graph
barMids <- barplot(cloneMean, xlab = "Daphnia clone", ylab = "Population growth rate", ylim = c(0,5))
arrows(barMids, cloneMean - cloneSEM, barMids, cloneMean + cloneSEM, code = 3, angle = 90)
dev.off()

  #ANOVA table
daphniaMod <- aov(Growth.rate ~ Detergent + Daphnia, data = daphnia)
summary(daphniaMod)
##             Df Sum Sq Mean Sq F value   Pr(>F)    
## Detergent    3   2.21   0.737   0.642    0.591    
## Daphnia      2  39.18  19.589  17.063 1.06e-06 ***
## Residuals   66  75.77   1.148                     
## ---
## Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

summary.lm(daphniaMod) # more details...
## Call:
## aov(formula = Growth.rate ~ Detergent + Daphnia, data = daphnia)
##
## Residuals:
##   Min       1Q   Median       3Q      Max 
## -2.25917 -0.72208 -0.06135  0.71041  2.28597 
##
## Coefficients:
##                 Estimate Std. Error t value Pr(>|t|)    
## (Intercept)      2.87280    0.30930   9.288 1.34e-13 ***
## DetergentBrandB  0.12521    0.35715   0.351    0.727    
## DetergentBrandC  0.06968    0.35715   0.195    0.846    
## DetergentBrandD -0.32660    0.35715  -0.914    0.364    
## DaphniaClone2    1.73725    0.30930   5.617 4.21e-07 ***
## DaphniaClone3    1.29884    0.30930   4.199 8.19e-05 ***
##  ---
## Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
##
## Residual standard error: 1.071 on 66 degrees of freedom
## Multiple R-squared:  0.3533,	Adjusted R-squared:  0.3043 
## F-statistic: 7.211 on 5 and 66 DF,  p-value: 1.944e-05

detergentMean - detergentMean[1] # comparisons of the mean to BrandA
##     BrandA      BrandB      BrandC      BrandD 
## 0.00000000  0.12521198  0.06968013 -0.32660105 

cloneMean - cloneMean[1]
##   Clone1   Clone2   Clone3 
## 0.000000 1.737246 1.298845

daphniaModHSD <- TukeyHSD(daphniaMod)
daphniaModHSD
# #   Tukey multiple comparisons of means
# # 95% family-wise confidence level
# # 
# # Fit: aov(formula = Growth.rate ~ Detergent + Daphnia, data = daphnia)
# # 
# # $Detergent
# # diff        lwr       upr     p adj
# # BrandB-BrandA  0.12521198 -0.8161307 1.0665547 0.9850797
# # BrandC-BrandA  0.06968013 -0.8716625 1.0110228 0.9973423
# # BrandD-BrandA -0.32660105 -1.2679437 0.6147416 0.7972087
# # BrandC-BrandB -0.05553185 -0.9968745 0.8858108 0.9986474
# # BrandD-BrandB -0.45181303 -1.3931557 0.4895296 0.5881893
# # BrandD-BrandC -0.39628118 -1.3376239 0.5450615 0.6849619
# # 
# # $Daphnia
# # diff        lwr       upr     p adj
# # Clone2-Clone1  1.737246  0.9956362 2.4788555 0.0000013
# # Clone3-Clone1  1.298845  0.5572351 2.0404544 0.0002393
# # Clone3-Clone2 -0.438401 -1.1800107 0.3032086 0.3378930

par(mfrow = c(1,2), mar = c(4,8,4,4), las = 1) # mfrow puts 2 graphs side by side; mar adjusts margins; las adjusts orientation of axis labels to horizontal
pdf("../Results/7c_TukeyHSD", 11.7, 8.3)
plot(daphniaModHSD)
dev.off()

# (Changing to new dataset)
timber <- read.delim("../Data/timber.txt")
summary(timber)
# # volume           girth            height    
# # Min.   :0.7386   Min.   : 66.23   Min.   :18.9  
# # 1st Qu.:1.4048   1st Qu.: 88.17   1st Qu.:21.6  
# # Median :1.7524   Median :102.94   Median :22.8  
# # Mean   :2.1847   Mean   :105.72   Mean   :22.8  
# # 3rd Qu.:2.7010   3rd Qu.:121.69   3rd Qu.:24.0  
# # Max.   :5.5757   Max.   :164.38   Max.   :26.1  

pdf("../Results/7d_Pairs", 11.7, 8.3)
pairs(timber)
dev.off()

  # calculate pairwise Pearsons r correlations
# # cor(timber)
# # volume     girth    height
# # volume 1.0000000 0.9671176 0.5982517
# # girth  0.9671176 1.0000000 0.5192873
# # height 0.5982517 0.5192873 1.0000000

timberMod <- lm(volume ~ girth + height, data = timber)
summary.aov(timberMod)
# # Df Sum Sq Mean Sq F value Pr(>F)    
# # girth        1  39.75   39.75 503.107 <2e-16 ***
# # height       1   0.54    0.54   6.793 0.0145 *  
# # Residuals   28   2.21    0.08                   
# # ---
# # Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

summary(timberMod)
# # Call:
# #   lm(formula = volume ~ girth + height, data = timber)
# # 
# # Residuals:
# #   Min       1Q   Median       3Q      Max 
# # -0.46391 -0.19171 -0.02072  0.15929  0.61439 
# # 
# # Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)    
# # (Intercept) -4.198997   0.625537  -6.713 2.75e-07 ***
# #   girth        0.042725   0.002398  17.815  < 2e-16 ***
# #   height       0.081883   0.031416   2.606   0.0145 *  
# #   ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 0.2811 on 28 degrees of freedom
# # Multiple R-squared:  0.9479,	Adjusted R-squared:  0.9442 
# # F-statistic:   255 on 2 and 28 DF,  p-value: < 2.2e-16

  # (changing dataset...)
plantGrowth <- read.delim("../Data/ipomopsis.csv")
summary(plantGrowth)
# # Root            Fruit            Grazing  
# # Min.   : 4.426   Min.   : 14.73   Grazed  :20  
# # 1st Qu.: 6.083   1st Qu.: 41.15   Ungrazed:20  
# # Median : 7.123   Median : 60.88                
# # Mean   : 7.181   Mean   : 59.41                
# # 3rd Qu.: 8.510   3rd Qu.: 76.19                
# # Max.   :10.253   Max.   :116.05      

plantMod <- lm(Fruit ~ Root + Grazing, data = plantGrowth)
anova(plantMod)
# # Analysis of Variance Table
# # 
# # Response: Fruit
# # Df  Sum Sq Mean Sq F value    Pr(>F)    
# # Root       1 16795.0 16795.0  368.91 < 2.2e-16 ***
# # Grazing    1  5264.4  5264.4  115.63 6.107e-13 ***
# # Residuals 37  1684.5    45.5                      
# # ---
# # Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

summary(plantMod)
# # Call:
# #   lm(formula = Fruit ~ Root + Grazing, data = plantGrowth)
# # 
# # Residuals:
# #   Min       1Q   Median       3Q      Max 
# # -17.1920  -2.8224   0.3223   3.9144  17.3290 
# # 
# #  Coefficients:
# #   Estimate Std. Error t value Pr(>|t|)    
# #  (Intercept)     -127.829      9.664  -13.23 1.35e-15 ***
# #   Root              23.560      1.149   20.51  < 2e-16 ***
# #   GrazingUngrazed   36.103      3.357   10.75 6.11e-13 ***
# #   ---
# #   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# # 
# # Residual standard error: 6.747 on 37 degrees of freedom
# # Multiple R-squared:  0.9291,	Adjusted R-squared:  0.9252 
# # F-statistic: 242.3 on 2 and 37 DF,  p-value: < 2.2e-16

  # Create scatter graph
plantGrowth$plotChar <- 1 #$
plantGrowth$plotChar[plantGrowth$Grazing == "Ungrazed"] <- 19 
plantGrowth$plotCol <- "red" #$
plantGrowth$plotCol[plantGrowth$Grazing == "Ungrazed"] <- "darkblue"

pdf("../Results/7e_ScatterPlot", 11.7, 8.3)

plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter(mm)", pch = plotChar, col = plotCol)

abline(a = -127.829, b = 23.56, col = "red")
abline(a = -127.829 + 36.103, b = 23.56, col = "darkblue")

dev.off()
