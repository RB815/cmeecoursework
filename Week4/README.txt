CMEE (Computational Methods in Ecology and Evolution) Week 4 README file


***Directory Contents***


README.txt -----------------------This file, which provides an overview of Week 4's directory 


/Code ----------------------------Contains statistics practicals completed this week. 

	PracticalOne.R------------The basics: simple calculations, logical tests, concatenate function, recycling. Intentional error at the end.

	PracticalTwo.R------------Data entry and manipulation: seq() function, rep () function, creating factors and generating levels gl(), creating data vectors, data indexing ($), 
				  attach() / detach() function, subset() function. NB. Intentional error "object 'rate' not found" within script.

	PracticalThree.R----------Graphics in R: histograms (hist), box and whisker plots (plot), scatterplots (plot), bar plots (barplot)

	PracticalFour.R-----------Probability distributions, the t test and Chi squared: density (dt), probability (pt), quantiles (qt), random values drawn from the t distribution (rt), 
				  t test (one and two tailed), matrices, chi squared, fisher.test

	PracticalFive.R-----------Further t tests, wilcoxon tests and various manipulations: t.test, wilcox.test. Pearson's, Spearman's and Kendall's correlations. 
				  Intentional errors "cannot compute exact p-value"

	PracticalSix.R------------Linear models: analysis of variance (aov), linear models (.lm), tapply, Tukey HSD, arrows, kruskal,test, abline.

	PracticalSeven.R----------Multivariate models: Two way anova, Tukey HSD, axis label orientation (par(las)), multiple regression, cor(), analysis of covariance, coefficients, abline.
	
	PracticalEight.R----------Model Criticism and Simplification: Diagnostic plots (plot), transformation of non-normal data, log.


/Data-----------------------------Contains all data used this week.

	birdMass.txt--------------Body mass of three bird species. Practical 8

	competition.txt-----------Measurements of plant biomass under a control treatment and four experimental treatments to reduce root competition: neighbour root clipping at 
				  two levels (n 50 and n 25) and root pruning at two levels (r 5 and r 10). Practical 6, 8

	daphnia.txt---------------Rate of Daphnia population growth in water containing four different detergents, using individuals of three different clones. Practical 7

	decay.txt-----------------Mass of biodegradeable plastic remaining after x number of days of being buried. Practical 8

	desertRats.txt------------Desert rat body mass , rainfall, predator abundances, vegetation cover and seed production. Practical 8

	factories.csv-------------Factories.xls saved as csv file. Practical 2

	factories.xls-------------An original excel dataset. Factory locations, treatment type, rate. Practical 2

	gardenOzone.txt-----------Ozone concentrations in three market gardens from 10 days in summer. Practical 5
	
	ipomopsis.csv-------------Fruit production (dry weight) of Ipomopsis plant, given the initial root stock and the presence or absence of rabbit grazing. Practical 3, 7

	kicksample.txt------------Biodiversity scores from kicksampling up- and down-stream from nine sewage outfalls. Practical 5
	
	species.txt---------------Counts of the number of species found in a series of quadrats of increasing area. Practical 8

	tannin.txt----------------Change in caterpillar growth (mg), on diets containing different tannin concentrations. Practical 6

	timber.txt----------------Volume of useable timber from trees of know height and girth. Practical 7 
		

/Results---------------------------Outputs from R. The number within the file name indicates which practical the graphs are from

	3a_Histograms--------------Histograms: Fruit production by frequency and density
	
	3b_Boxplots----------------Box plots: Fruit production in grazed and ungrazed plots

	3c_Scatterplots------------Scatterplots: Effects of root size on fruit production in grazed and ungrazed plots
	
	3d_BarPlots----------------Bar plots: Mean and standard deviation of fruit production for grazed and ungrazed plots

	4_Histogram----------------Histogram of 1000 random values around the t distribution

	5a_Ozone-------------------Box plot and bar plots showing Ozone concentrations in market gardens

	5b_Kicksample--------------Scatterplot showing biodiversity scores upsteam and downstream of sewage outflows

	6a_BoxPlot-----------------Boxplot from practial 6: plant biomass in response to root competition reduction

	6b_TukeysHSD---------------Tukey's HSD tests of differences in plant biomass in response to root competition reduction

	6c_BarChart----------------Means and standard errors of plant growth responses to root competition reduction

	6d_KruskalWallis-----------Caterpillar growth on diets of different tannin concentrations, showing the predictions of a linear model

	7a_BoxPlot-----------------Boxplots of growth rate of Daphnia populations based on clonal genotype and detergent present in the water

	7b_BarPlot-----------------Barplots of mean and standard error in population growth rate of Daphnia by detergent type and by clonal genotype

	7c_TukeyHSD----------------Tukeys HSD plots for pairwise combinations of clonal genotype and detergent

	7d_Pairs-------------------Pairwise scatterplots of yielded timber volume (tonnes), tree height (m) and tree girth (cm)

	7e_Scatterplot-------------Scatterplot of fruit production as a function of root stock diameter for rabbit grazed and ungrazed plants
	
	8a_LinearReg---------------Linear regression of plastic degradation as a function of burial time

	8b_DiagnosticPlots---------Diagnostic plots for the linear regression of remaining mass as a function of burial time

	8c_LogLinearReg------------Linear regression of log-transformed remaining mass as a function of burial time

	8d_LogDiagnosticPlots------Diagnostic plots for the log-transformed linear regression of remaining mass as a function of burial time

	8e_BoxPlot-----------------Box and whisker plots for mass data on three bird species

	8f_SpeciesCounts-----------Scatterplot of species counts as a function of sampling area

	8g_SpeciesDiagnosticPlots--Model diagnostics on a linear model of species count as a function of sampling area

	8h_Pairs-------------------Pairwise plots for the desert rodents dataset

	8i_BoxPlot-----------------Box and whisker plots for biomass data following root competition treatments

	myPlot.pdf-----------------A simple plot without meaning, created in Practical 3 to learn how to save graphs in pdf format from rstudio
	

***TIPS***

Working directory is "~/Documents/CMEECourseWork2015/Week4/Code"


***Contact Details***

Author: Rachel Balasuriya

e-mail: rachel.balasuriya15@imperial.ac.uk
