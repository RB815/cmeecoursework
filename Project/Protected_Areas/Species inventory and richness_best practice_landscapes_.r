library(maptools)
library(rgdal)
library(reshape)
library(raster)
library(plyr)

## test with q <- 25000
for(q in seq(25000,200000,25000)) #start with 25000
{
  ################## map richness from distribution maps #################
  Gridsize <- q #in units of your projection 
  proj4string <- CRS("+proj=moll +datum=WGS84") # choose which projection should be used to process the shapefiles
  # remember this is not relevant for the actual shapefiles, the code
  # assumes longlat data given as decimal degrees for the shapefiles
  #root <- "/home/kami/Documents/Dropbox/Projects/Aktiv/Ben_collen/OLD/Data/Ranges/MAMMALS_terrestrial"
  root <- "/home/qpc/Rachel/Rachel/Protected_Areas/PAshapefiles/"
  #############################################################################
  maps <- data.frame((list.files(root, pattern=".shp", full.names=FALSE)),(list.files(root, pattern=".shp", full.names=TRUE)))
  names(maps) <- c("File", "Path")
  maps[["Landscape"]] <- unlist(strsplit((as.character(maps$File)), ".shp", fixed = TRUE))
  head(maps)
  nfiles <-length(maps$Path) #number of files to process
  cont <- list() #initialises a list which will contain later the final data
  ############ this reads the range maps, point samples it and calculates the species richness ######################
  seq <- 1:nfiles
  #n <- 1
  for (n in seq) #for (n in 1:nfiles) #goes through all files in the folder given under "root"
  {
    temp <- readShapePoly((as.character(maps$Path[n])), proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
    #reads the shapefile with its unique identifier and its projection
    projTemp <- spTransform(temp,proj4string) #projects the longlat projection into a different projection  
    projTemp$dummy <- 1
    bound <- bbox(projTemp) #the bounding box of the shape files is changed to match the grid
    bound[,1] <- (((floor(bound[,"min"]/Gridsize))*Gridsize)+(Gridsize/2))-Gridsize
    bound[,2] <- (((ceiling(bound[,"max"]/Gridsize))*Gridsize)-(Gridsize/2))+Gridsize
    grid <- SpatialPoints(GridTopology(bound[,"min"], c(Gridsize, Gridsize), c((bound[,"max"]-bound[,"min"])/Gridsize)))
    gridded(grid) <- TRUE
    grd <- raster(grid)
    rp <- rasterize(projTemp, grd)
    tempGridr <- data.frame(x=as.data.frame((coordinates(rp)[!is.na(values(rp)),"x"])), 
                            y=as.data.frame((coordinates(rp)[!is.na(values(rp)),"y"])))
    names(tempGridr) <- c("x", "y")
    tempGridp <- NULL
    if(any(!is.na(values(rp))))
    {
      projTemp <- projTemp[-(values(rp)[!is.na(values(rp))]),]
    }
    if(length(projTemp)>0)
    {
      for(m in 1:length(projTemp))
      {
        for(k in 1:length(projTemp@polygons[[m]]@Polygons))
        {
          if(projTemp@polygons[[m]]@Polygons[[k]]@hole==FALSE)
          {
            tempGridt <- data.frame(x=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[1])/Gridsize))*Gridsize)+(Gridsize/2),
                                    y=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[2])/Gridsize))*Gridsize)+(Gridsize/2))
            #gets the nearest X coordinate in the grid to the centroid
            #gets its nearest Y coordinate in the grid to the centroid
            # adds the presence of species (derived from the file name) to the coordinates of the nearest gridnames(tempGrid) <- c("X", "Y", "species")
          }
          tempGridp <- rbind(tempGridp, tempGridt)
        }
      }
    }
    tempGrid <- rbind(tempGridr, tempGridp)
    tempGrid <- unique(tempGrid)
    tempGrid$species <- (as.character(maps$Landscape[n])) 
    # adds the presence of species (derived from the file name) to the xy coords
    names(tempGrid) <- c("X", "Y", "Landscapes")
    cont[[n]] <- tempGrid #adds the current grid samples by the coordinates
    rm(list=ls()[grep("tempGrid", ls())])
    print(paste(n, " of ", nfiles, " @ resolution of ", Gridsize, " done!", sep=""))
  }
  cont <- rbind.fill(cont)
  cont <- unique(cont)
  #save(cont, file=paste("/home/kami/Documents/Dropbox/Projects/Aktiv/EDGE/EDGEII/data/mammal_", Gridsize/1000 ,"_species_gridded(cont).Rdata", sep=""))
  save(cont, file=paste("/home/qpc/Rachel/Rachel/Protected_Areas/", Gridsize/1000 ,"_MarineProtectedAreas_gridded(cont).Rdata", sep=""))
  cont1 <- cast(melt(cont, id=c("X","Y")), fun.aggregate=length)
  cont1 <- as.data.frame(cont1)
#  save(cont1,file=paste("/home/kami/Documents/Dropbox/Projects/Aktiv/EDGE/EDGEII/data/mammal_", Gridsize/1000 , "_species_richness(cont1).Rdata", sep=""))
  save(cont1,file=paste("/home/qpc/EDGE Zones/global shapefiles/CP landscapes/EDGE_DATA/All", Gridsize/1000 , "_landscapes_new_richness(cont1).Rdata", sep=""))
}



head(cont)
str(cont)
cont2 <- cont



# world map
ghhs <- readShapePoly("/home/qpc/EDGE Zones/EDGE_in_R/Shapefiles/world/GSHHS_l_L1.shp", force_ring=T)
proj4string(ghhs) <- CRS("+proj=longlat +datum=WGS84")
ghhs <- spTransform(ghhs, CRS("+proj=moll"))
plot(ghhs, col = "black")

cont.df <- as.data.frame(cont)
head(cont.df)
coordinates(cont.df) <- ~X+Y
gridded (cont.df) <- TRUE
rich <- raster (cont.df)
plot (rich, add = T)


###for EDGE species
load("/home/qpc/EDGE Zones/Asia/DATA/25_species_gridded(cont).Rdata")


head(cont)
# should be around 2000
length(unique(cont$species))
head(cont2)
unique_landscape <- unique(cont2$Landscapes)
species_list <- list()
i <- 1
j <- 2
all_data <- data.frame()
for(i in 1:length(unique_landscape)){
  land <- cont2[which(cont2$Landscapes == unique_landscape[i]),]
  species_overlap <- data.frame(rep(NA, length(land$Landscapes)))
  for(j in 1:length(land$Landscapes)){
    species_overlap <- cont$species[which(cont$X == land$X[j] & cont$Y == land$Y[j])]
    data <- data.frame(landscape = rep(unique_landscape[i], length = length(species_overlap)),  species = species_overlap)
    all_data <- rbind(all_data, data)
    } 
  print(unique_landscape[i])
} 

birds <- read.csv("/home/qpc/EDGE Zones/Asia/EDGE_birds.csv", header = T, stringsAsFactors = F)
mammals <- read.csv("/home/qpc/EDGE Zones/COMBINED_DATA/mammal_EDGE.csv", header = T, stringsAsFactors = F)
amphibians <- read.csv("/home/qpc/EDGE Zones/COMBINED_DATA/Amphib_EDGE.csv", header = T, stringsAsFactors = F)

all_data$count <- rep(1, nrow(all_data))

#birds
ag_data <- aggregate(count ~ species + landscape,all_data, sum)
ag_data <- ag_data[which(ag_data$species %in% birds$Species),]

ag_data2 <- ag_data[, c("landscape", "species", "count")]
names(ag_data2) <- c("Landscape", "Species", "No. of cells")
ag_data2[["Taxon"]] <- rep(NA, length(ag_data2$Species))
head(ag_data2)


ag_data2$Taxon[which(ag_data2$Species %in% birds$Species)] <- paste("Aves")
ag_data2$Taxon[which(ag_data2$Species %in% mammals$Species)] <- paste("Mammalia")
ag_data2$Taxon[which(ag_data2$Species %in% amphibians$Species)] <- paste("Amphibia")


#how many species in each landscape

ag_data$count_sp <- rep(1, nrow(ag_data))
ag_temp<-aggregate(count_sp~ landscape,ag_data, sum)



# sum of all grid cells x no of edge sp in that grid cell
ag_cells <- aggregate(count~ landscape,ag_data, sum)
ag_cells$'No. of EDGE species' <- ag_temp$count_sp
ag_cells$landscape_area <- rep(NA, length(ag_cells$landscape))
ag_cells$proportion_area <- rep(NA, length(ag_cells$landscape))




for(i in 1:length(ag_cells$landscape_area)){
  ag_cells$landscape_area[i] <- length(cont2$Landscapes[which(cont2$Landscapes == ag_cells$landscape[i])])
  ag_cells$proportion_area[i] <- as.numeric(ag_cells$count[i])/as.numeric(ag_cells$landscape_area[i]) 
  print(i)
}

ag_cells

ag_cells <- ag_cells[,c("landscape", "count", "landscape_area", "proportion_area", "No. of EDGE species")]

names(ag_cells) <- c("Landscape", "EDGE cells", "Landscape cells" , 
                     "Proportion of EDGE to Landscape", "EDGE species richness")
ag_cells

write.csv(ag_data2, "/home/qpc/EDGE Zones/global shapefiles/CP landscapes/EDGE_DATA/global_bird_spp.csv", row.names = F)
write.csv(ag_cells, "/home/qpc/EDGE Zones/global shapefiles/CP landscapes/EDGE_DATA/global_bird_species_data.csv", row.names = F)


#amphibians
ag_data <- aggregate(count ~ species + landscape,all_data, sum)
ag_data <- ag_data[which(ag_data$species %in% amphibians$Species),]

ag_data2 <- ag_data[, c("landscape", "species", "count")]
names(ag_data2) <- c("Landscape", "Species", "No. of cells")
ag_data2[["Taxon"]] <- rep(NA, length(ag_data2$Species))
head(ag_data2)


ag_data2$Taxon[which(ag_data2$Species %in% birds$Species)] <- paste("Aves")
ag_data2$Taxon[which(ag_data2$Species %in% mammals$Species)] <- paste("Mammalia")
ag_data2$Taxon[which(ag_data2$Species %in% amphibians$Species)] <- paste("Amphibia")


#how many species in each landscape

ag_data$count_sp <- rep(1, nrow(ag_data))
ag_temp<-aggregate(count_sp~ landscape,ag_data, sum)



# sum of all grid cells x no of edge sp in that grid cell
ag_cells <- aggregate(count~ landscape,ag_data, sum)
ag_cells$'No. of EDGE species' <- ag_temp$count_sp
ag_cells$landscape_area <- rep(NA, length(ag_cells$landscape))
ag_cells$proportion_area <- rep(NA, length(ag_cells$landscape))




for(i in 1:length(ag_cells$landscape_area)){
  ag_cells$landscape_area[i] <- length(cont2$Landscapes[which(cont2$Landscapes == ag_cells$landscape[i])])
  ag_cells$proportion_area[i] <- as.numeric(ag_cells$count[i])/as.numeric(ag_cells$landscape_area[i]) 
  print(i)
}

ag_cells

ag_cells <- ag_cells[,c("landscape", "count", "landscape_area", "proportion_area", "No. of EDGE species")]

names(ag_cells) <- c("Landscape", "EDGE cells", "Landscape cells" , 
                     "Proportion of EDGE to Landscape", "EDGE species richness")
ag_cells

write.csv(ag_data2, "/home/qpc/EDGE Zones/global shapefiles/CP landscapes/EDGE_DATA/global_amphibians_spp.csv", row.names = F)
write.csv(ag_cells, "/home/qpc/EDGE Zones/global shapefiles/CP landscapes/EDGE_DATA/global_amphibians_species_data.csv", row.names = F)

## mammals
ag_data <- aggregate(count ~ species + landscape,all_data, sum)
ag_data <- ag_data[which(ag_data$species %in% mammals$Species),]

ag_data2 <- ag_data[, c("landscape", "species", "count")]
names(ag_data2) <- c("Landscape", "Species", "No. of cells")
ag_data2[["Taxon"]] <- rep(NA, length(ag_data2$Species))
head(ag_data2)


ag_data2$Taxon[which(ag_data2$Species %in% birds$Species)] <- paste("Aves")
ag_data2$Taxon[which(ag_data2$Species %in% mammals$Species)] <- paste("Mammalia")
ag_data2$Taxon[which(ag_data2$Species %in% amphibians$Species)] <- paste("Amphibia")


#how many species in each landscape

ag_data$count_sp <- rep(1, nrow(ag_data))
ag_temp<-aggregate(count_sp~ landscape,ag_data, sum)



# sum of all grid cells x no of edge sp in that grid cell
ag_cells <- aggregate(count~ landscape,ag_data, sum)
ag_cells$'No. of EDGE species' <- ag_temp$count_sp
ag_cells$landscape_area <- rep(NA, length(ag_cells$landscape))
ag_cells$proportion_area <- rep(NA, length(ag_cells$landscape))




for(i in 1:length(ag_cells$landscape_area)){
  ag_cells$landscape_area[i] <- length(cont2$Landscapes[which(cont2$Landscapes == ag_cells$landscape[i])])
  ag_cells$proportion_area[i] <- as.numeric(ag_cells$count[i])/as.numeric(ag_cells$landscape_area[i]) 
  print(i)
}

ag_cells

ag_cells <- ag_cells[,c("landscape", "count", "landscape_area", "proportion_area", "No. of EDGE species")]

names(ag_cells) <- c("Landscape", "EDGE cells", "Landscape cells" , 
                     "Proportion of EDGE to Landscape", "EDGE species richness")
ag_cells

write.csv(ag_data2, "/home/qpc/EDGE Zones/global shapefiles/CP landscapes/EDGE_DATA/global_mammal_spp.csv", row.names = F)
write.csv(ag_cells, "/home/qpc/EDGE Zones/global shapefiles/CP landscapes/EDGE_DATA/global_mammal_species_data.csv", row.names = F)

unique_landscape[which(unique_landscape %in% ag_cells$Landscape == F)]
