CMEE (Computational Methods in Ecology and Evolution) Project README file

***Directory Contents***

README.txt -----------------------This file, which provides an overview of my research project directory 

/Code ----------------------------
	/Extinction
	/Marine_Mammal_code
	/Coral_code
		1_Splittingshapefile.R -------- Split large compact shapefiles of species distributions downloaded from IUCN 
		2_jpeg_the_ranges_Corals.R ---- Sort out the coral EDGE scores file and convert shapefiles to jpeg format
		3_Species_inventory_and_richness_best_practice_Corals.r --- Using a mollweide projection, split shapefiles into resolutions of 25km to 200km. 
			Save dataframes with X & Y coordinates along the species names (cont) and species richness (cont1) in each grid cell, for each resolution.
		4_Adding_EDGE_and_mapping.R --- 
		
		6_Calc_quantile.R ------------- 

/Data-----------------------------Contains all data used this week.
	/fasta--------------------Contains fasta files taken from cmee2015masterepo. These were used for Week 1's Practical
		407228326.fasta
		407228412.fasta
		AT
		E.coli.fasta
		GC
	spawannxs.txt-------------File taken from http://www.cep.unep.org/pubs/legislation/spawannxs.txt. Used for practicising grep commands
	/Temperatures-------------Contains fasta files taken from cmee2015masterepo. These were used for Week 1's Practical
		1800.csv  
		1801.csv  
		1802.csv  
		1803.csv
		
/sandbox--------------------------A fun place to play!
	ListRootDir.txt
	/TestFind-----------------Contains a bunch of empty files, just for practicing creating and finding files.
		/Dir1
			/Dir11
				/Dir111
					File111.txt
			File1.csv
			File1.tex
			File1.txt
		/Dir2
			File2.csv
			File2.txt
			File.tex
		/Dir3
			File3.txt
	test.txt--------------------The result of redirecting and appending text in terminal
	/TestWild-------------------Another bunch of empty files, used to practice using wildcards
		Anotherfile.csv
		File1.csv
		File2.csv
		File3.csv
		File4.csv
		Anotherfile.txt
		File1txt
		File2.txt
		File3.txt
		File4.txt


***TIPS***

Bash scripts: 
Ensure that you are in the Code directory before running bash scripts.
Type $ bash <file.sh> <optional files or commands> in terminal to run shell script

***Contact Details***
Author: Rachel Balasuriya
e-mail: rachel.balasuriya15@imperial.ac.uk
