\documentclass[12pt, a4paper]{article}
\usepackage{authblk} % for adding affiliations
\usepackage{pgfgantt} % for the Gantt chart
\usepackage[margin={2cm, 2cm}]{geometry} % borders
\usepackage{setspace, lineno} % for line spacing & line numbers
\doublespacing % double spaced lines
\usepackage[pdfborder={0 0 0}]{hyperref}% For email addresses

\title{Effect of environmental variables on cyanobacteria communities in Lake Joyce, Antarctica}

\author[1]{Rachel Balasuriya\\{\small Supervised by: Dr Anne Jungblut}}

\author[2]{\small Dr James Rosindell}

\affil[1]{Natural History Museum \\
\url{a.jungblut@nhm.ac.uk}}
\affil[2]{Imperial College London \\
\url{j.rosindell@imperial.ac.uk}}

\date{\endgraf
  {\small Committee Members:} \textsc{\small Prof Tim Barraclough and Dr Tom Bell}\endgraf}

\begin{document}
  \maketitle

\begin{linenumbers}

\subsection*{Background}
Lake Joyce is a perennially ice-covered, meromictic lake in Antarctica which has been rising over several decades due to an influx of meltwater\cite{hawes2011}. Like neighbouring lakes, it contains communities of cyanobacteria which form microbial mats\cite{mackey2015, zhang2015}. These mats form their own microhabitats in shallow water, where there is enough light to photosynthesise\cite{hawes2011}. However, as the lake level rises, environmental conditions change\cite{lyons2000}, and so do the features of stationary microbial mats\cite{mackey2015}. Previous studies have shown that, whilst generalist phylotypes are widespread throughout the lakes, rarer phylotypes may occupy different niches, dependent on environmental conditions\cite{zhang2015}. The aim of this project would be to compare microbial mat and water communities from different depths of Lake Joyce and test how environmental variables such as light, nutrient and oxygen levels affect community composition.

\subsection*{Methods} 
16s and 18s rRNA and next generation sequencing of samples from different depths of the lakes would show diversity of cyanobacteria\cite{brambilla2001} across a range of environmental conditions due to the meromictic nature of the lake. This can be combined with environmental data already collected to form correlations between phylotype occurrence and environmental variables. Multivariate analyses can be used to determine which environmental variables, if any, affect community composition.

\subsection*{Project feasibility supported by a Gannt chart}
Anne Jungblut has worked on similar projects, including Lake Joyce and has the lab facilities and samples available for use. Samples have already been collected and some have been sequenced. Sequencing more samples will provide a more robust analysis. Environmental data also already exists and is available to use for this project.

\definecolor{barblue}{RGB}{153,204,255}
\definecolor{groupblue}{RGB}{51,102,254}
\definecolor{linkred}{RGB}{165,0,33}
\renewcommand\sfdefault{phv}
\renewcommand\mddefault{mc}
\renewcommand\bfdefault{bc}
\sffamily

\begin{ganttchart}[
    canvas/.append style={fill=none, draw=black!5, line width=.75pt},
    hgrid style/.style={draw=black!5, line width=.75pt},
    vgrid={*1{draw=black!5, line width=.75pt}},
    today=1,
    today rule/.style={
      draw=black!64,
      dash pattern=on 3.5pt off 4.5pt,
      line width=1.5pt
    },
    today label font=\small\bfseries,
    title/.style={draw=none, fill=none},
    title label font=\bfseries\footnotesize,
    title label node/.append style={below=7pt},
    include title in canvas=false,
    bar label font=\mdseries\small\color{black!70},
    bar label node/.append style={left=0cm},
    bar/.append style={draw=none, fill=black!63},
    bar incomplete/.append style={fill=barblue},
    bar progress label font=\mdseries\footnotesize\color{black!70},
    group incomplete/.append style={fill=groupblue},
    group left shift=0,
    group right shift=0,
    group height= 0.5,
    group peaks tip position=0,
    group label node/.append style={left=0.6cm},
    group progress label font=\bfseries\small
  ]{1}{23}
  \gantttitle[
    title label node/.append style={below left=7pt and -3pt}
  ]{WEEKS:\quad1}{1}
  \gantttitlelist{2,...,23}{1} \\
  \ganttgroup[progress=25]{Preparation}{1}{4} \\
  \ganttbar[progress=100]{\textbf{1.1} Select topic area}{1}{1} \\
  \ganttbar[progress=5]{\textbf{1.2} Background reading}{1}{22} \\
  \ganttbar[progress=90]{\textbf{1.3} Formulate question}{1}{1} \\
  \ganttbar[progress=100]{\textbf{1.4} Write proposal}{1}{1} \\
  \ganttbar[progress=0]{\textbf{1.5} Refine ideas}{1}{4} \\[grid]
  
  \ganttgroup[progress=0]{Practical}{5}{18} \\
  \ganttbar[progress=0]{\textbf{2.1} DNA sequencing}{6}{11} \\
  \ganttbar[progress=0]{\textbf{2.2} Analyses}{10}{14} \\
  \ganttbar[progress=0]{\textbf{2.3} Data interpretation}{14}{18} \\ [grid]
  
  \ganttgroup[progress=0]{Write up}{4}{23} \\  
  \ganttbar[progress=0]{\textbf{3.1} Introduction}{4}{9} \\
  \ganttbar[progress=0]{\textbf{3.2} Methods \& Results}{13}{19} \\
  \ganttbar[progress=0]{\textbf{3.3} Discussion}{18}{22} \\
  \ganttbar[progress=0]{\textbf{3.4} Amendments}{10}{23}

\end{ganttchart}

\bibliographystyle{plain}
\bibliography{ProposalRefs}

\end{linenumbers}
\end{document}

