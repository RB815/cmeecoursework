library(maptools)
library(rgdal)
library(reshape)
library(raster)
library(plyr)

## test with q <- 25000
for(q in seq(25000,200000,25000)) #start with 25000
{
  ################## map richness from distribution maps #################
  Gridsize <- q #in units of your projection 
  proj4string <- CRS("+proj=moll +datum=WGS84") # choose which projection should be used to process the shapefiles
  # remember this is not relevant for the actual shapefiles, the code
  # assumes longlat data given as decimal degrees for the shapefiles
  #root <- "/home/kami/Documents/Dropbox/Projects/Aktiv/Ben_collen/OLD/Data/Ranges/MAMMALS_terrestrial"
  root <- "/home/qpc/EDGE Zones/EDGE_in_R/Shapefiles/Mammals_2016"
  #############################################################################
  maps <- data.frame((list.files(root, pattern=".shp", full.names=FALSE)),(list.files(root, pattern=".shp", full.names=TRUE)))
  names(maps) <- c("File", "Path")
  maps[["Species"]] <- unlist(strsplit((as.character(maps$File)), ".shp", fixed = TRUE))
  nfiles <-length(maps$Path) #number of files to process
  cont <- list() #initialises a list which will contain later the final data
  ############ this reads the range maps, point samples it and calculates the species richness ######################
  seq <- 1:nfiles
  for (n in seq) #for (n in 1:nfiles) #goes through all files in the folder given under "root"
  {
    temp <- readShapePoly((as.character(maps$Path[n])), proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
    #reads the shapefile with its unique identifier and its projection
    projTemp <- spTransform(temp,proj4string) #projects the longlat projection into a different projection  
    projTemp$dummy <- 1
    bound <- bbox(projTemp) #the bounding box of the shape files is changed to match the grid
    bound[,1] <- (((floor(bound[,"min"]/Gridsize))*Gridsize)+(Gridsize/2))-Gridsize
    bound[,2] <- (((ceiling(bound[,"max"]/Gridsize))*Gridsize)-(Gridsize/2))+Gridsize
    grid <- SpatialPoints(GridTopology(bound[,"min"], c(Gridsize, Gridsize), c((bound[,"max"]-bound[,"min"])/Gridsize)))
    gridded(grid) <- TRUE
    grd <- raster(grid)
    rp <- rasterize(projTemp, grd)
    tempGridr <- data.frame(x=as.data.frame((coordinates(rp)[!is.na(values(rp)),"x"])), 
                            y=as.data.frame((coordinates(rp)[!is.na(values(rp)),"y"])))
    names(tempGridr) <- c("x", "y")
    tempGridp <- NULL
    if(any(!is.na(values(rp))))
    {
      projTemp <- projTemp[-(values(rp)[!is.na(values(rp))]),]
    }
    if(length(projTemp)>0)
    {
      for(m in 1:length(projTemp))
      {
        for(k in 1:length(projTemp@polygons[[m]]@Polygons))
        {
          if(projTemp@polygons[[m]]@Polygons[[k]]@hole==FALSE)
          {
            tempGridt <- data.frame(x=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[1])/Gridsize))*Gridsize)+(Gridsize/2),
                                    y=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[2])/Gridsize))*Gridsize)+(Gridsize/2))
            #gets the nearest X coordinate in the grid to the centroid
            #gets its nearest Y coordinate in the grid to the centroid
            # adds the presence of species (derived from the file name) to the coordinates of the nearest gridnames(tempGrid) <- c("X", "Y", "species")
          }
          tempGridp <- rbind(tempGridp, tempGridt)
        }
      }
    }
    tempGrid <- rbind(tempGridr, tempGridp)
    tempGrid <- unique(tempGrid)
    tempGrid$species <- (as.character(maps$Species[n])) 
    # adds the presence of species (derived from the file name) to the xy coords
    names(tempGrid) <- c("X", "Y", "species")
    cont[[n]] <- tempGrid #adds the current grid samples by the coordinates
    rm(list=ls()[grep("tempGrid", ls())])
    print(paste(n, " of ", nfiles, " @ resolution of ", Gridsize, " done!", sep=""))
  }
  cont <- rbind.fill(cont)
  cont <- unique(cont)
  #save(cont, file=paste("/home/kami/Documents/Dropbox/Projects/Aktiv/EDGE/EDGEII/data/mammal_", Gridsize/1000 ,"_species_gridded(cont).Rdata", sep=""))
  save(cont, file=paste("/home/qpc/EDGE Zones/EDGE_in_R/newdata/mammal_", Gridsize/1000 ,"_species_gridded(cont).Rdata", sep=""))
  cont1 <- cast(melt(cont, id=c("X","Y")), fun.aggregate=length)
  cont1 <- as.data.frame(cont1)
#  save(cont1,file=paste("/home/kami/Documents/Dropbox/Projects/Aktiv/EDGE/EDGEII/data/mammal_", Gridsize/1000 , "_species_richness(cont1).Rdata", sep=""))
  save(cont1,file=paste("/home/qpc/EDGE Zones/EDGE_in_R/newdata/mammal_", Gridsize/1000 , "_species_richness(cont1).Rdata", sep=""))
}

head(cont1)

cont1 <- as.data.frame(cont1)

coordinates(cont1) <- ~X+Y
gridded (cont1) <- TRUE
rich <- raster (cont1)
plot (rich)

