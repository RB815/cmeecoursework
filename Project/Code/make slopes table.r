rm(list = ls())
fl <- list.files("/media/terra/EDGEII", pattern="coeff.Rdata", full.names=T)

f <- fl[4]
  load(f)
  print(f)
if(exists("coeff.amph"))
{
  coeff <- coeff.amph
}
if(exists("coeff.mammal"))
{
  coeff <- coeff.mammal
}
  s <- list(c(1:100), c(101:200), c(201:300), c(301:400), c(401:500), c(501:600), c(601:700), c(701:800))
  grain <- c(25,50,75,100,125,150,175,200)
  res <- list()
  for(j in 1:8)
  {
    print(paste("Quantiles estimated intercept and slopes @ resolution ", grain[j], sep=""))
    vals <- data.frame(mean=NULL, sd=NULL, q=NULL)
    for(i in 1:9)
    {
      av <- mean(matrix(unlist(coeff), ncol=9, byrow=T)[s[[j]],i])
      sdav <- sd(matrix(unlist(coeff), ncol=9, byrow=T)[s[[j]],i])
      d <- ecdf(matrix(unlist(coeff), ncol=9, byrow=T)[s[[j]],i])
      print(paste("Mean: ", round(av,4), " ; Quantile: ", d(0), ".", sep=""))
      vals <- rbind(vals, data.frame(mean=av, sd=sdav, q=d(0)))
    }
    res <- c(res, list(grain=grain[j], vals))
  }
