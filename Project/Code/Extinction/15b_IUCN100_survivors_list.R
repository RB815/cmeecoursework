# Calculate sum ED of coral species after 100 years, based on extinction predictions

setwd("~/Documents/CMEECourseWork2015/Project/Code/")

library(reshape) # for the cast function which fits two dataframes together

# Load species names and X & Y coordinates
load("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_100_species_gridded(cont).Rdata") # called cont
cont_coral <- cont # rename cont something more descriptive
names(cont_coral) <- c("X", "Y", "Species") # Rename columns to match Pext_corals

# Load csv file with species names, ED scores and extinction risk at 100 yrs
Pext_corals <- read.csv("../Data/Pext_corals100.csv", sep = ",")
Pext_corals <- Pext_corals[, c("ED", "PEXT", "Species")]


## create a list of conts based on the extinct species for each iteration
cont_ext_list <- list()

## create loop to generate N iterations of extinction scenarios
## I do this in four runs of 25, and use 'k' to represent each run. Increase to about 1000(?) runs on HPC
# k <- 1 # If the computer can only handle one run, remove the next line and just change value of k (for trial)
# for (k in 1:4){ # 4 runs
  for(m in 1:500){ # WHY REPEAT? for m and k?
    survivors <- NULL # create empty object to fill with 'extant' species
    # drop species based on their PEXT and create vector of 'extant' species
    for(i in 1:length(Pext_corals$Species)){
      if(rbinom(1, 1, 1-Pext_corals$PEXT[i]) == 1){ # Using a binomial distribution and the likelihood of survival, randomly select whether species dies or survives
        survivors[length(survivors)+1] <- as.character(Pext_corals$Species[i])
      }
    }
    cont_coral <- cont_coral[which(cont_coral$Species %in% survivors),]
    #Merge 'surviving' species with location
    cont.Pext_corals <- merge(cont_coral, Pext_corals, by="Species")
    #   remove NAs and PEXT column which is no longer needed
    ED <- cont.Pext_corals[complete.cases(cont.Pext_corals[,c("X", "Y", "ED")]),c("X", "Y", "ED")]
    #Now total up the ED scores per grid square using cast function
    coral.ED <- as.data.frame(cast(melt(ED, id=c("X","Y")), fun.aggregate=sum))
    names(coral.ED) <- c("X", "Y", "sumED")
    # WHY TURN IT INTO A LIST?
    cont_ext_list[[length(cont_ext_list)+1]] <- coral.ED
    print(m)
  # }
    save(cont_ext_list, file = "/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/cont_sumED_ext_list_IUCN100_all.RData")
    # save(cont_ext_list, file = paste("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/cont_sumED_ext_list_IUCN100_", k,".RData", sep = ""))
}
