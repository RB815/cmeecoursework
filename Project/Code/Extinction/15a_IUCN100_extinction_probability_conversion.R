# Convert IUCN listing into probability of extiction in 100 years time, according to Mooers et al. (2008)
rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/Code/")

# Load csv with species names, ED and GE scores
edlist <- read.csv("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Coral_csv/ALL_corals_EDGE_scores.csv", header = T, stringsAsFactors = F) 

# Using Mooers et al. (2008) projected probabilities of extinction in 100 years, give each species probability based on GE score
for (i in 1:length(edlist$Species)){
  if (edlist$GE[i] == 0){
    edlist[["PEXT50"]][i] <- 0.00005 # Least Concern = 0.005% in 50yrs
    edlist[["PEXT100"]][i] <- 0.0001 # Least Concern = 0.001% in 100yrs
    edlist[["PEXT500"]][i] <- 0.0005 # Least Concern = 0.005% in 500yrs
  } 
  else if (edlist$GE[i] == 1){
    edlist[["PEXT50"]][i] <- 0.004 # Near Threatened = 0.4% in 50yrs
    edlist[["PEXT100"]][i] <- 0.01 # Near Threatened = 0.1% in 100yrs
    edlist[["PEXT500"]][i] <- 0.02 # Near Threatened = 0.2% in 500yrs
  } 
  else if (edlist$GE[i] == 2){
    edlist[["PEXT50"]][i] <- 0.05 # Vulnerable = 5% in 50yrs
    edlist[["PEXT100"]][i] <- 0.1 # Vulnerable = 10% in 100yrs
    edlist[["PEXT500"]][i] <- 0.39 # Vulnerable = 39% in 500yrs
  }
  else if (edlist$GE[i] == 3){
    edlist[["PEXT50"]][i] <- 0.42 # Endangered = 42% in 50yrs
    edlist[["PEXT100"]][i] <- 0.667 # Endangered = 66.7% in 100yrs
    edlist[["PEXT500"]][i] <- 0.996 # Endangered = 99.6% in 500yrs
  }
  else if (edlist$GE[i] == 4){
    edlist[["PEXT50"]][i] <- 0.97 # Critically Endangered = 97% in 50yrs
    edlist[["PEXT100"]][i] <- 0.999 # Critically Endangered = 99.9% in 100yrs
    edlist[["PEXT500"]][i] <- 1 # Critically Endangered = 100% in 500yrs
  }
}

for (i in 1:length(edlist$Species)){
  edlist[["PEXT.ED50"]][i] <- edlist$ED[i]*(1.000-edlist$PEXT50[i]) # Projected ED in 50 years
  edlist[["PEXT.ED100"]][i] <- edlist$ED[i]*(1.000-edlist$PEXT100[i]) # Projected ED in 100 years
  edlist[["PEXT.ED500"]][i] <- edlist$ED[i]*(1.000-edlist$PEXT500[i]) # Projected ED in 500 years
  
  edlist[["Psurvival50"]][i] <- 1.000-edlist$PEXT50[i] # Projected survival of species (for species richness) in 50 years
  edlist[["Psurvival100"]][i] <- 1.000-edlist$PEXT100[i] # Projected survival of species (for species richness) in 100 years
  edlist[["Psurvival500"]][i] <- 1.000-edlist$PEXT500[i] # Projected survival of species (for species richness) in 500 years
}

for (i in 1:length(edlist$Species)){
  edlist[["EDloss50"]][i] <- edlist$ED[i]-edlist$PEXT.ED50[i] # Projected ED loss in 50 years
  edlist[["EDloss100"]][i] <- edlist$ED[i]-edlist$PEXT.ED100[i] # Projected ED loss in 100 years
  edlist[["EDloss500"]][i] <- edlist$ED[i]-edlist$PEXT.ED500[i] # Projected ED loss in 500 years
}

for (i in 1:length(edlist$Species)){
  edlist[["propEDloss50"]][i] <- edlist$EDloss50[i]/edlist$ED[i] # Projected ED loss in 50 years
  edlist[["propEDloss100"]][i] <- edlist$EDloss100[i]/edlist$ED[i] # Projected ED loss in 100 years
  edlist[["propEDloss500"]][i] <- edlist$EDloss500[i]/edlist$ED[i] # Projected ED loss in 500 years
  }

# Format species column
edlist$Species <- as.character(edlist$Species)

# Separate dataframe into different timescales
Pext_corals50 <- edlist[, c("Species", "ED", "PEXT50", "PEXT.ED50", "Psurvival50", "EDloss50", "propEDloss50")] # Select only required columns and change name to be more descriptive
Pext_corals50 <- na.omit(Pext_corals50) # Remove data deficient species
names(Pext_corals50) <- c("species", "ED", "PEXT", "PEXT.ED", "Psurvival", "EDloss", "propEDloss")

Pext_corals100 <- edlist[, c("Species", "ED", "PEXT100", "PEXT.ED100", "Psurvival100", "EDloss100", "propEDloss100")] # Select only required columns and change name to be more descriptive
Pext_corals100 <- na.omit(Pext_corals100) # Remove data deficient species
names(Pext_corals100) <- c("species", "ED", "PEXT", "PEXT.ED", "Psurvival", "EDloss", "propEDloss")

Pext_corals500 <- edlist[, c("Species", "ED", "PEXT500", "PEXT.ED500", "Psurvival500", "EDloss500", "propEDloss500")] # Select only required columns and change name to be more descriptive
Pext_corals500 <- na.omit(Pext_corals500) # Remove data deficient species
names(Pext_corals500) <- c("species", "ED", "PEXT", "PEXT.ED", "Psurvival", "EDloss", "propEDloss")

# Save dataframes with (p)Extinction data
write.csv(Pext_corals50, file = "/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Coral_csv/Pext_corals50.csv") # Save the new dataframe
write.csv(Pext_corals100, file = "/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Coral_csv/Pext_corals100.csv") # Save the new dataframe
write.csv(Pext_corals500, file = "/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Coral_csv/Pext_corals500.csv") # Save the new dataframe
