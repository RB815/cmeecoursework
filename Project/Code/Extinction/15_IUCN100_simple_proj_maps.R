rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/")

library(maptools)
library(rgdal)
library(reshape)
library(raster)

# load coordinates for corals
load("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/Corals/Coral_100_species_gridded(cont).Rdata") # which cont resolution to use?

# load extinction risk values for corals (simple method, see 15a_IUCN100_extinction_probability_conversion.R)
edlist <- read.csv("~/Documents/CMEECourseWork2015/Project/Data/Coral_csv/Pext_corals100.csv", sep = ",") 
ed <- edlist[, c("species", "PEXT.ED", "Psurvival", "EDloss")]

# merge coordinates and extinction/survival probability dataframes
cont.ed <- merge(cont, ed, by="species")

# Tidy up df with only relevant columns, removing NAs
EX.ED <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "PEXT.ED")]),c("X", "Y", "PEXT.ED")]
SR <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "Psurvival")]),c("X", "Y", "Psurvival")]
loss <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "EDloss")]),c("X", "Y", "EDloss")]

# Sum the values in each grid square
coral.ED <- as.data.frame(cast(melt(EX.ED, id=c("X","Y")), fun.aggregate=sum))
names(coral.ED) <- c("X", "Y", "sumED")
coral.ED.l <- as.data.frame(cast(melt(EX.ED, id=c("X","Y")), fun.aggregate=length))
names(coral.ED.l) <- c("X", "Y", "richnessED")

coral.SR <- as.data.frame(cast(melt(SR, id=c("X","Y")), fun.aggregate=length))
names(coral.SR) <- c("X", "Y", "sumSR")
coral.SR.l <- as.data.frame(cast(melt(SR, id=c("X","Y")), fun.aggregate=length))
names(coral.SR.l) <- c("X", "Y", "SpeciesRichness")

coral.loss <- as.data.frame(cast(melt(loss, id=c("X","Y")), fun.aggregate=length))
names(coral.loss) <- c("X", "Y", "sumLoss")
coral.loss.l <- as.data.frame(cast(melt(loss, id=c("X","Y")), fun.aggregate=length))
names(coral.loss.l) <- c("X", "Y", "richnessLoss")

coral.ED <- merge(coral.ED, coral.ED.l, by=c("X", "Y"))
coral.SR <- merge(coral.SR, coral.SR.l, by=c("X", "Y"))
coral.SR <- merge(coral.loss, coral.loss.l, by=c("X", "Y"))


#plot the maps
coordinates (coral.ED) <- ~X+Y
gridded(coral.ED) <- TRUE
coralED <- raster (coral.ED)

coordinates (coral.SR) <- ~X+Y
gridded(coral.SR) <- TRUE
coralSR <- raster (coral.SR)

coordinates (coral.loss) <- ~X+Y
gridded(coral.loss) <- TRUE
coralLoss <- raster (coral.loss)

# world map
ghhs <- readShapePoly("/home/gjp15/Documents/CMEECourseWork2015/Project/Code/GSHHS_l_L1.shp", force_ring=T)
proj4string(ghhs) <- CRS("+proj=longlat +datum=WGS84")
ghhs <- spTransform(ghhs, CRS("+proj=moll"))

# Plot projected ED 
# plot(ghhs, col = "grey", main = "Projected ED in 50 years")
# plot(coralED, add=T)

# plot(ghhs, col = "grey", main = "Projected species richness in 50 years")
# plot(coralSR, add=T)

# plot(ghhs, col = "grey", main = "Projected ED loss in 50 years")
# plot(coralLoss, add=T)

# writeRaster(coralED, "Data/Map_Rasters/coralEDzones_100yrs", format = "raster")
# writeRaster(coralSR, "Data/Map_Rasters/coralSR_100yrs", format = "raster")
# writeRaster(coralLoss, "Data/Map_Rasters/coralED_loss_100yrs", format = "raster")