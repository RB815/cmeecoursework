# Calculate proportion loss in 100 years
rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/")

library(maptools)
library(rgdal)
library(reshape)
library(raster)

# load coordinates for corals
load("Data/Rdata/Corals/Coral_100_species_gridded(cont).Rdata") # which cont resolution to use?

# Load in current coral ED scores
currentED <- read.csv("Data/Coral_csv/ALL_corals_EDGE_scores.csv", sep = ",") 
Ced <- currentED[, c("species", "ED")]
# names(Ced) <- c("species", "ED") # remove and replace "species" with "Species" in script
cont$species <- sub(" ", "_", cont$species)

cont.Ced <- merge(cont, Ced, by="species")
cED <- cont.Ced[complete.cases(cont.Ced[,c("X", "Y", "ED")]),c("X", "Y", "ED")]

coral.ED <- as.data.frame(cast(melt(cED, id=c("X","Y")), fun.aggregate=sum))
names(coral.ED) <- c("X", "Y", "sumED")

#####################

# load extinction risk values for corals (simple method, see 15a_IUCN100_extinction_probability_conversion.R)
new_xy <- read.csv("Data/Coral_csv/Pext_corals50.csv", sep = ",") 
ed <- new_xy[, c("species", "PEXT.ED")]
# names(ed) <-  c("species", "PEXT.ED")
ed$species <- sub(" ", "_", ed$species)

# merge coordinates and extinction/survival probability dataframes
cont.ed <- merge(cont, ed, by="species")

# Tidy up df with only relevant columns, removing NAs
cont.ed <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "PEXT.ED")]),c("X", "Y", "PEXT.ED")]

# Sum the values in each grid square
new_xy <- as.data.frame(cast(melt(cont.ed, id=c("X","Y")), fun.aggregate=sum))
names(new_xy) <- c("X", "Y", "sumED")

#########################

scenario <- data.frame(X=coral.ED$X, Y=coral.ED$Y, current=coral.ED$sumED, 
                       future=rep(NA, length(coral.ED$X)), loss=rep(NA, length(coral.ED$X)),
                       proportion_lost=rep(NA, length(coral.ED$X)), SRloss=rep(NA, length(coral.ED$X)))

for(i in 1:length(scenario$current)){
  scenario$future[i] <- new_xy$sumED[which(new_xy$X == coral.ED$X[i] & new_xy$Y == coral.ED$Y[i])]
  scenario$loss[i] <- scenario$current[i] - new_xy$sumED[which(new_xy$X == coral.ED$X[i] & new_xy$Y == coral.ED$Y[i])]
}

for(i in 1:length(scenario$loss)){
  scenario$proportion_lost[i] <- scenario$loss[i]/scenario$current[i]
}

# save(scenario, file = "~/Documents/CMEECourseWork2015/Project/Data/Rdata/Corals/scenario_50_trial.RData")

#########################

# world map
ghhs <- readShapePoly("/home/gjp15/Documents/CMEECourseWork2015/Project/Code/GSHHS_l_L1.shp", force_ring=T)
proj4string(ghhs) <- CRS("+proj=longlat +datum=WGS84")
ghhs <- spTransform(ghhs, CRS("+proj=moll"))

lossplot <- scenario[,c("X", "Y", "loss")]
coordinates (lossplot) <- ~X+Y
gridded(lossplot) <- TRUE
lossplot <- raster(lossplot)

plot(ghhs, col = "grey", main = "Projected Coral ED loss in 50 years")
plot(lossplot, add =T)

propplot <- scenario[,c("X", "Y", "proportion_lost")]
coordinates (propplot) <- ~X+Y
gridded(propplot) <- TRUE
scplot <- raster(propplot)

plot(ghhs, col = "grey", main = "Proportion Coral ED loss in 50 years")
plot(scplot, add = T)


# writeRaster(scplot, "../Data/Map_Rasters/coralED_proportion_loss_100yrs", format = "raster")
