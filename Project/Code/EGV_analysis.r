library(nlme)
library(mgcv)
library(raster)
library(MASS)
library(spdep)
library(rgdal)

rm(list = ls())

s <- c(25,50,75,100,125,150,175,200)
coral.mod <- list()
egv.coral <- list()
for(n in s)
{
  print(paste("Doing ", n, ".", sep=""))
  load(paste("~/Documents/CMEECourseWork2015/Project/Data/EGV/Coral_", n, "_qEDGE.Rdata", sep=""))
  coral.qEDGE <- coral.qEDGE[complete.cases(coral.qEDGE),]
  coral.qEDGE$qEDGE[coral.qEDGE$qEDGE==Inf] <- 1
  egv <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste("moll",n, ".grd", sep=""), full.names=T)
  EGV <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste("moll",n, ".grd", sep=""), full.names=F)
  EGV <- matrix(unlist(strsplit(EGV, "_")), ncol=2, byrow=T)[,1]
  vals <- list()
  for(i in 1:length(egv))
  {
    tmp <- raster(egv[i])
    temp <- data.frame(extract(tmp, coral.qEDGE[,c(1,2)], method="simple"))
    names(temp) <- EGV[i]
    vals <- c(vals, list(temp))
  }
  tmp <- as.data.frame(matrix(unlist(vals), ncol=length(egv)))
  names(tmp) <- EGV
  vals <- tmp
  PCvals <- princomp(vals[complete.cases(vals), -21], cor=T, scale=T)
  testDat <- cbind(coral.qEDGE, data.frame(glc=vals$glc))
  testDat <- testDat[complete.cases(vals),]
  testDat <- cbind(testDat, PCvals$scores[,1:8])
  egv.amph <- c(egv.amph, list(PCvals))
  sub.mod <- list()
  for(j in 1:100)
  {
    print(paste("Replicate ", j, " started.", sep=""))
    tDat <- testDat[sample(row.names(testDat), 1000 ), ]
    amod <- gam(qED ~ s(X, Y, k=400) + Comp.1+Comp.2+Comp.3+Comp.4+Comp.5+Comp.6+Comp.7+Comp.8, data=tDat, 
                select=T, method="P-REML")
    sub.mod <- c(sub.mod, list(amod))
  }
  amph.mod <- c(amph.mod, list(sub.mod))
}

coeff.amph <- list()
for(i in 1:8)
{
  print(i)
  for(j in 1:100)
  {
    print(j)
    coeff.amph <- c(coeff.amph, list(summary(amph.mod[[i]][[j]])$p.coeff))
  }
}

save(coeff.amph, file="/media/terra/EDGEII/amph_ED_coeff.Rdata")
save(amph.mod, file="/media/terra/EDGEII/amph_ED_models.Rdata")

############ Mammals now ###############
library(mgcv)
library(nlme)
library(MASS)
library(raster)
library(spdep)
library(rgdal)
rm(list = ls())
s <- c(25,50,75,100,125,150,175,200)
mammal.mod <- list()
egv.mammal <- list()
for(n in s)
{
  print(paste("Doing ", n, ".", sep=""))
  load(paste("/media/terra/EDGEII/data/mammal_", n, "_qEDGE.Rdata", sep=""))
  mammal.qEDGE <- mammal.qEDGE[complete.cases(mammal.qEDGE),]
  mammal.qEDGE$qEDGE[mammal.qEDGE$qEDGE==Inf] <- 1
  egv <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste(n, ".grd", sep=""), full.names=T)
  EGV <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste(n, ".grd", sep=""), full.names=F)
  EGV <- matrix(unlist(strsplit(EGV, "_")), ncol=2, byrow=T)[,1]
  vals <- list()
  for(i in 1:length(egv))
  {
    tmp <- raster(egv[i])
    temp <- data.frame(extract(tmp, mammal.qEDGE[,c(1,2)], method="simple"))
    names(temp) <- EGV[i]
    vals <- c(vals, list(temp))
  }
  tmp <- as.data.frame(matrix(unlist(vals), ncol=length(egv)))
  names(tmp) <- EGV
  vals <- tmp
  PCvals <- princomp(vals[complete.cases(vals), -21], cor=T, scale=T)
  testDat <- cbind(mammal.qEDGE, data.frame(glc=vals$glc))
  testDat <- testDat[complete.cases(vals),]
  testDat <- cbind(testDat, PCvals$scores[,1:8])
  egv.mammal <- c(egv.mammal, list(PCvals))
  sub.mod <- list()
  for(j in 1:100)
  {
    print(paste("Replicate ", j, " started.", sep=""))
    tDat <- testDat[sample(row.names(testDat), 1000 ), ]
    amod <- gam(qED ~ s(X, Y, k=400) + Comp.1+Comp.2+Comp.3+Comp.4+Comp.5+Comp.6+Comp.7+Comp.8, data=tDat, select=T)
    sub.mod <- c(sub.mod, list(amod))
  }
  mammal.mod <- c(mammal.mod, list(sub.mod))
}

coeff.mammal <- list()
for(i in 1:8)
{
  print(i)
  for(j in 1:100)
  {
    print(j)
    coeff.mammal <- c(coeff.mammal, list(summary(mammal.mod[[i]][[j]])$p.coeff))
  }
}

save(coeff.mammal, file="/media/terra/EDGEII/mammal_ED_coeff.Rdata")
save(mammal.mod, file="/media/terra/EDGEII/mammal_ED_models.Rdata")

######################## The same for EDGE #################################
library(mgcv)
library(nlme)
library(MASS)
library(raster)
library(spdep)
library(rgdal)
rm(list = ls())
s <- c(25,50,75,100,125,150,175,200)
amph.mod <- list()
egv.amph <- list()
for(n in s)
{
  print(paste("Doing ", n, ".", sep=""))
  load(paste("/media/terra/EDGEII/data/amph_", n, "_qEDGE.Rdata", sep=""))
  coral.qEDGE <- coral.qEDGE[complete.cases(coral.qEDGE),]
  coral.qEDGE$qEDGE[coral.qEDGE$qEDGE==Inf] <- 1
  egv <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste(n, ".grd", sep=""), full.names=T)
  EGV <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste(n, ".grd", sep=""), full.names=F)
  EGV <- matrix(unlist(strsplit(EGV, "_")), ncol=2, byrow=T)[,1]
  vals <- list()
  for(i in 1:length(egv))
  {
    tmp <- raster(egv[i])
    temp <- data.frame(extract(tmp, coral.qEDGE[,c(1,2)], method="simple"))
    names(temp) <- EGV[i]
    vals <- c(vals, list(temp))
  }
  tmp <- as.data.frame(matrix(unlist(vals), ncol=length(egv)))
  names(tmp) <- EGV
  vals <- tmp
  PCvals <- princomp(vals[complete.cases(vals), -21], cor=T, scale=T)
  testDat <- cbind(coral.qEDGE, data.frame(glc=vals$glc))
  testDat <- testDat[complete.cases(vals),]
  testDat <- cbind(testDat, PCvals$scores[,1:8])
  egv.amph <- c(egv.amph, list(PCvals))
  sub.mod <- list()
  for(j in 1:100)
  {
    print(paste("Replicate ", j, " started.", sep=""))
    tDat <- testDat[sample(row.names(testDat), 1000 ), ]
    amod <- gam(qEDGE ~ s(X, Y, k=400) + Comp.1+Comp.2+Comp.3+Comp.4+Comp.5+Comp.6+Comp.7+Comp.8, data=tDat, select=T)
    sub.mod <- c(sub.mod, list(amod))
  }
  amph.mod <- c(amph.mod, list(sub.mod))
}

coeff.amph <- list()
for(i in 1:8)
{
  print(i)
  for(j in 1:100)
  {
    print(j)
    coeff.amph <- c(coeff.amph, list(summary(amph.mod[[i]][[j]])$p.coeff))
  }
}

save(coeff.amph, file="/media/terra/EDGEII/amph_EDGE_coeff.Rdata")
save(amph.mod, file="/media/terra/EDGEII/amph_EDGE_models.Rdata")

############ Mammals now ###############
library(mgcv)
library(nlme)
library(MASS)
library(raster)
library(spdep)
library(rgdal)
rm(list = ls())
s <- c(25,50,75,100,125,150,175,200)
mammal.mod <- list()
egv.mammal <- list()
for(n in s)
{
  print(paste("Doing ", n, ".", sep=""))
  load(paste("/media/terra/EDGEII/data/mammal_", n, "_qEDGE.Rdata", sep=""))
  mammal.qEDGE <- mammal.qEDGE[complete.cases(mammal.qEDGE),]
  mammal.qEDGE$qEDGE[mammal.qEDGE$qEDGE==Inf] <- 1
  egv <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste(n, ".grd", sep=""), full.names=T)
  EGV <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste(n, ".grd", sep=""), full.names=F)
  EGV <- matrix(unlist(strsplit(EGV, "_")), ncol=2, byrow=T)[,1]
  vals <- list()
  for(i in 1:length(egv))
  {
    tmp <- raster(egv[i])
    temp <- data.frame(extract(tmp, mammal.qEDGE[,c(1,2)], method="simple"))
    names(temp) <- EGV[i]
    vals <- c(vals, list(temp))
  }
  tmp <- as.data.frame(matrix(unlist(vals), ncol=length(egv)))
  names(tmp) <- EGV
  vals <- tmp
  PCvals <- princomp(vals[complete.cases(vals), -21], cor=T, scale=T)
  testDat <- cbind(mammal.qEDGE, data.frame(glc=vals$glc))
  testDat <- testDat[complete.cases(vals),]
  testDat <- cbind(testDat, PCvals$scores[,1:8])
  egv.mammal <- c(egv.mammal, list(PCvals))
  sub.mod <- list()
  for(j in 1:100)
  {
    print(paste("Replicate ", j, " started.", sep=""))
    tDat <- testDat[sample(row.names(testDat), 1000 ), ]
    amod <- gam(qEDGE ~ s(X, Y, k=400) + Comp.1+Comp.2+Comp.3+Comp.4+Comp.5+Comp.6+Comp.7+Comp.8, data=tDat, select=T)
    sub.mod <- c(sub.mod, list(amod))
  }
  mammal.mod <- c(mammal.mod, list(sub.mod))
}

coeff.mammal <- list()
for(i in 1:8)
{
  print(i)
  for(j in 1:100)
  {
    print(j)
    coeff.mammal <- c(coeff.mammal, list(summary(mammal.mod[[i]][[j]])$p.coeff))
  }
}

save(coeff.mammal, file="/media/terra/EDGEII/mammal_EDGE_coeff.Rdata")
save(mammal.mod, file="/media/terra/EDGEII/mammal_EDGE_models.Rdata")


