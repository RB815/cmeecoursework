library(mgcv)
library(nlme)
library(MASS)
library(raster)
library(spdep)
library(rgdal)
rm(list = ls())
s <- c(25,50,75,100,125,150,175,200)
PCA_amph <- list()
for(n in s)
{
  print(paste("Doing ", n, ".", sep=""))
  load(paste("/media/terra/EDGEII/data/amph_", n, "_qEDGE.Rdata", sep=""))
  amph.qEDGE <- amph.qEDGE[complete.cases(amph.qEDGE),]
  amph.qEDGE$qEDGE[amph.qEDGE$qEDGE==Inf] <- 1
  egv <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste("moll", n, ".grd", sep=""), full.names=T)
  egv <- egv[-grep("glc", egv)]
  EGV <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste("moll",n, ".grd", sep=""), full.names=F)
  EGV <- EGV[-grep("glc", EGV)]
  EGV <- matrix(unlist(strsplit(EGV, "_")), ncol=2, byrow=T)[,1]
  vals <- list()
  for(i in 1:length(egv))
  {
    tmp <- raster(egv[i])
    temp <- data.frame(extract(tmp, amph.qEDGE[,c(1,2)], method="simple"))
    names(temp) <- EGV[i]
    vals <- c(vals, list(temp))
  }
  tmp <- as.data.frame(matrix(unlist(vals), ncol=length(egv)))
  names(tmp) <- EGV
  vals <- tmp
  PCvals <- princomp(vals[complete.cases(vals), ], cor=T, scale=T)
  PCA_amph <- c(PCA_amph, list(PCvals))
}


PCA_mammal <- list()
for(n in s)
{
  print(paste("Doing ", n, ".", sep=""))
  load(paste("/media/terra/EDGEII/data/mammal_", n, "_qEDGE.Rdata", sep=""))
  mammal.qEDGE <- mammal.qEDGE[complete.cases(mammal.qEDGE),]
  mammal.qEDGE$qEDGE[mammal.qEDGE$qEDGE==Inf] <- 1
  egv <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste("moll", n, ".grd", sep=""), full.names=T)
  egv <- egv[-grep("glc", egv)]
  EGV <- list.files("/media/terra/EDGEII/data/EGV/R-grids", pattern=paste("moll",n, ".grd", sep=""), full.names=F)
  EGV <- EGV[-grep("glc", EGV)]
  EGV <- matrix(unlist(strsplit(EGV, "_")), ncol=2, byrow=T)[,1]
  vals <- list()
  for(i in 1:length(egv))
  {
    tmp <- raster(egv[i])
    temp <- data.frame(extract(tmp, mammal.qEDGE[,c(1,2)], method="simple"))
    names(temp) <- EGV[i]
    vals <- c(vals, list(temp))
  }
  tmp <- as.data.frame(matrix(unlist(vals), ncol=length(egv)))
  names(tmp) <- EGV
  vals <- tmp
  PCvals <- princomp(vals[complete.cases(vals), ], cor=T, scale=T)
  PCA_mammal <- c(PCA_mammal, list(PCvals))
}

save(PCA_amph, PCA_mammal, file="/media/terra/EDGEII/Manuscript/Supplemental material/PCA-results.Rdata")