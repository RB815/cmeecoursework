#install following packages
library(maptools)
library(rgdal)

#edge.list <- read.delim("/home/kami/Documents/EDGE/Reanalysis/Data/Mammals/EDGE list 2.1.0.csv", sep=";", header=T)
edge.list <- read.csv("/home/qpc/EDGE Zones/TERRESTRIAL_MAMMALS/SPP FOR ZONES.csv")

proj4string <- CRS("+proj=moll +datum=WGS84") # choose which projection should be used to process the shapefiles

# remember this is not relevant for the actual shapefiles, the code
# assumes longlat data given as decimal degrees for the shapefiles

#root <- "/home/kami/Documents/Dropbox/Projects/Aktiv/Ben_collen/OLD/Data/Ranges/MAMMALS_terrestrial"
root <- "/home/qpc/EDGE Zones/EDGE_in_R/Shapefiles/Mammals_2016"


#############################################################################
maps <- data.frame((list.files(root, pattern=".shp", full.names=FALSE)),(list.files(root, pattern=".shp", full.names=TRUE)))
maps[1:100,]

names(maps) <- c("File", "Path")

maps[["Species"]] <- unlist(strsplit((as.character(maps$File)), ".shp", fixed = TRUE))

#maps <- maps[maps$Species %in% sub(" ", "_", edge.list$RedList.name),]

#find discrepancies in shapefile spp and EDGE spp **** Rikki has done this step *****
fix <- maps$Species[which(maps$Species %in% edge.list$Species == F)]
write.csv(fix, "/home/qpc/EDGE Zones/EDGE_in_R/Shapefiles/Mammals_2016/species_to_add.csv", row.names = F)

## remove redundant species from maps dataframe

maps <- maps[-which(maps$Species %in% edge.list$Species == F),]


nfiles <-length(maps$Path) #number of files to process

n <- 1
for (n in 1:nfiles) #goes through all files in the folder given under "root"
{
  temp <- readShapePoly((as.character(maps$Path[n])), proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
  #reads the shapefile with its unique identifier and its projection
  #temp <- nowrapSpatialPolygons(temp, offset = 180, eps=rep(.Machine$double.eps, 2))
  projTemp <- spTransform(temp,proj4string) #projects the longlat projection into a different projection	
  #jpeg(paste("/home/kami/Documents/Dropbox/Projects/Aktiv/EDGE/EDGEII/Range_images/", maps$Species[n], ".jpg", sep=""))
  jpeg(paste("/home/qpc/EDGE Zones/EDGE_in_R/Shapefiles/Mammals_2016/Mammal_images/", maps$Species[n], ".jpg", sep=""))
  plot(projTemp, lwd=2, col="grey")
  dev.off()
  print(n)
}
