# Open

rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/")

library(RNetCDF)

OpenDataDHW1 <- open.nc("Data/DegreeHeatingWeeks/dhw_b05kmnn_20160101.nc")
ReadDataDHW1 <- read.nc(OpenDataDHW1)
# > names(ReadDataDHW1)
# [1] "time"         "lat"          "lon"          "surface_flag" "CRW_DHW"      "crs"

length(ReadDataDHW1$lon)
ProjDataDHW1 <- ReadDataDHW1
# proj4string(ProjDataDHW1) <- CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
# TransformDataDHW1 <- spTransform(ProjDataDHW1, CRS("+proj=moll"))

Time1 <- var.get.nc(OpenDataDHW1, "time")
Lat1 <- var.get.nc(OpenDataDHW1, "lat")
Lon1 <- var.get.nc(OpenDataDHW1, "lon")
SFlag <- var.get.nc(OpenDataDHW1, "surface_flag")