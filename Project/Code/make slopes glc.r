lf <- list.files("/media/terra/EDGEII/cluster/output", full.names=T, pattern="amph_ED_coeff")
fn <- list.files("/media/terra/EDGEII/cluster/output", full.names=F, pattern="amph_ED_coeff")
fn <- unlist(strsplit(fn, ".Rdata"))
tab <- list()
for(i in 1:length(lf))
{
  load(lf[i])
  print("******************")
  print(lf[i])
  
  if(exists("coeff.amph"))
  {
    coeff <- coeff.amph
  }
  if(exists("coeff.mammal"))
  {
    coeff <- coeff.mammal
  }
  nam <- unique(unlist(lapply(coeff, names)))
  coeff.df <- as.data.frame(do.call('rbind',lapply(coeff,'[',unique(unlist(lapply(coeff, names))))))
  names(coeff.df) <- nam
  coeff.df <- coeff.df[,!names(coeff.df) %in% c("as.factor(glc)170", "as.factor(glc)190")]
  res <- data.frame(mean=NULL, SD=NULL, p=NULL)
  for(j in 1:ncol(coeff.df))
  {
    av <- mean(coeff.df[,j], na.rm=T)
    sdav <- sd(coeff.df[,j], na.rm=T)
    d <- ecdf(coeff.df[,j])
    print(paste(nam[j], ": Mean: ", round(av,4), " ; Quantile: ", d(0), ".", sep=""))
    res <- rbind(res, data.frame(mean=av, SD=sdav, p=d(0)))
  }
  write.csv(res, file=paste("/media/terra/EDGEII/", fn[i], ".csv", sep=""), row.names=F)
}


lf <- list.files("/media/terra/EDGEII/cluster/output", full.names=T, pattern="amph_EDGE_coeff")
fn <- list.files("/media/terra/EDGEII/cluster/output", full.names=F, pattern="amph_EDGE_coeff")
fn <- unlist(strsplit(fn, ".Rdata"))
tab <- list()
for(i in 1:length(lf))
{
  load(lf[i])
  print("******************")
  print(lf[i])
  
  if(exists("coeff.amph"))
  {
    coeff <- coeff.amph
  }
  if(exists("coeff.mammal"))
  {
    coeff <- coeff.mammal
  }
  nam <- unique(unlist(lapply(coeff, names)))
  coeff.df <- as.data.frame(do.call('rbind',lapply(coeff,'[',unique(unlist(lapply(coeff, names))))))
  names(coeff.df) <- nam
  coeff.df <- coeff.df[,!names(coeff.df) %in% c("as.factor(glc)170", "as.factor(glc)190")]
  res <- data.frame(mean=NULL, SD=NULL, p=NULL)
  for(j in 1:ncol(coeff.df))
  {
    av <- mean(coeff.df[,j], na.rm=T)
    sdav <- sd(coeff.df[,j], na.rm=T)
    d <- ecdf(coeff.df[,j])
    print(paste(nam[j], ": Mean: ", round(av,4), " ; Quantile: ", d(0), ".", sep=""))
    res <- rbind(res, data.frame(mean=av, SD=sdav, p=d(0)))
  }
  write.csv(res, file=paste("/media/terra/EDGEII/", fn[i], ".csv", sep=""), row.names=F)
}


lf <- list.files("/media/terra/EDGEII/cluster/output", full.names=T, pattern="mammal_ED_coeff")
fn <- list.files("/media/terra/EDGEII/cluster/output", full.names=F, pattern="mammal_ED_coeff")
fn <- unlist(strsplit(fn, ".Rdata"))
tab <- list()
for(i in 1:length(lf))
{
  load(lf[i])
  print("******************")
  print(lf[i])
  
  if(exists("coeff.amph"))
  {
    coeff <- coeff.amph
  }
  if(exists("coeff.mammal"))
  {
    coeff <- coeff.mammal
  }
  nam <- unique(unlist(lapply(coeff, names)))
  coeff.df <- as.data.frame(do.call('rbind',lapply(coeff,'[',unique(unlist(lapply(coeff, names))))))
  names(coeff.df) <- nam
  coeff.df <- coeff.df[,!names(coeff.df) %in% c("as.factor(glc)170", "as.factor(glc)190")]
  res <- data.frame(mean=NULL, SD=NULL, p=NULL)
  for(j in 1:ncol(coeff.df))
  {
    av <- mean(coeff.df[,j], na.rm=T)
    sdav <- sd(coeff.df[,j], na.rm=T)
    d <- ecdf(coeff.df[,j])
    print(paste(nam[j], ": Mean: ", round(av,4), " ; Quantile: ", d(0), ".", sep=""))
    res <- rbind(res, data.frame(mean=av, SD=sdav, p=d(0)))
  }
  write.csv(res, file=paste("/media/terra/EDGEII/", fn[i], ".csv", sep=""), row.names=F)
}



lf <- list.files("/media/terra/EDGEII/cluster/output", full.names=T, pattern="mammal_EDGE_coeff")
fn <- list.files("/media/terra/EDGEII/cluster/output", full.names=F, pattern="mammal_EDGE_coeff")
fn <- unlist(strsplit(fn, ".Rdata"))
tab <- list()
for(i in 1:length(lf))
{
  load(lf[i])
  print("******************")
  print(lf[i])
  
  if(exists("coeff.amph"))
  {
    coeff <- coeff.amph
  }
  if(exists("coeff.mammal"))
  {
    coeff <- coeff.mammal
  }
  nam <- unique(unlist(lapply(coeff, names)))
  coeff.df <- as.data.frame(do.call('rbind',lapply(coeff,'[',unique(unlist(lapply(coeff, names))))))
  names(coeff.df) <- nam
  coeff.df <- coeff.df[,!names(coeff.df) %in% c("as.factor(glc)170", "as.factor(glc)190")]
  res <- data.frame(mean=NULL, SD=NULL, p=NULL)
  for(j in 1:ncol(coeff.df))
  {
    av <- mean(coeff.df[,j], na.rm=T)
    sdav <- sd(coeff.df[,j], na.rm=T)
    d <- ecdf(coeff.df[,j])
    print(paste(nam[j], ": Mean: ", round(av,4), " ; Quantile: ", d(0), ".", sep=""))
    res <- rbind(res, data.frame(mean=av, SD=sdav, p=d(0)))
  }
  write.csv(res, file=paste("/media/terra/EDGEII/", fn[i], ".csv", sep=""), row.names=F)
}
