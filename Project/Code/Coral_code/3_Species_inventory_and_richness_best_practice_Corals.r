rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/")

library(maptools) # for readShapePoly function
library(rgdal)
library(reshape)
library(raster) # for raster function (to convert dataframes to raster files)
library(plyr)

for(q in seq(25000,200000,25000)) #start with 25000
{
  ################## map richness from distribution maps #################
  Gridsize <- q #in units of your projection 
  Proj.4.string <- CRS("+proj=moll +datum=WGS84") # choose which projection should be used to process the shapefiles
  # remember this is not relevant for the actual shapefiles, the code
  # assumes longlat data given as decimal degrees for the shapefiles
  root <- "Data/Shapefiles/Corals/"
  
  #############################################################################
  
  maps <- data.frame((list.files(root, pattern=".shp", full.names=FALSE)),(list.files(root, pattern=".shp", full.names=TRUE)))
  names(maps) <- c("File", "Path")
  maps[["species"]] <- unlist(strsplit((as.character(maps$File)), ".shp", fixed = TRUE))
  maps$species <- sub(" ", "_", maps$species)
  nfiles <-length(maps$Path) #number of files to process
  cont <- list() #initialises a list which will contain later the final data
  ############ this reads the range maps, point samples it and calculates the species richness ######################
  seq <- 1:nfiles
  for (n in seq) #for (n in 1:nfiles) #goes through all files in the folder given under "root"
  {
    temp <- readShapePoly((as.character(maps$Path[n])), proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
    #reads the shapefile with its unique identifier and its projection
    projTemp <- spTransform(temp,Proj.4.string) #projects the longlat projection into a different projection  
    projTemp$dummy <- 1
    bound <- bbox(projTemp) #the bounding box of the shape files is changed to match the grid
    bound[,1] <- (((floor(bound[,"min"]/Gridsize))*Gridsize)+(Gridsize/2))-Gridsize
    bound[,2] <- (((ceiling(bound[,"max"]/Gridsize))*Gridsize)-(Gridsize/2))+Gridsize
    grid <- SpatialPoints(GridTopology(bound[,"min"], c(Gridsize, Gridsize), c((bound[,"max"]-bound[,"min"])/Gridsize)))
    gridded(grid) <- TRUE
    grd <- raster(grid)
    rp <- rasterize(projTemp, grd)
    tempGridr <- data.frame(x=as.data.frame((coordinates(rp)[!is.na(values(rp)),"x"])), 
                            y=as.data.frame((coordinates(rp)[!is.na(values(rp)),"y"])))
    names(tempGridr) <- c("x", "y")
    tempGridp <- NULL
    if(any(!is.na(values(rp))))
    {
      projTemp <- projTemp[-(values(rp)[!is.na(values(rp))]),]
    }
    if(length(projTemp)>0)
    {
      for(m in 1:length(projTemp))
      {
        for(k in 1:length(projTemp@polygons[[m]]@Polygons))
        {
          if(projTemp@polygons[[m]]@Polygons[[k]]@hole==FALSE)
          {
            tempGridt <- data.frame(x=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[1])/Gridsize))*Gridsize)+(Gridsize/2),
                                    y=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[2])/Gridsize))*Gridsize)+(Gridsize/2))
            #gets the nearest X coordinate in the grid to the centroid
            #gets its nearest Y coordinate in the grid to the centroid
            # adds the presence of species (derived from the file name) to the coordinates of the nearest gridnames(tempGrid) <- c("X", "Y", "species")
          }
          tempGridp <- rbind(tempGridp, tempGridt)
        }
      }
    }
    tempGrid <- rbind(tempGridr, tempGridp)
    tempGrid <- unique(tempGrid)
    tempGrid$species <- (as.character(maps$Species[n])) 
    # adds the presence of species (derived from the file name) to the xy coords
    names(tempGrid) <- c("X", "Y", "species")
    cont[[n]] <- tempGrid #adds the current grid samples by the coordinates
    rm(list=ls()[grep("tempGrid", ls())])
    print(paste(n, " of ", nfiles, " @ resolution of ", Gridsize, " done!", sep=""))
  }
  cont <- rbind.fill(cont)
  cont <- unique(cont)
  # Save X,Y coordinates for each grid cell and the names of the specise in that grid cell for each resolution
  save(cont, file=paste("~/Documents/CMEECourseWork2015/Project/Data/Cont_Rdata/Coral_", Gridsize/1000 ,"_species_gridded(cont).Rdata", sep=""))
  cont1 <- cast(melt(cont, id=c("X","Y")), fun.aggregate=length)
  cont1 <- as.data.frame(cont1)
  # Save X,Y coordinates for each grid cell and the species richness in that grid cell for each resolution
  save(cont1,file=paste("~/Documents/CMEECourseWork2015/Project/Data/Cont_Rdata/Coral_", Gridsize/1000 , "_species_richness(cont1).Rdata", sep=""))
}