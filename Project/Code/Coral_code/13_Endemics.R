# Find the species with the 25% smallest range (this figure may need to be adjusted, subject to literature review)
# These are our (assumed) endemic species

# Read in csv file with names of species and their area cover
Corals <- read.csv("~/Documents/CMEECourseWork2015/Project/Data/EDR_Corals.csv")

# Calculate the 25th quantile
Area25 <- quantile(Corals$area, 0.25)

# Create a new dataframe for our 'endemic' species
Corals25 <- data.frame(Corals[1,c("X","binom","area","ED","EDGE","EDR")])

# Loop through each row in the original dataframe to find the species with an area within the bottom 25%. Remove na's.
for (i in 1:length(Corals$area)){
  if (Corals$area[i] <= Area25){
    Corals25[i,] <- Corals[i,]
  }
  Corals25 <- na.omit(Corals25)
}

# Remove the first row we created for formatting the new dataframe
Corals25 <- Corals25[-1,]

# Tidy up the dataframe
Corals25 <- Corals25[c("binom", "area", "ED", "EDGE","EDR")]
names(Corals25) <- c("species", "area", "ED", "EDGE", "EDR")


#### Mapping endemic species ####

# Load in species location data
load("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_100_species_gridded(cont).Rdata") # which cont resolution to use?

# Merge the area and location dataframes
cont.ed <- merge(cont, Corals25, by="species")

# Pick out the important columns, ensuring they all have completed information
Endemics <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "area")]),c("X", "Y", "area")]

# For each grid square, add together the area of each present species (sum area)
# coral.End <- as.data.frame(cast(melt(Endemics, id=c("X","Y")), fun.aggregate=sum))
# names(coral.End) <- c("X", "Y", "sumEndemic")

# For each grid square, add together the number of present species (species richness)
coral.End.l <- as.data.frame(cast(melt(Endemics, id=c("X","Y")), fun.aggregate=length))
names(coral.End.l) <- c("X", "Y", "richnessEndemic")

# Put cumulative areas and species richenss into the same dataframe
# coral.End <- merge(coral.End, coral.End.l, by=c("X", "Y"))

# Turn dataframe into a raster layer
coordinates (coral.End) <- ~X+Y # select columns 'X' and 'Y' as the coordinates for the map
gridded(coral.End) <- TRUE # make sure it's gridded so that everything matches up nicely
coralEnd <- raster(coral.End) # finally turn into image form

# world map
ghhs <- readShapePoly("/home/gjp15/Documents/CMEECourseWork2015/Project/Code/GSHHS_l_L1.shp", force_ring=T)
proj4string(ghhs) <- CRS("+proj=longlat +datum=WGS84")
ghhs <- spTransform(ghhs, CRS("+proj=moll"))
plot(ghhs, col = "grey", main = "Most Endemic Corals, 100km res")

# Add endemic species to map as a layer ontop of the world map
plot(coralEnd, add=T)
