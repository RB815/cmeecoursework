library(maptools)
library(rgdal)
library(rgeos)
library(raster)
library(reshape)
library(plyr)

root <- "~/Documents/CMEECourseWork2015/Project/Data/Shapefiles/"
edlist <- read.csv("~/Documents/CMEECourseWork2015/Project/Data/ALL_corals_EDGE_scores.csv", sep=",")
ed <- edlist[, c("ED", "EDGE", "Species")]

for(q in seq(25000,200000,25000)) #start with 25000
{
  ################## map richness from distribution maps #################
  Gridsize <- q #in units of your projection 
  Proj4String <- CRS("+proj=moll +datum=WGS84") # choose which projection should be used to process the shapefiles
  # remember this is not relevant for the actual shapefiles, the code
  # assumes longlat data given as decimal degrees for the shapefiles
  root <- "~/Documents/CMEECourseWork2015/Project/Data/Shapefiles/"
  #############################################################################
  maps <- data.frame((list.files(root, pattern=".shp", full.names=FALSE)),(list.files(root, pattern=".shp", full.names=TRUE)))
  names(maps) <- c("File", "Path")
  maps[["Species"]] <- unlist(strsplit((as.character(maps$File)), ".shp", fixed = TRUE))
  # maps <- maps[maps$Species %in% as.character(ed[order(-ed$ED),][1:ceiling(10*(nrow(ed)/100)),"Species"]),]
  maps <- maps[maps$Species %in% as.character(ed[order(-ed$ED),][1:ceiling(10*(nrow(ed)/100)),"Species"]),]
  nfiles <-length(maps$Path) #number of files to process
  cont <- list() #initialises a list which will contain later the final data
  ############ this reads the range maps, point samples it and calculates the species richness ######################
  seq <- 1:nfiles
  for (n in seq) #for (n in 1:nfiles) #goes through all files in the folder given under "root"
  {
    temp <- readShapePoly((as.character(maps$Path[n])), proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
    #reads the shapefile with its unique identifier and its projection
    projTemp <- spTransform(temp,Proj4String) #projects the longlat projection into a different projection  
    projTemp$dummy <- 1
    bound <- bbox(projTemp) #the bounding box of the shape files is changed to match the grid
    bound[,1] <- (((floor(bound[,"min"]/Gridsize))*Gridsize)+(Gridsize/2))-Gridsize
    bound[,2] <- (((ceiling(bound[,"max"]/Gridsize))*Gridsize)-(Gridsize/2))+Gridsize
    grid <- SpatialPoints(GridTopology(bound[,"min"], c(Gridsize, Gridsize), c((bound[,"max"]-bound[,"min"])/Gridsize)))
    gridded(grid) <- TRUE
    grd <- raster(grid)
    rp <- rasterize(projTemp, grd)
    tempGridr <- data.frame(x=as.data.frame((coordinates(rp)[!is.na(values(rp)),"x"])), 
                            y=as.data.frame((coordinates(rp)[!is.na(values(rp)),"y"])))
    names(tempGridr) <- c("x", "y")
    tempGridp <- NULL
    if(any(!is.na(values(rp))))
    {
      projTemp <- projTemp[-(values(rp)[!is.na(values(rp))]),]
    }
    if(length(projTemp)>0)
    {
      for(m in 1:length(projTemp))
      {
        for(k in 1:length(projTemp@polygons[[m]]@Polygons))
        {
          if(projTemp@polygons[[m]]@Polygons[[k]]@hole==FALSE)
          {
            tempGridt <- data.frame(x=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[1])/Gridsize))*Gridsize)+(Gridsize/2),
                                    y=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[2])/Gridsize))*Gridsize)+(Gridsize/2))
            #gets the nearest X coordinate in the grid to the centroid
            #gets its nearest Y coordinate in the grid to the centroid
            # adds the presence of species (derived from the file name) to the coordinates of the nearest gridnames(tempGrid) <- c("X", "Y", "species")
          }
          tempGridp <- rbind(tempGridp, tempGridt)
        }
      }
    }
    tempGrid <- rbind(tempGridr, tempGridp)
    tempGrid <- unique(tempGrid)
    tempGrid$species <- (as.character(maps$Species[n])) 
    # adds the presence of species (derived from the file name) to the xy coords
    names(tempGrid) <- c("X", "Y", "species")
    cont[[n]] <- tempGrid #adds the current grid samples by the coordinates
    rm(list=ls()[grep("tempGrid", ls())])
    print(paste(n, " of ", nfiles, " @ resolution of ", Gridsize, " done!", sep=""))
  }
  cont <- rbind.fill(cont)
  cont <- unique(cont)
  save(cont, file=paste("~/Documents/CMEECourseWork2015/Project/Data/Rdata/top10pctED_corals_", Gridsize/1000 ,"_species_gridded(cont).Rdata", sep=""))
  cont1 <- cast(melt(cont, id=c("X","Y")), fun.aggregate=length)
  cont1 <- as.data.frame(cont1)
  save(cont1,file=paste("~/Documents/CMEECourseWork2015/Project/Data/Rdata/top10pctED_corals_", Gridsize/1000 , "_species_richness(cont1).Rdata", sep=""))
}


######################## Now EDGE ###############################

for(q in seq(25000,200000,25000)) #start with 25000
{
  ################## map richness from distribution maps #################
  Gridsize <- q #in units of your projection 
  Proj4String <- CRS("+proj=moll +datum=WGS84") # choose which projection should be used to process the shapefiles
  # remember this is not relevant for the actual shapefiles, the code
  # assumes longlat data given as decimal degrees for the shapefiles
  #############################################################################
  maps <- data.frame((list.files(root, pattern=".shp", full.names=FALSE)),(list.files(root, pattern=".shp", full.names=TRUE)))
  names(maps) <- c("File", "Path")
  maps[["Species"]] <- unlist(strsplit((as.character(maps$File)), ".shp", fixed = TRUE))
  maps <- maps[maps$Species %in% as.character(ed[order(-ed$EDGE),][1:ceiling(10*(nrow(ed)/100)),"Species"]),]
  nfiles <-length(maps$Path) #number of files to process
  cont <- list() #initialises a list which will contain later the final data
  ############ this reads the range maps, point samples it and calculates the species richness ######################
  seq <- 1:nfiles
  for (n in seq) #for (n in 1:nfiles) #goes through all files in the folder given under "root"
  {
    temp <- readShapePoly((as.character(maps$Path[n])), proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
    #reads the shapefile with its unique identifier and its projection
    projTemp <- spTransform(temp,Proj4String) #projects the longlat projection into a different projection  
    projTemp$dummy <- 1
    bound <- bbox(projTemp) #the bounding box of the shape files is changed to match the grid
    bound[,1] <- (((floor(bound[,"min"]/Gridsize))*Gridsize)+(Gridsize/2))-Gridsize
    bound[,2] <- (((ceiling(bound[,"max"]/Gridsize))*Gridsize)-(Gridsize/2))+Gridsize
    grid <- SpatialPoints(GridTopology(bound[,"min"], c(Gridsize, Gridsize), c((bound[,"max"]-bound[,"min"])/Gridsize)))
    gridded(grid) <- TRUE
    grd <- raster(grid)
    rp <- rasterize(projTemp, grd)
    tempGridr <- data.frame(x=as.data.frame((coordinates(rp)[!is.na(values(rp)),"x"])), 
                            y=as.data.frame((coordinates(rp)[!is.na(values(rp)),"y"])))
    names(tempGridr) <- c("x", "y")
    tempGridp <- NULL
    if(any(!is.na(values(rp))))
    {
      projTemp <- projTemp[-(values(rp)[!is.na(values(rp))]),]
    }
    if(length(projTemp)>0)
    {
      for(m in 1:length(projTemp))
      {
        for(k in 1:length(projTemp@polygons[[m]]@Polygons))
        {
          if(projTemp@polygons[[m]]@Polygons[[k]]@hole==FALSE)
          {
            tempGridt <- data.frame(x=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[1])/Gridsize))*Gridsize)+(Gridsize/2),
                                    y=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[2])/Gridsize))*Gridsize)+(Gridsize/2))
            #gets the nearest X coordinate in the grid to the centroid
            #gets its nearest Y coordinate in the grid to the centroid
            # adds the presence of species (derived from the file name) to the coordinates of the nearest gridnames(tempGrid) <- c("X", "Y", "species")
          }
          tempGridp <- rbind(tempGridp, tempGridt)
        }
      }
    }
    tempGrid <- rbind(tempGridr, tempGridp)
    tempGrid <- unique(tempGrid)
    tempGrid$species <- (as.character(maps$Species[n])) 
    # adds the presence of species (derived from the file name) to the xy coords
    names(tempGrid) <- c("X", "Y", "species")
    cont[[n]] <- tempGrid #adds the current grid samples by the coordinates
    rm(list=ls()[grep("tempGrid", ls())])
    print(paste(n, " of ", nfiles, " @ resolution of ", Gridsize, " done!", sep=""))
  }
  cont <- rbind.fill(cont)
  cont <- unique(cont)
  save(cont, file=paste("~/Documents/CMEECourseWork2015/Project/Data/10percent/top10pctEDGE_Coral_", Gridsize/1000 ,"_species_gridded(cont).Rdata", sep=""))
  cont1 <- cast(melt(cont, id=c("X","Y")), fun.aggregate=length)
  cont1 <- as.data.frame(cont1)
  save(cont1,file=paste("~/Documents/CMEECourseWork2015/Project/Data/10percent/top10pctEDGE_Coral_", Gridsize/1000 , "_species_richness(cont1).Rdata", sep=""))
}

#### For EDR ####

edlist <- read.csv("~/Documents/CMEECourseWork2015/Project/Data/EDR_Corals.csv", sep=",")
# ed <- edlist[, c("ED", "EDGE", "Species")]
ed <- edlist[, c("binom", "EDR")]
names(ed) <- c("Species", "EDR")


for(q in seq(25000,200000,25000)) #start with 25000
{
  ################## map richness from distribution maps #################
  Gridsize <- q #in units of your projection 
  Proj4String <- CRS("+proj=moll +datum=WGS84") # choose which projection should be used to process the shapefiles
  # remember this is not relevant for the actual shapefiles, the code
  # assumes longlat data given as decimal degrees for the shapefiles
  root <- "~/Documents/CMEECourseWork2015/Project/Data/Shapefiles/"
  #############################################################################
  maps <- data.frame((list.files(root, pattern=".shp", full.names=FALSE)),(list.files(root, pattern=".shp", full.names=TRUE)))
  names(maps) <- c("File", "Path")
  maps[["Species"]] <- unlist(strsplit((as.character(maps$File)), ".shp", fixed = TRUE))
  # maps <- maps[maps$Species %in% as.character(ed[order(-ed$ED),][1:ceiling(10*(nrow(ed)/100)),"Species"]),]
  maps <- maps[maps$Species %in% as.character(ed[order(-ed$EDR),][1:ceiling(10*(nrow(ed)/100)),"Species"]),]
  nfiles <-length(maps$Path) #number of files to process
  cont <- list() #initialises a list which will contain later the final data
  ############ this reads the range maps, point samples it and calculates the species richness ######################
  seq <- 1:nfiles
  for (n in seq) #for (n in 1:nfiles) #goes through all files in the folder given under "root"
  {
    temp <- readShapePoly((as.character(maps$Path[n])), proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
    #reads the shapefile with its unique identifier and its projection
    projTemp <- spTransform(temp,Proj4String) #projects the longlat projection into a different projection  
    projTemp$dummy <- 1
    bound <- bbox(projTemp) #the bounding box of the shape files is changed to match the grid
    bound[,1] <- (((floor(bound[,"min"]/Gridsize))*Gridsize)+(Gridsize/2))-Gridsize
    bound[,2] <- (((ceiling(bound[,"max"]/Gridsize))*Gridsize)-(Gridsize/2))+Gridsize
    grid <- SpatialPoints(GridTopology(bound[,"min"], c(Gridsize, Gridsize), c((bound[,"max"]-bound[,"min"])/Gridsize)))
    gridded(grid) <- TRUE
    grd <- raster(grid)
    rp <- rasterize(projTemp, grd)
    tempGridr <- data.frame(x=as.data.frame((coordinates(rp)[!is.na(values(rp)),"x"])), 
                            y=as.data.frame((coordinates(rp)[!is.na(values(rp)),"y"])))
    names(tempGridr) <- c("x", "y")
    tempGridp <- NULL
    if(any(!is.na(values(rp))))
    {
      projTemp <- projTemp[-(values(rp)[!is.na(values(rp))]),]
    }
    if(length(projTemp)>0)
    {
      for(m in 1:length(projTemp))
      {
        for(k in 1:length(projTemp@polygons[[m]]@Polygons))
        {
          if(projTemp@polygons[[m]]@Polygons[[k]]@hole==FALSE)
          {
            tempGridt <- data.frame(x=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[1])/Gridsize))*Gridsize)+(Gridsize/2),
                                    y=((floor((projTemp@polygons[[m]]@Polygons[[k]]@labpt[2])/Gridsize))*Gridsize)+(Gridsize/2))
            #gets the nearest X coordinate in the grid to the centroid
            #gets its nearest Y coordinate in the grid to the centroid
            # adds the presence of species (derived from the file name) to the coordinates of the nearest gridnames(tempGrid) <- c("X", "Y", "species")
          }
          tempGridp <- rbind(tempGridp, tempGridt)
        }
      }
    }
    tempGrid <- rbind(tempGridr, tempGridp)
    tempGrid <- unique(tempGrid)
    tempGrid$species <- (as.character(maps$Species[n])) 
    # adds the presence of species (derived from the file name) to the xy coords
    names(tempGrid) <- c("X", "Y", "species")
    cont[[n]] <- tempGrid #adds the current grid samples by the coordinates
    rm(list=ls()[grep("tempGrid", ls())])
    print(paste(n, " of ", nfiles, " @ resolution of ", Gridsize, " done!", sep=""))
  }
  cont <- rbind.fill(cont)
  cont <- unique(cont)
  save(cont, file=paste("~/Documents/CMEECourseWork2015/Project/Data/Rdata/top10pctEDR_corals_", Gridsize/1000 ,"_species_gridded(cont).Rdata", sep=""))
  cont1 <- cast(melt(cont, id=c("X","Y")), fun.aggregate=length)
  cont1 <- as.data.frame(cont1)
  save(cont1,file=paste("~/Documents/CMEECourseWork2015/Project/Data/Rdata/top10pctEDR_corals_", Gridsize/1000 , "_species_richness(cont1).Rdata", sep=""))
}
