# Convert species range shapefiles into jpeg images

rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/")

library(maptools)
library(rgdal) # for CRS function 

# Load species names, ED, GE, and EDGE scores
edge.list <- read.csv("Data/Coral_csv/ALL_corals_EDGE_scores.csv")

# Tidy corals dataframe
edge.list <- edge.list[c("Species", "ED", "GE", "EDGE")] # Only keep wanted columns
names(edge.list) <- c("species", "ED", "GE", "EDGE") # rename to keep in line with other files
edge.list$species <- sub(" ", "_", edge.list$species) # remove spaces from species names
coral.edge.list <- edge.list # rename to be more specific
write.csv(coral.edge.list, "Data/Coral_csv/ALL_corals_EDGE_scores.csv") # save

# choose which projection should be used to process the shapefiles
Proj.4.string <- CRS("+proj=moll +datum=WGS84") # Mollweide projection is best

# remember this is not relevant for the actual shapefiles, the code
# assumes longlat data given as decimal degrees for the shapefiles

root <- "Data/Shapefiles/Corals/"

#############################################################################
# Read in .shp file names and pathways
maps <- data.frame((list.files(root, pattern=".shp", full.names=FALSE)),(list.files(root, pattern=".shp", full.names=TRUE)))

names(maps) <- c("File", "Path") # rename columns

# Add column and fill with species names, changing format (if needed) to match coral species list
maps[["species"]] <- unlist(strsplit((as.character(maps$File)), ".shp", fixed = TRUE))
maps$species <- sub(" ", "_", maps$species)

# find discrepancies in shapefile spp and EDGE spp
Fix <- maps$species[which(maps$species %in% edge.list$species == F)]
write.csv(Fix, "Data/Coral_csv/species_to_add.csv", row.names = F)

# remove redundant species from maps dataframe
maps <- maps[which(maps$species %in% edge.list$species == T),]

nfiles <-length(maps$Path) #number of files to process


for (n in 1:nfiles) #goes through all files in the folder given under "root"
{
  temp <- readShapePoly((as.character(maps$Path[n])), proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
  #reads the shapefile with its unique identifier and its projection
  #temp <- nowrapSpatialPolygons(temp, offset = 180, eps=rep(.Machine$double.eps, 2))
  projTemp <- spTransform(temp,Proj.4.string) #projects the longlat projection into a different projection	
  jpeg(paste("Data/Jpeg_output/Corals/", maps$species[n], ".jpg", sep="")) # save jpeg files
  plot(projTemp, lwd=2)
  dev.off()
  print(n)
}