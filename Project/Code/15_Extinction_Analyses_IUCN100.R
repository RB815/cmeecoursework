library(maptools)
library(rgdal)
library(reshape)
library(raster)
library(plyr)
library(data.table)
library(reshape2)

# open up corals csv with species names, ED, and GE scores
edlist <- read.csv("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/ALL_corals_EDGE_scores.csv", header = T, stringsAsFactors = F) 

# Using Mooers et al. (2008) projected probabilities of extinction in 100 years, give each species probability based on GE score
for (i in 1:length(edlist$Species)){
  if (edlist$GE[i] == 0){
    edlist[["PEXT"]][i] <- 0.0001 # Least Concern = 0.01%
  } 
  else if (edlist$GE[i] == 1){
    edlist[["PEXT"]][i] <- 0.01 # Near Threatened = 0.1%
  } 
  else if (edlist$GE[i] == 2){
    edlist[["PEXT"]][i] <- 0.1 # Vulnerable = 1%
  }
  else if (edlist$GE[i] == 3){
    edlist[["PEXT"]][i] <- 0.667 # Endangered = 66.7%
  }
  else if (edlist$GE[i] == 4){
    edlist[["PEXT"]][i] <- 0.999 # Critically Endangered = 99.9%
  }
}


Pext_corals100 <- edlist[, c("ED", "PEXT", "Species")] # Select only required columns and change name to be more descriptive
Pext_corals100 <- na.omit(Pext_corals100) # Remove data deficient species

Pext_corals100$Species <- as.character(Pext_corals100$Species) # Convert Species column to character format

save(Pext_corals100, file = "/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Pext_corals100.csv") # Save the new dataframe

#############################################################################################################################

## combine amphibian, bird and mammal grid cell data
# load("/home/qpc/EDGE Zones/AMPHIBIANS/DATA/amph_100_species_gridded(cont).Rdata")
# cont_amph <- cont

# load("/home/qpc/EDGE Zones/Birds/DATA/100_species_gridded(cont).Rdata")
# cont_bird <- cont
# load("/home/qpc/EDGE Zones/EDGE_in_R/newdata/mammal_100_species_gridded(cont).Rdata")
# cont_mam <- cont

# cont_all <- rbind(cont_mam, cont_bird, cont_amp)
# head(cont_all)
# save(cont_all, file = "/home/qpc/EDGE Zones/EXT/DATA/cont_all.RData")
# load("/home/qpc/EDGE Zones/EXT/DATA/cont_all.RData")
# cont_all <- cont_all[which(cont_all$species %in% ed$species),]

# Load Rdata file containing species names and X & Y coordinates
load("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_100_species_gridded(cont).Rdata")
cont_coral <- cont

######### drop species from 'cont_all' based on their 'extinction' from the 'edlist', as defined by their PEXT #######

# Load Rdata file containing species names and X & Y coordinates
load("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_100_species_gridded(cont).Rdata")
cont_coral <- cont

# Load csv file with species names, ED scores and extinction risk
ed <- load("../Data/Pext_corals100.csv")

## create a list of conts based on the extinct species for each iteration
cont_ext_list <- list()

## create loop to generate N iterations of extinction scenarios
## I do this in four runs of 25, and use 'k' to represent each run. Increase to about 1000(?) runs on HPC
k <- 1
for (k in 1:4){
  for(m in 1:25){
    survivors <- NULL # create empty object to fill with 'extant' species
  # drop species based on their PEXT and create vector of 'extant' species
  for(i in 1:length(ed$Species)){
    if(rbinom(1, 1, 1-ed$PEXT[i]) == 1){
      survivors[length(survivors)+1] <- ed$Species[i]
    }
  }
  # cont_all_EXT <- cont_all[which(cont_all$species %in% survivors),]
  #Merge it with ED scores file and tidy, removing blanks
  cont.ed <- merge(cont_coral, ed, by="Species")
  #   ED <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "ED")]),c("X", "Y", "ED")]
  ED <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "ED")]),c("X", "Y", "ED")]
  #Now total up the ED scores per grid square using cast function
  coral.ED <- as.data.frame(cast(melt(ED, id=c("X","Y")), fun.aggregate=sum))
  names(coral.ED) <- c("X", "Y", "sumED")
  cont_ext_list[[length(cont_ext_list)+1]] <- coral.ED
  print(m)
  }
}
save(cont_ext_list, file = paste("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/cont_sumED_ext_list_IUCN100_", k,".RData", sep = ""))


load("//home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/cont_sumED_ext_list_IUCN100_1.RData")
### combine lists

## read in lists
k <- 1
load(file = paste("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/cont_sumED_ext_list_IUCN100_", k,".RData", sep = ""))
c1 <- cont_ext_list
k <- 2
load(file = paste("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/cont_sumED_ext_list_IUCN100_", k,".RData", sep = ""))
c2 <- cont_ext_list
k <- 3
load(file = paste("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/cont_sumED_ext_list_IUCN100_", k,".RData", sep = ""))
c3 <- cont_ext_list
k <- 4
load(file = paste("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/cont_sumED_ext_list_IUCN100_", k,".RData", sep = ""))
c4 <- cont_ext_list

#combine into one list

c_all <- list()
for(i in 1:25){
  c_all[[length(c_all)+1]] <- c1[[i]]
}
for(i in 1:25){
  c_all[[length(c_all)+1]] <- c2[[i]]
}
for(i in 1:25){
  c_all[[length(c_all)+1]] <- c3[[i]]
}
for(i in 1:25){
  c_all[[length(c_all)+1]] <- c4[[i]]
}
save(c_all, file = "/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/cont_sumED_ext_list_IUCN100_all.RData")

## load in all lists
load(file = "../Data/Rdata/cont_sumED_ext_list_IUCN100_all.RData") # large list called c_all

# identify unique grid cells for analysis

cont_coral[["unique_XY"]] <- paste(cont_coral$X, cont_coral$Y, sep = "_")
head(cont_coral)
new_xy <- unique(cont_coral$unique_XY)
# head(new_xy)
# tail(new_xy)
new_xy <- as.data.frame(new_xy)
names(new_xy) <- c("XY")
# length(new_xy$XY)
for(i in 1:length(new_xy$XY)){
  new_xy[["X"]][i] <- strsplit(as.character(new_xy$XY[i]), "_", fixed = T)[[1]][1]
  new_xy[["Y"]][i] <- strsplit(as.character(new_xy$XY[i]), "_", fixed = T)[[1]][2]
}

# get distribution and mean for each grid cell following extinctions
new_xy[["mean_sumED"]] <- rep(NA, length(new_xy$XY)) # create empty column

for(i in 1:length(new_xy$XY)){
  distr <- NULL # create an empty list
  for(j in 1:length(c_all)){
    hm <- c_all[[j]]$sumED[which(c_all[[j]]$X == new_xy$X[i] # where the X and Y coordinates in c_all match those in new_xy, add 
                                 & c_all[[j]]$Y == new_xy$Y[i])] # sum value from c_all to hm
    if(length(hm) >0){ # If there are any species within this coordinate...
      distr[length(distr)+1] <- hm  # ...add to dist list
    }
  }
  new_xy$mean_sumED[i] <- mean(distr) # calculate the mean distribution from all 4 runs and add it to mean_sumED
  print(i)
}

# new_xy <- n_all

save(new_xy, file = "../Data/Rdata/EXT_ED_loss_trial_100_1.RData")

## get sumED for different groups before extinction to pool together

load("../Data/Pext_corals.csv") # called Pext_corals
load("../Data/Rdata/EXT_ED_loss_trial_100_1.RData") # called new_xy
#n_all$X <- as.numeric(n_all$X)
new_xy$X <- as.numeric(new_xy$X)

#ext.ED <- n_all
ext.ED <- new_xy

ext.ED$X <- as.numeric(ext.ED$X)
ext.ED$Y <- as.numeric(ext.ED$Y)
ext.ED <- as.data.frame(ext.ED)
ext.ED <- ext.ED[,c("X", "Y", "mean_sumED")]
str(ext.ED)

coordinates (ext.ED) <- ~X+Y
gridded (ext.ED) <- TRUE
extED <- raster (ext.ED)
plot(extED)

# coordinates (all.ED) <- ~X+Y
coordinates (coral.ED) <- ~X+Y
#gridded (all.ED) <- TRUE
gridded (coral.ED) <- TRUE
#allED <- raster (all.ED)
allED <- raster (coral.ED)
plot(allED)


# scenario <- data.frame(X=all.ED$X, Y=all.ED$Y, current=all.ED$sumED, 
#                        future=rep(NA, length(all.ED$X)), loss=rep(NA, length(all.ED$X)), 
#                        proportion_lost=rep(NA, length(all.ED$X)))
# head(scenario)

scenario <- data.frame(X=ext.ED$X, Y=ext.ED$Y, current=ext.ED$mean_sumED, 
                       future=rep(NA, length(ext.ED$X)), loss=rep(NA, length(ext.ED$X)), 
                       proportion_lost=rep(NA, length(ext.ED$X)))
head(scenario)

# for(i in 1:length(scenario$current)){
#   scenario$future[i] <- n_all$mean_sumED[which(n_all$X == all.ED$X[i] & n_all$Y == all.ED$Y[i])]
#   scenario$loss[i] <- scenario$current[i] - n_all$mean_sumED[which(n_all$X == all.ED$X[i] & n_all$Y == all.ED$Y[i])]
# }

for(i in 1:length(scenario$current)){
  scenario$future[i] <- new_xy$mean_sumED[which(new_xy$X == ext.ED$X[i] & new_xy$Y == ext.ED$Y[i])]
  scenario$loss[i] <- scenario$current[i] - new_xy$mean_sumED[which(new_xy$X == ext.ED$X[i] & new_xy$Y == ext.ED$Y[i])]
}


for(i in 1:length(scenario$loss)){
  scenario$proportion_lost[i] <- scenario$loss[i]/scenario$current[i]
}

save(scenario, file = "/home/qpc/EDGE Zones/EXT/DATA/scenario_100_1_trial.RData")

lossplot <- scenario[,c("X", "Y", "loss")]

coordinates (lossplot) <- ~X+Y
gridded(lossplot) <- TRUE
lossplot <- raster(lossplot)
plot(lossplot)

propplot <- scenario[,c("X", "Y", "proportion_lost")]

coordinates (propplot) <- ~X+Y
gridded(propplot) <- TRUE
scplot <- raster(propplot)
plot(scplot)