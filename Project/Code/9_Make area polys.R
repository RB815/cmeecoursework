library(sp)
library(raster)
library(rgdal)
library(rgeos)
library(maptools)

##########

load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_25_species_gridded(cont).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_25_species_richness(cont1).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_25_qEDGE.Rdata")
head(cont1)
head(Coral.qEDGE)
cont1$t10 <- 1
coordinates(cont1) <- ~X+Y
gridded(cont1) <- TRUE
Coral.qEDGE$eda <- 0
Coral.qEDGE$eda[Coral.qEDGE$qED>0.975 & Coral.qEDGE$richnessED > 2] <- 1
Coral.qEDGE$edgea <- 0
Coral.qEDGE$edgea[Coral.qEDGE$qEDGE>0.975 & Coral.qEDGE$richnessEDGE > 2] <- 1
coordinates(Coral.qEDGE) <- ~X+Y
gridded(Coral.qEDGE) <- TRUE

t10 <- raster(cont1, layer=2)
eda <- raster(Coral.qEDGE, layer=7)
edgea <- raster(Coral.qEDGE, layer=8)
t10p <- rasterToPolygons(t10, dissolve=T)
edap <- rasterToPolygons(eda, function(x){x==1}, dissolve=T)
edgeap <- rasterToPolygons(edgea, function(x){x==1}, dissolve=T)
EDarea25 <- gIntersection(edap, t10p)
EDGEarea25 <- gIntersection(edgeap, t10p)
if(class(EDarea25)=="SpatialCollections")
{
  EDarea25 <- gUnionCascaded(EDarea25@polyobj)
}
if(class(EDGEarea25)=="SpatialCollections")
{
  EDGEarea25 <- gUnionCascaded(EDGEarea25@polyobj)
}
EDarea25 <- spChFIDs(EDarea25, make.names(rep(paste("a.ED", ".25", sep=""), length(EDarea25)+1), unique=T)[-1])
EDGEarea25 <- spChFIDs(EDGEarea25, make.names(rep(paste("a.EDGE", ".25", sep=""), length(EDGEarea25)+1), unique=T)[-1])
EDarea25 <- SpatialPolygonsDataFrame(EDarea25, 
                                     data.frame(Res=rep(25, length(EDarea25)) , 
                                                Cat=rep("ED", length(EDarea25)), 
                                                Taxon=rep("Coral", length(EDarea25)),
                                                row.names=make.names(rep(paste("a.ED", ".25", sep=""), 
                                                                         length(EDarea25)+1), unique=T)[-1]))
EDGEarea25 <- SpatialPolygonsDataFrame(EDGEarea25, 
                                     data.frame(Res=rep(25, length(EDGEarea25)) , 
                                                Cat=rep("EDGE", length(EDGEarea25)), 
                                                Taxon=rep("Coral", length(EDGEarea25)),
                                                row.names=make.names(rep(paste("a.EDGE", ".25", sep=""), 
                                                                         length(EDGEarea25)+1), unique=T)[-1]))
##########
##########
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_50_species_gridded(cont).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_50_species_richness(cont1).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_50_qEDGE.Rdata")
head(cont1)
head(Coral.qEDGE)
cont1$t10 <- 1
coordinates(cont1) <- ~X+Y
gridded(cont1) <- TRUE
Coral.qEDGE$eda <- 0
Coral.qEDGE$eda[Coral.qEDGE$qED>0.975 & Coral.qEDGE$richnessED > 2] <- 1
Coral.qEDGE$edgea <- 0
Coral.qEDGE$edgea[Coral.qEDGE$qEDGE>0.975 & Coral.qEDGE$richnessEDGE > 2] <- 1
coordinates(Coral.qEDGE) <- ~X+Y
gridded(Coral.qEDGE) <- TRUE

t10 <- raster(cont1, layer=2)
eda <- raster(Coral.qEDGE, layer=7)
edgea <- raster(Coral.qEDGE, layer=8)
t10p <- rasterToPolygons(t10, dissolve=T)
edap <- rasterToPolygons(eda, function(x){x==1}, dissolve=T)
edgeap <- rasterToPolygons(edgea, function(x){x==1}, dissolve=T)
EDarea50 <- gIntersection(edap, t10p)
EDGEarea50 <- gIntersection(edgeap, t10p)
if(class(EDarea50)=="SpatialCollections")
{
  EDarea50 <- gUnionCascaded(EDarea50@polyobj)
}
if(class(EDGEarea50)=="SpatialCollections")
{
  EDGEarea50 <- gUnionCascaded(EDGEarea50@polyobj)
}
EDarea50 <- spChFIDs(EDarea50, make.names(rep(paste("ED", ".50", sep=""), length(EDarea50)), unique=T))
EDGEarea50 <- spChFIDs(EDGEarea50, make.names(rep(paste("EDGE", ".50", sep=""), length(EDGEarea50)), unique=T))
EDarea50 <- SpatialPolygonsDataFrame(EDarea50, 
                                     data.frame(Res=rep(50, length(EDarea50)) , 
                                                Cat=rep("ED", length(EDarea50)), 
                                                Taxon=rep("Coral", length(EDarea50)),
                                                row.names=make.names(rep(paste("ED", ".50", sep=""), 
                                                                         length(EDarea50)), unique=T)))
EDGEarea50 <- SpatialPolygonsDataFrame(EDGEarea50, 
                                     data.frame(Res=rep(50, length(EDGEarea50)) , 
                                                Cat=rep("EDGE", length(EDGEarea50)), 
                                                Taxon=rep("Coral", length(EDGEarea50)),
                                                row.names=make.names(rep(paste("EDGE", ".50", sep=""), 
                                                                         length(EDGEarea50)), unique=T)))
##########
##########
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_75_species_gridded(cont).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_75_species_richness(cont1).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_75_qEDGE.Rdata")
head(cont1)
head(Coral.qEDGE)
cont1$t10 <- 1
coordinates(cont1) <- ~X+Y
gridded(cont1) <- TRUE
Coral.qEDGE$eda <- 0
Coral.qEDGE$eda[Coral.qEDGE$qED>0.975 & Coral.qEDGE$richnessED > 2] <- 1
Coral.qEDGE$edgea <- 0
Coral.qEDGE$edgea[Coral.qEDGE$qEDGE>0.975 & Coral.qEDGE$richnessEDGE > 2] <- 1
coordinates(Coral.qEDGE) <- ~X+Y
gridded(Coral.qEDGE) <- TRUE

t10 <- raster(cont1, layer=2)
eda <- raster(Coral.qEDGE, layer=7)
edgea <- raster(Coral.qEDGE, layer=8)
t10p <- rasterToPolygons(t10, dissolve=T)
edap <- rasterToPolygons(eda, function(x){x==1}, dissolve=T)
edgeap <- rasterToPolygons(edgea, function(x){x==1}, dissolve=T)
EDarea75 <- gIntersection(edap, t10p)
EDGEarea75 <- gIntersection(edgeap, t10p)
if(class(EDarea75)=="SpatialCollections")
{
  EDarea75 <- gUnionCascaded(EDarea75@polyobj)
}
if(class(EDGEarea75)=="SpatialCollections")
{
  EDGEarea75 <- gUnionCascaded(EDGEarea75@polyobj)
}
EDarea75 <- spChFIDs(EDarea75, make.names(rep(paste("ED", ".75", sep=""), length(EDarea75)), unique=T))
EDGEarea75 <- spChFIDs(EDGEarea75, make.names(rep(paste("EDGE", ".75", sep=""), length(EDGEarea75)), unique=T))
EDarea75 <- SpatialPolygonsDataFrame(EDarea75, 
                                     data.frame(Res=rep(75, length(EDarea75)) , 
                                                Cat=rep("ED", length(EDarea75)), 
                                                Taxon=rep("Coral", length(EDarea75)),
                                                row.names=make.names(rep(paste("ED", ".75", sep=""), 
                                                                         length(EDarea75)), unique=T)))
EDGEarea75 <- SpatialPolygonsDataFrame(EDGEarea75, 
                                     data.frame(Res=rep(75, length(EDGEarea75)) , 
                                                Cat=rep("EDGE", length(EDGEarea75)), 
                                                Taxon=rep("Coral", length(EDGEarea75)),
                                                row.names=make.names(rep(paste("EDGE", ".75", sep=""), 
                                                                         length(EDGEarea75)), unique=T)))
##########
##########
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_100_species_gridded(cont).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_100_species_richness(cont1).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_100_qEDGE.Rdata")
head(cont1)
head(Coral.qEDGE)
cont1$t10 <- 1
coordinates(cont1) <- ~X+Y
gridded(cont1) <- TRUE
Coral.qEDGE$eda <- 0
Coral.qEDGE$eda[Coral.qEDGE$qED>0.975 & Coral.qEDGE$richnessED > 2] <- 1
Coral.qEDGE$edgea <- 0
Coral.qEDGE$edgea[Coral.qEDGE$qEDGE>0.975 & Coral.qEDGE$richnessEDGE > 2] <- 1
coordinates(Coral.qEDGE) <- ~X+Y
gridded(Coral.qEDGE) <- TRUE

t10 <- raster(cont1, layer=2)
eda <- raster(Coral.qEDGE, layer=7)
edgea <- raster(Coral.qEDGE, layer=8)
t10p <- rasterToPolygons(t10, dissolve=T)
edap <- rasterToPolygons(eda, function(x){x==1}, dissolve=T)
edgeap <- rasterToPolygons(edgea, function(x){x==1}, dissolve=T)
EDarea100 <- gIntersection(edap, t10p)
EDGEarea100 <- gIntersection(edgeap, t10p)
if(class(EDarea100)=="SpatialCollections")
{
  EDarea100 <- gUnionCascaded(EDarea100@polyobj)
}
if(class(EDGEarea100)=="SpatialCollections")
{
  EDGEarea100 <- gUnionCascaded(EDGEarea100@polyobj)
}
EDarea100 <- spChFIDs(EDarea100, make.names(rep(paste("ED", ".100", sep=""), length(EDarea100)), unique=T))
EDGEarea100 <- spChFIDs(EDGEarea100, make.names(rep(paste("EDGE", ".100", sep=""), length(EDGEarea100)), unique=T))
EDarea100 <- SpatialPolygonsDataFrame(EDarea100, 
                                     data.frame(Res=rep(100, length(EDarea100)) , 
                                                Cat=rep("ED", length(EDarea100)), 
                                                Taxon=rep("Coral", length(EDarea100)),
                                                row.names=make.names(rep(paste("ED", ".100", sep=""), 
                                                                         length(EDarea100)), unique=T)))
EDGEarea100 <- SpatialPolygonsDataFrame(EDGEarea100, 
                                     data.frame(Res=rep(100, length(EDGEarea100)) , 
                                                Cat=rep("EDGE", length(EDGEarea100)), 
                                                Taxon=rep("Coral", length(EDGEarea100)),
                                                row.names=make.names(rep(paste("EDGE", ".100", sep=""), 
                                                                         length(EDGEarea100)), unique=T)))
##########
##########
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_125_species_gridded(cont).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_125_species_richness(cont1).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_125_qEDGE.Rdata")
head(cont1)
head(Coral.qEDGE)
cont1$t10 <- 1
coordinates(cont1) <- ~X+Y
gridded(cont1) <- TRUE
Coral.qEDGE$eda <- 0
Coral.qEDGE$eda[Coral.qEDGE$qED>0.975 & Coral.qEDGE$richnessED > 2] <- 1
Coral.qEDGE$edgea <- 0
Coral.qEDGE$edgea[Coral.qEDGE$qEDGE>0.975 & Coral.qEDGE$richnessEDGE > 2] <- 1
coordinates(Coral.qEDGE) <- ~X+Y
gridded(Coral.qEDGE) <- TRUE

t10 <- raster(cont1, layer=2)
eda <- raster(Coral.qEDGE, layer=7)
edgea <- raster(Coral.qEDGE, layer=8)
t10p <- rasterToPolygons(t10, dissolve=T)
edap <- rasterToPolygons(eda, function(x){x==1}, dissolve=T)
edgeap <- rasterToPolygons(edgea, function(x){x==1}, dissolve=T)
EDarea125 <- gIntersection(edap, t10p)
EDGEarea125 <- gIntersection(edgeap, t10p)
if(class(EDarea125)=="SpatialCollections")
{
  EDarea125 <- gUnionCascaded(EDarea125@polyobj)
}
if(class(EDGEarea125)=="SpatialCollections")
{
  EDGEarea125 <- gUnionCascaded(EDGEarea125@polyobj)
}
EDarea125 <- spChFIDs(EDarea125, make.names(rep(paste("ED", ".125", sep=""), length(EDarea125)), unique=T))
EDGEarea125 <- spChFIDs(EDGEarea125, make.names(rep(paste("EDGE", ".125", sep=""), length(EDGEarea125)), unique=T))
EDarea125 <- SpatialPolygonsDataFrame(EDarea125, 
                                     data.frame(Res=rep(125, length(EDarea125)) , 
                                                Cat=rep("ED", length(EDarea125)), 
                                                Taxon=rep("Coral", length(EDarea125)),
                                                row.names=make.names(rep(paste("ED", ".125", sep=""), 
                                                                         length(EDarea125)), unique=T)))
EDGEarea125 <- SpatialPolygonsDataFrame(EDGEarea125, 
                                     data.frame(Res=rep(125, length(EDGEarea125)) , 
                                                Cat=rep("EDGE", length(EDGEarea125)), 
                                                Taxon=rep("Coral", length(EDGEarea125)),
                                                row.names=make.names(rep(paste("EDGE", ".125", sep=""), 
                                                                         length(EDGEarea125)), unique=T)))
##########
##########
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_150_species_gridded(cont).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_150_species_richness(cont1).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_150_qEDGE.Rdata")
head(cont1)
head(Coral.qEDGE)
cont1$t10 <- 1
coordinates(cont1) <- ~X+Y
gridded(cont1) <- TRUE
Coral.qEDGE$eda <- 0
Coral.qEDGE$eda[Coral.qEDGE$qED>0.975 & Coral.qEDGE$richnessED > 2] <- 1
Coral.qEDGE$edgea <- 0
Coral.qEDGE$edgea[Coral.qEDGE$qEDGE>0.975 & Coral.qEDGE$richnessEDGE > 2] <- 1
coordinates(Coral.qEDGE) <- ~X+Y
gridded(Coral.qEDGE) <- TRUE

t10 <- raster(cont1, layer=2)
eda <- raster(Coral.qEDGE, layer=7)
edgea <- raster(Coral.qEDGE, layer=8)
t10p <- rasterToPolygons(t10, dissolve=T)
edap <- rasterToPolygons(eda, function(x){x==1}, dissolve=T)
edgeap <- rasterToPolygons(edgea, function(x){x==1}, dissolve=T)
EDarea150 <- gIntersection(edap, t10p)
EDGEarea150 <- gIntersection(edgeap, t10p)
if(class(EDarea150)=="SpatialCollections")
{
  EDarea150 <- gUnionCascaded(EDarea150@polyobj)
}
if(class(EDGEarea150)=="SpatialCollections")
{
  EDGEarea150 <- gUnionCascaded(EDGEarea150@polyobj)
}
EDarea150 <- spChFIDs(EDarea150, make.names(rep(paste("ED", ".150", sep=""), length(EDarea150)), unique=T))
EDGEarea150 <- spChFIDs(EDGEarea150, make.names(rep(paste("EDGE", ".150", sep=""), length(EDGEarea150)), unique=T))
EDarea150 <- SpatialPolygonsDataFrame(EDarea150, 
                                     data.frame(Res=rep(150, length(EDarea150)) , 
                                                Cat=rep("ED", length(EDarea150)), 
                                                Taxon=rep("Coral", length(EDarea150)),
                                                row.names=make.names(rep(paste("ED", ".150", sep=""), 
                                                                         length(EDarea150)), unique=T)))
EDGEarea150 <- SpatialPolygonsDataFrame(EDGEarea150, 
                                     data.frame(Res=rep(150, length(EDGEarea150)) , 
                                                Cat=rep("EDGE", length(EDGEarea150)), 
                                                Taxon=rep("Coral", length(EDGEarea150)),
                                                row.names=make.names(rep(paste("EDGE", ".150", sep=""), 
                                                                         length(EDGEarea150)), unique=T)))
##########
##########
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_175_species_gridded(cont).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_175_species_richness(cont1).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_175_qEDGE.Rdata")
head(cont1)
head(Coral.qEDGE)
cont1$t10 <- 1
coordinates(cont1) <- ~X+Y
gridded(cont1) <- TRUE
Coral.qEDGE$eda <- 0
Coral.qEDGE$eda[Coral.qEDGE$qED>0.975 & Coral.qEDGE$richnessED > 2] <- 1
Coral.qEDGE$edgea <- 0
Coral.qEDGE$edgea[Coral.qEDGE$qEDGE>0.975 & Coral.qEDGE$richnessEDGE > 2] <- 1
coordinates(Coral.qEDGE) <- ~X+Y
gridded(Coral.qEDGE) <- TRUE

t10 <- raster(cont1, layer=2)
eda <- raster(Coral.qEDGE, layer=7)
edgea <- raster(Coral.qEDGE, layer=8)
t10p <- rasterToPolygons(t10, dissolve=T)
edap <- rasterToPolygons(eda, function(x){x==1}, dissolve=T)
edgeap <- rasterToPolygons(edgea, function(x){x==1}, dissolve=T)
EDarea175 <- gIntersection(edap, t10p)
EDGEarea175 <- gIntersection(edgeap, t10p)
if(class(EDarea175)=="SpatialCollections")
{
  EDarea175 <- gUnionCascaded(EDarea175@polyobj)
}
if(class(EDGEarea175)=="SpatialCollections")
{
  EDGEarea175 <- gUnionCascaded(EDGEarea175@polyobj)
}
EDarea175 <- spChFIDs(EDarea175, make.names(rep(paste("ED", ".175", sep=""), length(EDarea175)), unique=T))
EDGEarea175 <- spChFIDs(EDGEarea175, make.names(rep(paste("EDGE", ".175", sep=""), length(EDGEarea175)), unique=T))
EDarea175 <- SpatialPolygonsDataFrame(EDarea175, 
                                     data.frame(Res=rep(175, length(EDarea175)) , 
                                                Cat=rep("ED", length(EDarea175)), 
                                                Taxon=rep("Coral", length(EDarea175)),
                                                row.names=make.names(rep(paste("ED", ".175", sep=""), 
                                                                         length(EDarea175)), unique=T)))
EDGEarea175 <- SpatialPolygonsDataFrame(EDGEarea175, 
                                     data.frame(Res=rep(175, length(EDGEarea175)) , 
                                                Cat=rep("EDGE", length(EDGEarea175)), 
                                                Taxon=rep("Coral", length(EDGEarea175)),
                                                row.names=make.names(rep(paste("EDGE", ".175", sep=""), 
                                                                         length(EDGEarea175)), unique=T)))
##########
##########
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_200_species_gridded(cont).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_200_species_richness(cont1).Rdata")
load("~/Documents/CMEECourseWork2015/Project/Data/Rdata/Coral_200_qEDGE.Rdata")
head(cont1)
head(Coral.qEDGE)
cont1$t10 <- 1
coordinates(cont1) <- ~X+Y
gridded(cont1) <- TRUE
Coral.qEDGE$eda <- 0
Coral.qEDGE$eda[Coral.qEDGE$qED>0.975 & Coral.qEDGE$richnessED > 2] <- 1
Coral.qEDGE$edgea <- 0
Coral.qEDGE$edgea[Coral.qEDGE$qEDGE>0.975 & Coral.qEDGE$richnessEDGE > 2] <- 1
coordinates(Coral.qEDGE) <- ~X+Y
gridded(Coral.qEDGE) <- TRUE

t10 <- raster(cont1, layer=2)
eda <- raster(Coral.qEDGE, layer=7)
edgea <- raster(Coral.qEDGE, layer=8)
t10p <- rasterToPolygons(t10, dissolve=T)
edap <- rasterToPolygons(eda, function(x){x==1}, dissolve=T)
edgeap <- rasterToPolygons(edgea, function(x){x==1}, dissolve=T)
EDarea200 <- gIntersection(edap, t10p)
EDGEarea200 <- gIntersection(edgeap, t10p)
if(class(EDarea200)=="SpatialCollections")
{
  EDarea200 <- gUnionCascaded(EDarea200@polyobj)
}
if(class(EDGEarea200)=="SpatialCollections")
{
  EDGEarea200 <- gUnionCascaded(EDGEarea200@polyobj)
}
EDarea200 <- spChFIDs(EDarea200, make.names(rep(paste("ED", ".200", sep=""), length(EDarea200)), unique=T))
EDGEarea200 <- spChFIDs(EDGEarea200, make.names(rep(paste("EDGE", ".200", sep=""), length(EDGEarea200)), unique=T))
EDarea200 <- SpatialPolygonsDataFrame(EDarea200, 
                                     data.frame(Res=rep(200, length(EDarea200)) , 
                                                Cat=rep("ED", length(EDarea200)), 
                                                Taxon=rep("Coral", length(EDarea200)),
                                                row.names=make.names(rep(paste("ED", ".200", sep=""), 
                                                                         length(EDarea200)), unique=T)))
EDGEarea200 <- SpatialPolygonsDataFrame(EDGEarea200, 
                                     data.frame(Res=rep(200, length(EDGEarea200)) , 
                                                Cat=rep("EDGE", length(EDGEarea200)), 
                                                Taxon=rep("Coral", length(EDGEarea200)),
                                                row.names=make.names(rep(paste("EDGE", ".200", sep=""), 
                                                                         length(EDGEarea200)), unique=T)))
##########

EDGEarea <- rbind(EDarea25, EDGEarea25, EDarea50, EDGEarea50, EDarea75, EDGEarea75, EDarea100, EDGEarea100, 
                  EDarea125, EDGEarea125, EDarea150, EDGEarea150, EDarea175, EDGEarea175, EDarea200, EDGEarea200)


EDGEarea <- spChFIDs(EDGEarea, make.names(seq(1:length(EDGEarea))))