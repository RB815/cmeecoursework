library(PBSmapping)
library(gpclib)

#The range of the polar bear needed adjustment. I downloaded the latest range from the single species page of the redlist.org and ran the below.

#temp <- readShapePoly("/home/qpc/EDGE Zones/EDGE_in_R/Shapefiles/Mammals_2016/Ursus maritimus.shp", proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
temp <- readShapePoly("~/Documents/CMEECourseWork2015/Project/Data/Jpeg_output/Acanthastrea bowerbanki.jpg", proj4string=CRS("+proj=longlat"), force_ring=TRUE) 

plot(temp)
temp <- SpatialPolygons2PolySet(temp)

data(worldLLhigh)
wrld <- refocusWorld(worldLLhigh, xlim=c(-180,180), ylim=c(0,90))
test <- joinPolys(temp, wrld, operation="INT")
test1 <- PolySet2SpatialPolygons(test)
test2 <- SpatialPolygonsDataFrame(test1, data.frame(BINOM=rep("Ursus_maritimus", 321), row.names=names(test1)))
#test2 <- SpatialPolygonsDataFrame(test1, data.frame(BINOM=rep("Abrocoma_boliviensis", 321), row.names=names(test1)))
test3 <- unionSpatialPolygons(test2, test2$BINOM)
test4 <- SpatialPolygonsDataFrame(test3, data.frame(BINOM=rep("Ursus_maritimus"), row.names=names(test3)))
#test4 <- SpatialPolygonsDataFrame(test3, data.frame(BINOM=rep("Abrocoma_boliviensis"), row.names=names(test3)))
writePolyShape(test4, "/home/kami/Documents/Dropbox/Projects/Aktiv/Ben_collen/OLD/Data/Ranges/MAMMALS_terrestrial/Ursus_maritimus")
#writePolyShape(test4, "F:/EDGE_in_R/Shapefiles/Mammal_Images/Abrocoma_boliviensis")

plot(test2)
