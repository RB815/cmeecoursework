rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/")

#Look at ED or EDGE heatmap using cont files

library(maptools) # for readShapePoly function to load map
library(rgdal)
library(reshape)
library(raster)

#Open cont file from data set
load("/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Rdata/M_Mammals/Marine_Mammal_100_species_gridded(cont).Rdata") # which cont resolution to use?

#open EDGE scores file
edlist <- read.csv("~/Documents/CMEECourseWork2015/Project/Data/Marine_Mammal_csv/EDGE_Marine_Mammals.csv", sep = ",") 
ed <- edlist[, c("species", "ED", "EDGE")]
# ed <- edlist[, c("binom", "EDR")]
# names(ed) <- c("species", "EDR") # remove and replace "species" with "Species" in script

#Merge it with EDGE scores file and tidy into ED and EDGE, removing blanks
cont.ed <- merge(cont_mamm, ed, by="species")
ED <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "ED")]),c("X", "Y", "ED")]
EDGE <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "EDGE")]),c("X", "Y", "EDGE")]
# EDR <- cont.ed[complete.cases(cont.ed[,c("X", "Y", "EDR")]),c("X", "Y", "EDR")]

#Now total up the ED or EDGE scores per grid square using cast function
Mamm.ED <- as.data.frame(cast(melt(ED, id=c("X","Y")), fun.aggregate=sum))
names(Mamm.ED) <- c("X", "Y", "sumED")
Mamm.ED.l <- as.data.frame(cast(melt(ED, id=c("X","Y")), fun.aggregate=length))
names(Mamm.ED.l) <- c("X", "Y", "richnessED")
Mamm.EDGE <- as.data.frame(cast(melt(EDGE, id=c("X","Y")), fun.aggregate=sum))
names(Mamm.EDGE) <- c("X", "Y", "sumEDGE")
Mamm.EDGE.l <- as.data.frame(cast(melt(EDGE, id=c("X","Y")), fun.aggregate=length))
names(Mamm.EDGE.l) <- c("X", "Y", "richnessEDGE")

# Mamm.EDR <- as.data.frame(cast(melt(EDR, id=c("X","Y")), fun.aggregate=sum))
# names(Mamm.EDR) <- c("X", "Y", "sumEDR")
# Mamm.EDR.l <- as.data.frame(cast(melt(EDR, id=c("X","Y")), fun.aggregate=length))
# names(Mamm.EDR.l) <- c("X", "Y", "richnessEDR")

Mamm.ED <- merge(Mamm.ED, Mamm.ED.l, by=c("X", "Y"))
Mamm.EDGE <- merge(Mamm.EDGE, Mamm.EDGE.l, by=c("X", "Y"))
# Mamm.EDR <- merge(Mamm.EDR, Mamm.EDR.l, by=c("X", "Y"))

#plot the maps

# world map
ghhs <- readShapePoly("/home/gjp15/Documents/CMEECourseWork2015/Project/Code/GSHHS_l_L1.shp", force_ring=T)
proj4string(ghhs) <- CRS("+proj=longlat +datum=WGS84")
ghhs <- spTransform(ghhs, CRS("+proj=moll"))

coordinates (Mamm.ED) <- ~X+Y
gridded(Mamm.ED) <- TRUE
MammED <- raster (Mamm.ED)
# plot(ghhs, col = "grey", main = "Marine Mammal ED")
# plot(MammED)

coordinates (Mamm.EDGE) <- ~X+Y
gridded(Mamm.EDGE) <- TRUE
MammEDGE <- raster (Mamm.EDGE)
plot(ghhs, col = "grey", main = "1")
plot(MammEDGE, add = T)

# coordinates (Mamm.EDR) <- ~X+Y
# gridded(Mamm.EDR) <- TRUE
# MammEDR <- raster (Mamm.EDR)
# plot(MammEDR)

# plot(ghhs, col = "grey", main = "EDR25")
# plot(MammEDR, add=T)

# plot(ghhs, col = "grey", main = "ED Corals Distribution 100km res")
# plot(MammED, add=T)

# Find top 25% and 50% of sites
# top25sumEDR <- max(Mamm.EDR$sumEDR)/100*75
# top50sumEDR <- max(Mamm.EDR$sumEDR)/100*50
# 
writeRaster(MammEDGE, "Data/Map_Rasters/MammEDGEzones_100kmRes", format = "raster")
writeRaster(MammED, "Data/Map_Rasters/MammEDzones_100kmRes", format = "raster")
# writeRaster(MammEDR, "Data/Map_Rasters/MammEDRzones_100kmRes", format = "raster")
