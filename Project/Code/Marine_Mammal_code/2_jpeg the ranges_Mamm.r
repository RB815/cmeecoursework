rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/")

#install following packages
library(maptools)
library(rgdal) # for CRS function which sets the projection of the shapefiles

edge.list <- read.csv("Data/Marine_Mammal_csv/EDGE_Marine_Mammals.csv")

Proj.4.string <- CRS("+proj=moll +datum=WGS84") # choose which projection should be used to process the shapefiles

# remember this is not relevant for the actual shapefiles, the code
# assumes longlat data given as decimal degrees for the shapefiles

root <- "/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Shapefiles/Marine_Mammals/"

maps <- data.frame((list.files(root, pattern=".shp", full.names=FALSE)),(list.files(root, pattern=".shp", full.names=TRUE)))#[1:123]
names(maps) <- c("File", "Path")
maps[["species"]] <- unlist(strsplit((as.character(maps$File)), ".shp", fixed = TRUE))[1:length(maps$File)]
# Fix format to match EDGE list
maps$species <- sub(" ", "_", maps$species)

#find discrepancies in shapefile spp and EDGE spp **** Rikki has done this step *****
# Fix <- maps$species[which(maps$species %in% edge.list$species == F)]
Fix <- edge.list$species[which(edge.list$species %in% maps$species == F)]
write.csv(Fix, "~/Documents/CMEECourseWork2015/Project/Data/Marine_Mammal_csv/mammal_species_to_add.csv", row.names = F)

## remove redundant species from maps dataframe
maps <- maps[which(maps$species %in% edge.list$species == T),]
ed <- edge.list[which(edge.list$species %in% maps$species == T),]
write.csv(ed, "~/Documents/CMEECourseWork2015/Project/Data/Marine_Mammal_csv/All_Marine_Mammals.csv", row.names = F)
ed <- ed[which(ed$GE != "DD"),]
M_Mammals <- ed
write.csv(M_Mammals, "~/Documents/CMEECourseWork2015/Project/Data/Marine_Mammal_csv/EDGE_Marine_Mammals.csv", row.names = F)


nfiles <-length(maps$Path) #number of files to process

for (n in 1:nfiles) #goes through all files in the folder given under "root"
{
  temp <- readShapePoly((as.character(maps$Path[n])), proj4string=CRS("+proj=longlat"), force_ring=TRUE) 
  #reads the shapefile with its unique identifier and its projection
  #temp <- nowrapSpatialPolygons(temp, offset = 180, eps=rep(.Machine$double.eps, 2))
  projTemp <- spTransform(temp,Proj.4.string) #projects the longlat projection into a different projection	
  jpeg(paste("~/Documents/CMEECourseWork2015/Project/Data/Jpeg_output/M_Mammals/", maps$species[n], ".jpg", sep=""))
  plot(projTemp, lwd=2)
  dev.off()
  print(n)
}
