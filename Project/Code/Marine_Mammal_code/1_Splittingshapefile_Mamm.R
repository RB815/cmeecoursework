# Split marine mammals shapefiles
rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/")

# First, install and load 'sp' and 'rgdal' packages:

library(sp)
library(rgdal)

# Load marine mammals shapefile .shp, ensuring the in quotation marks matches the file name without extension

Data <- readOGR(file.choose(), "MARINE_MAMMALS") 

# As we want to get one shapefile for each species, we will choose the 'binomial' variable. 
#In that way, we need determine the names and the number of species we are using.

Unique <- unique(Data@data$binomial)

# Finally, we use a loop to save shapefiles for each species. 

for (i in 1:length(Unique)) {
  tmp <- Data[Data$binomial == Unique[i], ] 
writeOGR(tmp, dsn="/home/gjp15/Documents/CMEECourseWork2015/Project/Data/Shapefiles/Marine_Mammals/", layer = Unique[i], driver="ESRI Shapefile",
overwrite_layer=TRUE)
}