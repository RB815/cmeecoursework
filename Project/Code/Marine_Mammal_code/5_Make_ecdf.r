#EDGE randomisation

rm(list=ls())
graphics.off()
setwd("~/Documents/CMEECourseWork2015/Project/")

# First get the species names and area of the ranges
library(maptools)

# Create a dataframe of shapefile paths and species names
lf <- data.frame(path = as.character(list.files("~/Documents/CMEECourseWork2015/Project/Data/Shapefiles/Marine_Mammals/", full.names=T, pattern=".shp")),
                 species = sub(".shp", "", list.files("~/Documents/CMEECourseWork2015/Project/Data/Shapefiles/Marine_Mammals/", full.names=F, pattern=".shp")))
lf$species <- sub(" ", "_", lf$species)

# Add area column
lf$area <- NA

seq <- 1:length(lf$path)

#
for(i in seq)
{
  tmp<-list()
  x <- readShapePoly(as.character(lf$path[i])) # reads each shapefile in database
  for (v in 1:length(x))
  {  
    tmp <- c(tmp, list(c((x@polygons[[v]]@Polygons[[1]]@hole),x@polygons[[v]]@Polygons[[1]]@area)))
  }
  tmp1 <- as.data.frame(matrix(unlist(tmp),ncol=2,byrow=T))
  lf$area[i] <- sum(tmp1$V2[tmp1$V1==0])
  print(paste(i, " of ", length(lf$path), " done."))
}

# load csv of species with EDGE, ED, GE values
edlist <- read.csv("~/Documents/CMEECourseWork2015/Project/Data/Marine_Mammal_csv/EDGE_Marine_Mammals.csv", sep=",")

# Extract just the species name, ED, EDGE
ed <- edlist[, c("ED", "EDGE", "species")]

# create a dataframe with species name, area, ED score and EDGE score, merging the ed and lf dataframes
lfm <- merge(lf[, c("species", "area")], ed , by="species", all=F)
  

r <- function(data, var, size, weight) {
  sum(data[sample(1:nrow(data), size, prob=weight, replace=TRUE), var])
}

Mamm_ED_ecdf <- list() # create empty list
tmpdat <- lfm[!is.na(lfm$ED),] # remove na's from ED column in lfm dataframe
for(j in 1:length(lfm$species)) # :max species richness
{
  tmp <- ecdf(replicate(1000, r(tmpdat, "ED", j, tmpdat$area)))
  Mamm_ED_ecdf <- c(Mamm_ED_ecdf, tmp)
  print(j)
}


Mamm_EDGE_ecdf <- list()
tmpdat <- lfm[complete.cases(lfm[, c("ED", "EDGE")]),]
for(j in 1:length(lfm$species))
{
  tmp <- ecdf(replicate(1000, r(tmpdat, "EDGE", j, tmpdat$area)))
  Mamm_EDGE_ecdf <- c(Mamm_EDGE_ecdf, tmp)
  print(j)
}

save(Mamm_ED_ecdf, Mamm_EDGE_ecdf, file="~/Documents/CMEECourseWork2015/Project/Data/Rdata/Mammal_ecdf.rdata")
