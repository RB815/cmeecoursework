library(rgeos)
library(raster)
library(sp)
library(maptools)
load("/media/terra/EDGEII/Manuscript/Supplemental material/EDGEarea.Rdata")
PA.africa <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Africa/ksafi-orn-mpg-de-search-1360788424502.shp", proj4string=CRS("+proj=longlat +datum=WGS84"))
PA.americas <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Americas/ksafi-orn-mpg-de-search-1360789190800.shp", proj4string=CRS("+proj=longlat +datum=WGS84"))
PA.europe <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Europe/ksafi-orn-mpg-de-search-1360789321407.shp", proj4string=CRS("+proj=longlat +datum=WGS84"))
PA.asia <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Asia/ksafi-orn-mpg-de-search-136078923051.shp", proj4string=CRS("+proj=longlat +datum=WGS84"))
PA.oceania <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Oceania/ksafi-orn-mpg-de-search-1360789480412.shp")
# need to fix Oceania 

for(i in 1:length(PA.oceania))
{
  for(j in 1:length(PA.oceania@polygons[[i]]@Polygons))
  {
    if(any(abs(diff(PA.oceania@polygons[[i]]@Polygons[[j]]@coords[,1])) > 180))
    {
      if(modal(PA.oceania@polygons[[i]]@Polygons[[j]]@coords[,1]) > 0)
      {
        PA.oceania@polygons[[i]]@Polygons[[j]]@coords[PA.oceania@polygons[[i]]@Polygons[[j]]@coords[,1] < 0 , 1] <- 179.9999
      }else{
        PA.oceania@polygons[[i]]@Polygons[[j]]@coords[PA.oceania@polygons[[i]]@Polygons[[j]]@coords[,1] > 0 , 1] <- -179.9999
      }
    }
  }
}

for(i in 1:length(PA.oceania))
{
  for(j in 1:length(PA.oceania@polygons[[i]]@Polygons))
  {
    if(any(abs(PA.oceania@polygons[[i]]@Polygons[[j]]@coords[,1]) > 180))
    {
      if(any(PA.oceania@polygons[[i]]@Polygons[[j]]@coords[,1] > 180))
      {
        PA.oceania@polygons[[i]]@Polygons[[j]]@coords[PA.oceania@polygons[[i]]@Polygons[[j]]@coords[,1] > 180 , 1] <- 179.9999
      }else{
        PA.oceania@polygons[[i]]@Polygons[[j]]@coords[PA.oceania@polygons[[i]]@Polygons[[j]]@coords[,1] < -180 , 1] <- -179.9999
      }
    }
  }
}


writePolyShape(PA.oceania, "/media/terra/EDGEII/data/EGV/PA/Oceania/oceania_adjusted")
PA.oceania <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Oceania/oceania_adjusted.shp", proj4string = CRS("+proj=longlat +datum=WGS84"))
########################## start new: simplfiy the shapes and reproject them
library(rgeos)
library(raster)
library(sp)
library(maptools)
library(rgdal)

PA.africa <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Africa/ksafi-orn-mpg-de-search-1360788424502.shp", proj4string=CRS("+proj=longlat +datum=WGS84"))
PA.africa$IO <- 1
PA.africa$wdpaid <- NULL
PA.africa$objectid <- NULL
PA.africa$wdpa_pid <- NULL
PA.africa$country <- NULL
PA.africa$sub_loc <- NULL
PA.africa$name <- NULL
PA.africa$orig_name <- NULL
PA.africa$desig <- NULL
PA.africa$desig_eng <- NULL
PA.africa$desig_type <- NULL
PA.africa$iucn_cat <- NULL
PA.africa$marine <- NULL
PA.africa$rep_m_area <- NULL
PA.africa$rep_area <- NULL
PA.africa$status <- NULL
PA.africa$status_yr <- NULL
PA.africa$gov_type <- NULL
PA.africa$mang_auth <- NULL
PA.africa$int_crit <- NULL
PA.africa$mang_plan <- NULL
PA.africa$official <- NULL
PA.africa$is_point <- NULL
PA.africa$no_take <- NULL
PA.africa$no_tk_area <- NULL
PA.africa$metadata_i <- NULL
PA.africa$action <- NULL
PA.africa <- spTransform(PA.africa, CRS("+proj=moll +datum=WGS84"))

PA.americas <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Americas/ksafi-orn-mpg-de-search-1360789190800.shp", proj4string=CRS("+proj=longlat +datum=WGS84"))
PA.americas$IO <- 1
PA.americas$wdpaid <- NULL
PA.americas$objectid <- NULL
PA.americas$wdpa_pid <- NULL
PA.americas$country <- NULL
PA.americas$sub_loc <- NULL
PA.americas$name <- NULL
PA.americas$orig_name <- NULL
PA.americas$desig <- NULL
PA.americas$desig_eng <- NULL
PA.americas$desig_type <- NULL
PA.americas$iucn_cat <- NULL
PA.americas$marine <- NULL
PA.americas$rep_m_area <- NULL
PA.americas$rep_area <- NULL
PA.americas$status <- NULL
PA.americas$status_yr <- NULL
PA.americas$gov_type <- NULL
PA.americas$mang_auth <- NULL
PA.americas$int_crit <- NULL
PA.americas$mang_plan <- NULL
PA.americas$official <- NULL
PA.americas$is_point <- NULL
PA.americas$no_take <- NULL
PA.americas$no_tk_area <- NULL
PA.americas$metadata_i <- NULL
PA.americas$action <- NULL
PA.americas <- spTransform(PA.americas, CRS("+proj=moll +datum=WGS84"))

PA.europe <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Europe/ksafi-orn-mpg-de-search-1360789321407.shp", proj4string=CRS("+proj=longlat +datum=WGS84"))
PA.europe$IO <- 1
PA.europe$wdpaid <- NULL
PA.europe$objectid <- NULL
PA.europe$wdpa_pid <- NULL
PA.europe$country <- NULL
PA.europe$sub_loc <- NULL
PA.europe$name <- NULL
PA.europe$orig_name <- NULL
PA.europe$desig <- NULL
PA.europe$desig_eng <- NULL
PA.europe$desig_type <- NULL
PA.europe$iucn_cat <- NULL
PA.europe$marine <- NULL
PA.europe$rep_m_area <- NULL
PA.europe$rep_area <- NULL
PA.europe$status <- NULL
PA.europe$status_yr <- NULL
PA.europe$gov_type <- NULL
PA.europe$mang_auth <- NULL
PA.europe$int_crit <- NULL
PA.europe$mang_plan <- NULL
PA.europe$official <- NULL
PA.europe$is_point <- NULL
PA.europe$no_take <- NULL
PA.europe$no_tk_area <- NULL
PA.europe$metadata_i <- NULL
PA.europe$action <- NULL
PA.europe <- spTransform(PA.europe, CRS("+proj=moll +datum=WGS84"))



PA.asia <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Asia/ksafi-orn-mpg-de-search-136078923051.shp", proj4string=CRS("+proj=longlat +datum=WGS84"))
PA.asia$IO <- 1
PA.asia$wdpaid <- NULL
PA.asia$objectid <- NULL
PA.asia$wdpa_pid <- NULL
PA.asia$country <- NULL
PA.asia$sub_loc <- NULL
PA.asia$name <- NULL
PA.asia$orig_name <- NULL
PA.asia$desig <- NULL
PA.asia$desig_eng <- NULL
PA.asia$desig_type <- NULL
PA.asia$iucn_cat <- NULL
PA.asia$marine <- NULL
PA.asia$rep_m_area <- NULL
PA.asia$rep_area <- NULL
PA.asia$status <- NULL
PA.asia$status_yr <- NULL
PA.asia$gov_type <- NULL
PA.asia$mang_auth <- NULL
PA.asia$int_crit <- NULL
PA.asia$mang_plan <- NULL
PA.asia$official <- NULL
PA.asia$is_point <- NULL
PA.asia$no_take <- NULL
PA.asia$no_tk_area <- NULL
PA.asia$metadata_i <- NULL
PA.asia$action <- NULL
PA.asia <- spTransform(PA.asia, CRS("+proj=moll +datum=WGS84"))

PA.oceania <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/Oceania/oceania_adjusted.shp", proj4string = CRS("+proj=longlat +datum=WGS84"))
PA.oceania$IO <- 1
PA.oceania$wdpaid <- NULL
PA.oceania$objectid <- NULL
PA.oceania$wdpa_pid <- NULL
PA.oceania$country <- NULL
PA.oceania$sub_loc <- NULL
PA.oceania$name <- NULL
PA.oceania$orig_name <- NULL
PA.oceania$desig <- NULL
PA.oceania$desig_eng <- NULL
PA.oceania$desig_type <- NULL
PA.oceania$iucn_cat <- NULL
PA.oceania$marine <- NULL
PA.oceania$rep_m_area <- NULL
PA.oceania$rep_area <- NULL
PA.oceania$status <- NULL
PA.oceania$status_yr <- NULL
PA.oceania$gov_type <- NULL
PA.oceania$mang_auth <- NULL
PA.oceania$int_crit <- NULL
PA.oceania$mang_plan <- NULL
PA.oceania$official <- NULL
PA.oceania$is_point <- NULL
PA.oceania$no_take <- NULL
PA.oceania$no_tk_area <- NULL
PA.oceania$metadata_i <- NULL
PA.oceania$action <- NULL
PA.oceania <- spTransform(PA.oceania, CRS("+proj=moll +datum=WGS84"))

writePolyShape(PA.africa, "/media/terra/EDGEII/data/EGV/PA/PA.africa.moll.simple.shp")
writePolyShape(PA.americas, "/media/terra/EDGEII/data/EGV/PA/PA.americas.moll.simple.shp")
writePolyShape(PA.europe, "/media/terra/EDGEII/data/EGV/PA/PA.europe.moll.simple.shp")
writePolyShape(PA.asia, "/media/terra/EDGEII/data/EGV/PA/PA.asia.moll.simple.shp")
writePolyShape(PA.oceania, "/media/terra/EDGEII/data/EGV/PA/PA.oceania.moll.simple.shp")


#################### start a new again load the data and calculate the overlap
library(rgeos)
library(raster)
library(sp)
library(maptools)
library(rgdal)

PA.africa <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/PA.africa.moll.simple.shp", proj4string=CRS("+proj=moll +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0"))
PA.americas <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/PA.americas.moll.simple.shp", proj4string=CRS("+proj=moll +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0"))
PA.europe <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/PA.europe.moll.simple.shp", proj4string=CRS("+proj=moll +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0"))
PA.asia <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/PA.asia.moll.simple.shp", proj4string=CRS("+proj=moll +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0"))
PA.oceania <- readShapePoly("/media/terra/EDGEII/data/EGV/PA/PA.oceania.moll.simple.shp", proj4string = CRS("+proj=moll +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0"))

load("/media/terra/EDGEII/Manuscript/Supplemental material/EDGEarea.Rdata")
lf <- list.files("/media/terra/EDGEII/data", pattern="_qEDGE.Rdata", full.names=T)
lf_amph <- lf[grep("amph", lf)]
lf_mammal <- lf[grep("mammal", lf)]
pEDGE.mammal <- NULL
pEDGE.amph <- NULL
pED.mammal <- NULL
pED.amph <- NULL

for(i in 1:length(lf_amph))
{
  load(lf_amph[i])
  coordinates(amph.qEDGE) <- ~X+Y
  gridded(amph.qEDGE) <- TRUE
  r <- raster(amph.qEDGE)
  PA.africa.r <- rasterize(PA.africa, r, field="IO")
  PA.americas.r <- rasterize(PA.americas, r, field="IO")
  PA.europe.r <- rasterize(PA.europe, r, field="IO")
  PA.asia.r <- rasterize(PA.asia, r, field="IO")
  PA.oceania.r <- rasterize(PA.oceania, r, field="IO")
  PA.africa.p <- rasterToPolygons(PA.africa.r, dissolve=T)
  PA.americas.p <- rasterToPolygons(PA.americas.r, dissolve=T)
  PA.europe.p <- rasterToPolygons(PA.europe.r, dissolve=T)
  PA.asia.p <- rasterToPolygons(PA.asia.r, dissolve=T)
  PA.oceania.p <- rasterToPolygons(PA.oceania.r, dissolve=T)
  m.edge <- EDGEarea[EDGEarea$Res==as.numeric(matrix(unlist(strsplit(lf_amph[i], "_")), ncol=3, byrow=T)[,2]) 
                     & EDGEarea$Taxon=="Amphibia" 
                     & EDGEarea$Cat=="EDGE", ]
  proj4string(m.edge) <- CRS("+proj=moll +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0")
  I1 <- gIntersection(m.edge, PA.africa.p)
  I2 <- gIntersection(m.edge, PA.americas.p)
  I3 <- gIntersection(m.edge, PA.europe.p)
  I4 <- gIntersection(m.edge, PA.asia.p)
  I5 <- gIntersection(m.edge, PA.oceania.p)
  parea <- NULL
  parea <- if(!is.null(I1)) c(parea, gArea(I1))
  parea <- if(!is.null(I2)) c(parea, gArea(I2))
  parea <- if(!is.null(I3)) c(parea, gArea(I3))
  parea <- if(!is.null(I4)) c(parea, gArea(I4))
  parea <- if(!is.null(I5)) c(parea, gArea(I5))
  pEDGE.amph <- c(pEDGE.amph, I(sum(parea) / gArea(m.edge)))
}

for(i in 1:length(lf_mammal))
{
  load(lf_mammal[i])
  coordinates(mammal.qEDGE) <- ~X+Y
  gridded(mammal.qEDGE) <- TRUE
  r <- raster(mammal.qEDGE)
  PA.africa.r <- rasterize(PA.africa, r, field="IO")
  PA.americas.r <- rasterize(PA.americas, r, field="IO")
  PA.europe.r <- rasterize(PA.europe, r, field="IO")
  PA.asia.r <- rasterize(PA.asia, r, field="IO")
  PA.oceania.r <- rasterize(PA.oceania, r, field="IO")
  PA.africa.p <- rasterToPolygons(PA.africa.r, dissolve=T)
  PA.americas.p <- rasterToPolygons(PA.americas.r, dissolve=T)
  PA.europe.p <- rasterToPolygons(PA.europe.r, dissolve=T)
  PA.asia.p <- rasterToPolygons(PA.asia.r, dissolve=T)
  PA.oceania.p <- rasterToPolygons(PA.oceania.r, dissolve=T)
  m.edge <- EDGEarea[EDGEarea$Res==as.numeric(matrix(unlist(strsplit(lf_mammal[i], "_")), ncol=3, byrow=T)[,2]) 
                     & EDGEarea$Taxon=="Mammalia" 
                     & EDGEarea$Cat=="EDGE", ]
  proj4string(m.edge) <- CRS("+proj=moll +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0")
  I1 <- gIntersection(m.edge, PA.africa.p)
  I2 <- gIntersection(m.edge, PA.americas.p)
  I3 <- gIntersection(m.edge, PA.europe.p)
  I4 <- gIntersection(m.edge, PA.asia.p)
  I5 <- gIntersection(m.edge, PA.oceania.p)
  parea <- NULL
  parea <- if(!is.null(I1)) c(parea, gArea(I1))
  parea <- if(!is.null(I2)) c(parea, gArea(I2))
  parea <- if(!is.null(I3)) c(parea, gArea(I3))
  parea <- if(!is.null(I4)) c(parea, gArea(I4))
  parea <- if(!is.null(I5)) c(parea, gArea(I5))
  pEDGE.mammal <- c(pEDGE.mammal, I(sum(parea) / gArea(m.edge))) 
}

for(i in 1:length(lf_amph))
{
  load(lf_amph[i])
  coordinates(amph.qEDGE) <- ~X+Y
  gridded(amph.qEDGE) <- TRUE
  r <- raster(amph.qEDGE)
  PA.africa.r <- rasterize(PA.africa, r, field="IO")
  PA.americas.r <- rasterize(PA.americas, r, field="IO")
  PA.europe.r <- rasterize(PA.europe, r, field="IO")
  PA.asia.r <- rasterize(PA.asia, r, field="IO")
  PA.oceania.r <- rasterize(PA.oceania, r, field="IO")
  PA.africa.p <- rasterToPolygons(PA.africa.r, dissolve=T)
  PA.americas.p <- rasterToPolygons(PA.americas.r, dissolve=T)
  PA.europe.p <- rasterToPolygons(PA.europe.r, dissolve=T)
  PA.asia.p <- rasterToPolygons(PA.asia.r, dissolve=T)
  PA.oceania.p <- rasterToPolygons(PA.oceania.r, dissolve=T)
  m.edge <- EDGEarea[EDGEarea$Res==as.numeric(matrix(unlist(strsplit(lf_amph[i], "_")), ncol=3, byrow=T)[,2]) 
                     & EDGEarea$Taxon=="Amphibia" 
                     & EDGEarea$Cat=="ED", ]
  proj4string(m.edge) <- CRS("+proj=moll +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0")
  I1 <- gIntersection(m.edge, PA.africa.p)
  I2 <- gIntersection(m.edge, PA.americas.p)
  I3 <- gIntersection(m.edge, PA.europe.p)
  I4 <- gIntersection(m.edge, PA.asia.p)
  I5 <- gIntersection(m.edge, PA.oceania.p)
  parea <- NULL
  parea <- if(!is.null(I1)) c(parea, gArea(I1))
  parea <- if(!is.null(I2)) c(parea, gArea(I2))
  parea <- if(!is.null(I3)) c(parea, gArea(I3))
  parea <- if(!is.null(I4)) c(parea, gArea(I4))
  parea <- if(!is.null(I5)) c(parea, gArea(I5))
  pED.amph <- c(pED.amph, I(sum(parea) / gArea(m.edge)))
}

for(i in 1:length(lf_mammal))
{
  load(lf_mammal[i])
  coordinates(mammal.qEDGE) <- ~X+Y
  gridded(mammal.qEDGE) <- TRUE
  r <- raster(mammal.qEDGE)
  PA.africa.r <- rasterize(PA.africa, r, field="IO")
  PA.americas.r <- rasterize(PA.americas, r, field="IO")
  PA.europe.r <- rasterize(PA.europe, r, field="IO")
  PA.asia.r <- rasterize(PA.asia, r, field="IO")
  PA.oceania.r <- rasterize(PA.oceania, r, field="IO")
  PA.africa.p <- rasterToPolygons(PA.africa.r, dissolve=T)
  PA.americas.p <- rasterToPolygons(PA.americas.r, dissolve=T)
  PA.europe.p <- rasterToPolygons(PA.europe.r, dissolve=T)
  PA.asia.p <- rasterToPolygons(PA.asia.r, dissolve=T)
  PA.oceania.p <- rasterToPolygons(PA.oceania.r, dissolve=T)
  m.edge <- EDGEarea[EDGEarea$Res==as.numeric(matrix(unlist(strsplit(lf_mammal[i], "_")), ncol=3, byrow=T)[,2]) 
                     & EDGEarea$Taxon=="Mammalia" 
                     & EDGEarea$Cat=="ED", ]
  proj4string(m.edge) <- CRS("+proj=moll +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0")
  I1 <- gIntersection(m.edge, PA.africa.p)
  I2 <- gIntersection(m.edge, PA.americas.p)
  I3 <- gIntersection(m.edge, PA.europe.p)
  I4 <- gIntersection(m.edge, PA.asia.p)
  I5 <- gIntersection(m.edge, PA.oceania.p)
  parea <- NULL
  parea <- if(!is.null(I1)) c(parea, gArea(I1))
  parea <- if(!is.null(I2)) c(parea, gArea(I2))
  parea <- if(!is.null(I3)) c(parea, gArea(I3))
  parea <- if(!is.null(I4)) c(parea, gArea(I4))
  parea <- if(!is.null(I5)) c(parea, gArea(I5))
  pED.mammal <- c(pED.mammal, I(sum(parea) / gArea(m.edge))) 
}

 
