Starting weekly assessment for Rachel, Week3

Current Marks = 100

Note that: 
(1) Major sections begin with a double "====" line 
(2) Subsections begin with a single "====" line 
(3) Code output or text file content are printed within single "*****" lines 

======================================================================
======================================================================
PART 1: Checking project workflow...

Found the following directories in parent directory: .git, Week2, Week9, Week4, Assessment, CMEELongPrac, Week1, Week5, Week8, Week6

Found the following files in parent directory: texput.log, README.txt, .gitignore, __init__.py, contributors.txt

Checking for key files in parent directory...

Found .gitignore in parent directory, great! 

Printing contents of .gitignore:
**********************************************************************
*~ 
*.tmp
*.pyc
**********************************************************************

Found README in parent directory, named: README.txt

Printing contents of README.txt:
**********************************************************************
My CMEECourseWork Repository
Contains codes, data and practicals from each week

Author: Rachel Balasuriya
Contact: rachel.balasuriya15@imperial.ac.uk
**********************************************************************

======================================================================
Looking for the weekly directories...

Found 7 weekly directories: Week1, Week2, Week4, Week5, Week6, Week8, Week9

The Week3 directory will be assessed 

======================================================================
======================================================================
PART 2: Checking weekly code and workflow...

======================================================================
======================================================================

FINISHED WEEKLY ASSESSMENT

Current Marks for the Week = 100

NOTE THAT THESE ARE NOT THE FINAL MARKS FOR THE WEEK, BUT UPPER BOUND ON THE MARKS!